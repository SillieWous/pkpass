#!/bin/bash

show_help()
{
  echo TODO
}

MANIFEST="manifest-deb.yml"
OUTDIR="."
WORKDIR="."

ARCHS="armel|armhf|arm64|i386|amd64|mipsel|mips64el|ppc64el|s390x"
PRIORITIES="optional|standard|important|required"
SECTIONS="admin|cli-mono|comm|database|debug|devel|doc|editors|education|\
          electronics|embedded|fonts|games|gnome|gnu-r|gnustep|graphics|\
          hamradio|haskell|httpd|interpreters|introspection|java|javascript|\
          kde|kernel|libdevel|libs|lisp|localization|mail|math|metapackages|\
          misc|net|news|ocaml|oldlibs|otherosfs|perl|php|python|ruby|rust|\
          science|shells|sound|tasks|tex|text|utils|vcs|video|web|x11|xfce|zope"

OPTIND=1
while getopts a:d:f:hw: OPT; do
  case $OPT in
    a)  ARCH=${OPTARG}
        ;;
    d)  OUTDIR=${OPTARG}
        ;;
    f)  MANIFEST=${OPTARG}
        ;;
    h)  show_help
        exit 0
        ;;
    w)  WORKDIR=${OPTARG}
        ;;
    *)  show_help >&2
        exit 1
        ;;
  esac
done

cd ${WORKDIR}

PACKAGE_NAME=$(sed -nEe "s/^  package_name\s*:\s*(\w+)$/\1/p" $MANIFEST)
SOURCE=$(sed -nEe "s/^  source\s*:\s*(\w+)$/\1/p" $MANIFEST)
VERSION=$(sed -nEe "s/^  version\s*:\s*([0-9.]+[0-9]+)$/\1/p" $MANIFEST)
SECTION=$(sed -nEe "s/^  section\s*:\s*($SECTIONS)$/\1/p" $MANIFEST)
PRIORITY=$(sed -nEe "s/^  priority\s*:\s*($PRIORITIES)$/\1/p" $MANIFEST)
if [ -z "${ARCH}" ]; then
  ARCH=$(sed -nEe "s/^  arch\s*:\s*($ARCHS)$/\1/p" $MANIFEST)
fi
ESSENTIAL=$(sed -nEe "s/^  essential\s*:\s*(yes|no)$/\1/p" $MANIFEST)
INSTALLED_SIZE=$(sed -nEe "s/^  installed_size\s*:\s*([0-9]+)$/\1/p" $MANIFEST)
MAINTAINER=$(sed -nEe "s/^  maintainer\s*:\s*(\w.*)$/\1/p" $MANIFEST)
DESCRIPTION=$(sed -nEe "s/^  description\s*:\s*(\w.*)$/\1/p" $MANIFEST)
HOMEPAGE=$(sed -nEe "s/^  homepage\s*:\s*([A-Za-z0-9\/:\.]@)$/\1/p" $MANIFEST)
BUILT_USING=$(sed -nEe "s/^  built_using\s*:\s*(\S*)$/\1/p" $MANIFEST)

FILES=$(awk "/^  file\s*:\s*$/{flag=1;next}/^  \w+\s*:/{flag=0}flag" $MANIFEST)

# Mandatory: Package, Version, Architechture, Description, Maintainer
if [ -z "${PACKAGE_NAME}" ]; then echo "'package_name' must be set"; exit 1; fi
if [ -z "${VERSION}" ];      then echo "'version' must be set";      exit 1; fi
if [ -z "${ARCH}" ];         then echo "'arch' must be set";         exit 1; fi
if [ -z "${DESCRIPTION}" ];  then echo "'description' must be set";  exit 1; fi

if [ -z "${MAINTAINER}" ];   then echo "'maintainer' not set, writing $USERNAME"; MAINTAINER=${USERNAME}; fi

# Recomended: Section, Priority
if [ -z "${PRIORITY}" ]; then echo "'priority' not set, writing optional"; PRIORITY="optional"; fi
if [ -z "${SECTION}" ];  then echo "It is recommended to set 'section'"; fi

# Optional: Source, Essential, Depends, Installed-size, Homepage, Built-Using


## Create folder structure
DEB_DIR=${OUTDIR}/${PACKAGE_NAME}_${VERSION}_${ARCH}

rm -rf ${DEB_DIR}          # Ensure clear directory
mkdir -p ${DEB_DIR}/DEBIAN

## Copy FILES
OLDIFS=$IFS
IFS=$'\n'
for LINE in $FILES; do
  LINE="$(eval "echo $LINE")"

  if [[ $LINE == *"-"* ]]; then
    if [[ -v "FILE_PATH" ]]; then
      if ([ -z ${FILE_PATH} ] || [ -z $PKG_PATH ]); then
        echo "Missing pkg_path or file_path"
        exit 1
      fi
      install -D "$FILE_PATH" "$DEB_DIR/${PKG_PATH#/}"
    fi
    FILE_PATH=""
    PKG_PATH=""
  fi

  NEW_FILE_PATH=$(echo $LINE | sed -nE "s/\s*-?\s*file_path\s*:\s*(\S*)/\1/p")
  NEW_PKG_PATH=$(echo $LINE | sed -nE "s/\s*-?\s*pkg_path\s*:\s*(\S*)/\1/p")
  if [[ ! -z $NEW_FILE_PATH ]]; then
    FILE_PATH=$NEW_FILE_PATH
  fi
  if [[ ! -z $NEW_PKG_PATH ]]; then
    PKG_PATH=$NEW_PKG_PATH
  fi
done
IFS=$OLDIFS

install -D "$FILE_PATH" "$DEB_DIR/${PKG_PATH#/}"

# determine installed size if not set
if [ -z "${INSTALLED_SIZE}" ]; then
  INSTALLED_SIZE="$(du -s ${DEB_DIR} | awk '{print $1;}')"
  echo "'installed_size' not set determined as: ${INSTALLED_SIZE}"
fi

# Create control file
                                  CONTROL="Package:${PACKAGE_NAME}"
if [ ! -z $SOURCE ];         then CONTROL="$CONTROL\nSource:$SOURCE"; fi
                                  CONTROL="$CONTROL\nVersion:$VERSION"
if [ ! -z $SECTION ];        then CONTROL="$CONTROL\nSection:$SECTION"; fi
if [ ! -z $PRIORITY ];       then CONTROL="$CONTROL\nPriority:$PRIORITY"; fi
                                  CONTROL="$CONTROL\nArchitecture:$ARCH"
if [ ! -z $ESSENTIAL ];      then CONTROL="$CONTROL\nEssential:$ESSENTIAL"; fi
if [ ! -z $INSTALLED_SIZE ]; then CONTROL="$CONTROL\nInstalled-Size:$INSTALLED_SIZE"; fi
                                  CONTROL="$CONTROL\nMaintainer:$MAINTAINER"
                                  CONTROL="$CONTROL\nDescription:$DESCRIPTION"
if [ ! -z $HOMEPAGE ];       then CONTROL="$CONTROL\nHompage:$HOMEPAGE"; fi
if [ ! -z $BUILT_USING ];    then CONTROL="$CONTROL\nBuilt-Using:$BUILT_USING"; fi

echo -e $CONTROL >> ${DEB_DIR}/DEBIAN/control

# set directory permisions
find "${DEB_DIR}" -type d -exec chmod 0755 {} \;  #set directory attributes
find "${DEB_DIR}" -type f -exec chmod 0644 {} \;  #set data file attributes
if [ -d "${DEB_DIR}/usr/bin" ]; then
  find "${DEB_DIR}/usr/bin" -type f -exec chmod 0755 {} \;  #set executable attributes
fi

## create package
dpkg-deb --root-owner-group --build "${DEB_DIR}" "${PACKAGE_NAME}.deb"
