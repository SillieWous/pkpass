#ifndef PASS_H
#define PASS_H

#include "widget/resizableimage.h"

#include <gtkmm/overlay.h>
#include <gtkmm/image.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/eventbox.h>

#include <memory>

#include <libpkpass.h>

using namespace std;

// Main ui namespace
namespace pkpass::gtk
{
  /*!
   * \brief A widget for displaying a pass.
   */
  class Pass : public virtual Gtk::EventBox
  {
  public:
    Pass(shared_ptr<pkpass::IPass>);
    /// \todo desctuctor

    void on_size_allocate        (Gtk::Allocation &allocation) override;
    bool on_button_release_event (GdkEventButton *event) override;
    void set_height              (int height = -1);
    void flip                    ();

  protected:
    /// \brief
    shared_ptr<pkpass::IPass>  Pk_pass;

    /// \brief
    vector<ResizableImage*> Background_images;
    /// \brief
    vector<ResizableImage*> Logo_images;
    /// \brief
    ResizableImage *Background_image;
    /// \brief
    ResizableImage *Logo_image;
    /// \brief
    ResizableImage *Barcode_image;
    /// \brief
    Gtk::Overlay   *Overlay;
    /// \brief
    Gtk::Box       *Pass_fields_box;
    /// \brief
    Gtk::Box       *Header_box;
    /// \brief
    Gtk::Label     *Header_label;
    /// \brief
    Gtk::Label     *Primary_label;
    /// \brief
    Gtk::Label     *Secondary_label;
    /// \brief
    Gtk::Label     *Auxiliary_label;
    /// \brief
    Gtk::Label     *Back_label;
    /// \brief
    Gtk::Button    *Flip_button;

    /// \brief
    string Label_colour = "#000000";

    /// \brief
    int  Height = -1;

    /// \brief beunwerk
    bool Flipped = false;

    void add_pass_fields ( pkpass::IPassFields&         passFields );
    void add_label       ( Gtk::Label                  *label,
                           pkpass::IPassFieldContents   content,
                           Gtk::Justification           justify );
    void add_aux_label   ( Gtk::Label                  *label,
                           pkpass::IAuxiliaryFields     content,
                           Gtk::Justification           justify );
    void set_label_colour();

    int select_best_image_fit(int width);
  };
}

#endif // PASS_H
