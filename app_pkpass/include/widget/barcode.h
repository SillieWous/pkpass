#ifndef BARCODE_H
#define BARCODE_H

#include "widget/resizableimage.h"

#include <libpkpass.h>

namespace pkpass::gtk
{
  /// \brief Helper namespace for create barcode backgrounds.
  namespace barcode
  {
    ResizableImage *create_background_image(pkpass::IBarcode& barcode);
  };
}

#endif // BARCODE_H
