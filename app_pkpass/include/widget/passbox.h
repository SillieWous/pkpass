#ifndef PASSBOX_H
#define PASSBOX_H

#include "pass.h"

#include <libpkpass.h>

#include <gtkmm/box.h>

#include <vector>
#include <memory>

using namespace std;

namespace pkpass::gtk
{
  /*!
   * \brief A box/array containing all passes.
   */
  class PassBox : public virtual Gtk::Box
  {
  public:
    PassBox();

    void add(shared_ptr<pkpass::IPass> pass);
    void flip_pass();

  private:
    /// \brief
    vector<Pass*> Passes;
  };
}

#endif // PASSBOX_H
