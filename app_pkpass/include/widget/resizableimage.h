#ifndef RESIZABLEIMAGE_H
#define RESIZABLEIMAGE_H

#include <gtkmm/drawingarea.h>

using namespace std;

namespace pkpass::gtk
{
  /*!
   * \brief A class used for displaying background images of a pass that scales with the size of the box.
   */
  class ResizableImage : public Gtk::DrawingArea
  {
  public:
    /// \brief
    enum class EImageType
    {
      FILE,      ///<
      SVG_STRING ///<
    };

    ResizableImage(string text, EImageType type = EImageType::FILE);

    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
    void blur();
    int  get_natural_width();
    int  get_natural_height();
    void set_height(int height);
    void set_width(int width);
    void set_expanded(bool expanded = true);
    bool is_expanded();

  protected:
    /// \brief
    Glib::RefPtr<Gdk::Pixbuf> Pixbuf;
    /// \brief
    int                       Natural_width;
    /// \brief
    int                       Natural_height;
    /// \brief
    int                       Height = -1;
    /// \brief
    int                       Width = -1;
    /// \brief
    bool                      Expanded = false;

  private:
    Cairo::RefPtr<Cairo::ImageSurface> pixbuf_to_image_surface(Glib::RefPtr<Gdk::Pixbuf> pixbuf);
  };
}

#endif // RESIZABLEIMAGE_H
