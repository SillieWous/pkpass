#ifndef PASSES_H
#define PASSES_H

#include "widget/passbox.h"

#include <gtkmm/headerbar.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/flowbox.h>
#include <gtkmm/viewport.h>
#include <gtkmm/window.h>
#include <gtkmm/paned.h>
#include <gtkmm/button.h>
#include <gtkmm/filechooserdialog.h>


namespace pkpass::gtk
{
  /*!
   * \brief The main window
   */
  class PassesWindow : public Gtk::Window
  {
  public:
    PassesWindow();

  private:
    /// \brief
    Gtk::HeaderBar      *Header_bar;
    /// \brief
    Gtk::ScrolledWindow *Scrolled_window;
    /// \brief
    Gtk::Viewport       *Viewport;
    /// \brief
    Gtk::Button         *Add_button;
    /// \brief
    PassBox             *Pass_box;

    void open_file_chooser();

    void toggle_settings();

    void on_file_dialog_response(int response_id, Gtk::FileChooserDialog* dialog);
    void on_folder_dialog_response(int response_id, Gtk::FileChooserDialog* dialog);

    void on_load();
  };
}

#endif // PASSES_H
