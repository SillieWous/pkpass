TEMPLATE = app
CONFIG += c++17
CONFIG -= qt
CONFIG -= app_bundle

CONFIG(debug, debug|release):DEFINES += DEBUG

VERSION = 0.0.1

CONFIG += link_pkgconfig
PKGCONFIG += gtkmm-3.0

glibResourcesTarget.target = $$OUT_PWD/passes-resources.c
glibResourcesTarget.depends = FORCE
glibResourcesTarget.commands = \
  glib-compile-resources $$PWD/src/passes.gresource.xml --sourcedir $$PWD/src --c-name passes --internal --generate \
    --target $$OUT_PWD/passes-resources.c; \
  glib-compile-resources $$PWD/src/passes.gresource.xml --sourcedir $$PWD/src --c-name passes --internal --generate \
    --target $$OUT_PWD/passes-resources.h;

QMAKE_EXTRA_TARGETS += glibResourcesTarget
GENERATED_SOURCES += $$OUT_PWD/passes-resources.c

SOURCES += \
  src/main.cpp \
  src/widget/barcode.cpp \
  src/widget/pass.cpp \
  src/widget/passbox.cpp \
  src/widget/resizableimage.cpp \
  src/window/passes.cpp

HEADERS += \
  include/widget/barcode.h \
  include/widget/pass.h \
  include/widget/passbox.h \
  include/widget/resizableimage.h \
  include/window/passes.h

OTHER_FILES += \
  src/passes.gresource.xml \
  src/passes-window.ui \
  src/passes-widget.ui \
  po/LINGUAS \
  po/POTFILES \
  data/org.gnome.passes.appdata.xml.in \
  data/org.gnome.passes.desktop.in \
  data/org.gnome.passes.gschema.xml \
  manifest-deb.yml \
  passes.desktop \
  passes.svg

INCLUDEPATH += \
  $$PWD/include \
  $$PWD/../libpkpass/include \
  $$PWD/../libbarcodegen/include

LIBS += \
  -L$$OUT_PWD/../libbarcodegen -lbarcodegen \
  -L$$OUT_PWD/../libpkpass -lpkpass \
  -ljsoncpp

QMAKE_POST_LINK += \
  cp $$PWD/passes.desktop $$PWD/passes.svg $$OUT_PWD/ && \
  $$PWD/../package_deb.sh -f $$PWD/manifest-deb.yml -w $$OUT_PWD -a $$(TARGET_ARCH) &&\
  $$PWD/../doc/generate-docs.sh -d $$OUT_PWD -p app_pkpass -f $$PWD/../doc

desktop.files = passes.desktop
icon.files    = passes.svg

unix {
  target.path  = .local/bin
  desktop.path = .local/share/applications
  icon.path    = .local/share/icons/hicolor/scalable/apps
}
!isEmpty(target.path): INSTALLS += target desktop icon
