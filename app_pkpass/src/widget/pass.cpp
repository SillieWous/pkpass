#include "widget/pass.h"

#include "widget/barcode.h"

#include <gtkmm/builder.h>

#include <regex>
#include <sstream>
#include <iomanip>

using namespace std;

namespace pkpass::gtk
{
  /*!
   * \brief Pass::Pass
   * \param pass
   */
  Pass::Pass(shared_ptr<pkpass::IPass> pass)
    : Pk_pass(pass)
    , Header_label(nullptr)
    , Primary_label(nullptr)
    , Secondary_label(nullptr)
  {
    auto builder = Gtk::Builder::create_from_resource("/org/gnome/passes/passes-widget.ui");

    builder->get_widget("Pass-Overlay",         Overlay);
    builder->get_widget("Pass-Pass_fields_box", Pass_fields_box);
    builder->get_widget("Pass-Header_box",      Header_box);
    builder->get_widget("Pass-Header_label",    Header_label);
    builder->get_widget("Pass-Primary_label",   Primary_label);
    builder->get_widget("Pass-Secondary_label", Secondary_label);
    builder->get_widget("Pass-Auxiliary_label", Auxiliary_label);
    builder->get_widget("Pass-Back_label",      Back_label);
    builder->get_widget("Pass-Flip_button",     Flip_button);

    add(*Overlay);

    /// \todo check if files in directory
    Background_images.push_back(new ResizableImage(pass->get_storage_directory() + "background.png"));
    Background_images.push_back(new ResizableImage(pass->get_storage_directory() + "background@2x.png"));
    Logo_images      .push_back(new ResizableImage(pass->get_storage_directory() + "logo.png"));
    Logo_images      .push_back(new ResizableImage(pass->get_storage_directory() + "logo@2x.png"));
    Barcode_image    = barcode::create_background_image(*pass->get_barcodes().front());

    select_best_image_fit(get_width());

    Pass_fields_box->pack_end(*Barcode_image, true, true, 5);

    for(const auto& bg_image : Background_images)
      bg_image->blur();

    Barcode_image->set_halign(Gtk::ALIGN_CENTER);
    Barcode_image->set_valign(Gtk::ALIGN_END);

    add_pass_fields(pass->get_event_ticket());

    Flip_button->signal_clicked().connect( sigc::mem_fun(*this, &Pass::flip) );
    Overlay->show_all();
    Pass_fields_box->show_all();

    Back_label->hide();
  }

  /*!
   * \brief Pass::on_size_allocate
   * \param allocation
   */
  void Pass::on_size_allocate(Gtk::Allocation &allocation)
  {
    auto width = allocation.get_width();

    select_best_image_fit(width);

    if(width > Background_image->get_natural_width())
      {
        width = Background_image->get_natural_width();
        allocation.set_width(width);
      }

    Background_image->set_height(width/5);

    auto barcode_size = width/3;
    Barcode_image->set_height(barcode_size);
    Barcode_image->set_width(barcode_size);

    Gtk::EventBox::on_size_allocate(allocation);
  }

  /*!
   * \brief Pass::set_height
   * \param height
   */
  void Pass::set_height(int height)
  {
    Height = height;
    Background_image->set_height(Height);

    Gtk::Allocation allocation;
    int baseline;

    get_allocated_size(allocation, baseline);

    size_allocate(allocation);
  }

  void Pass::flip()
  {
    auto frontShown = !Back_label->get_visible();

    frontShown ? Back_label->show() : Back_label->hide();

    frontShown ? Primary_label->hide()   : Primary_label->show();
    frontShown ? Secondary_label->hide() : Secondary_label->show();
    frontShown ? Auxiliary_label->hide() : Auxiliary_label->show();
    frontShown ? Barcode_image->hide()   : Barcode_image->show();
    frontShown ? Header_box->hide()      : Header_box->show();

    Flipped = true;
  }

  /*!
   * \brief Pass::on_button_release_event
   * \param event
   * \return
   */
  bool Pass::on_button_release_event(GdkEventButton *event)
  {
    if(event->button == 1 && !Flipped)
      {
        Background_image->set_expanded(!Background_image->is_expanded());
        Background_image->queue_draw();

        Gtk::Allocation allocation;
        int baseline;

        get_allocated_size(allocation, baseline);

        size_allocate(allocation);
      }

    Flipped = false;

    return true;
  }

  /*!
   * \brief Pass::add_pass_fields
   * \param passFields
   */
  void Pass::add_pass_fields(pkpass::IPassFields& passFields)
  {
    set_label_colour();

    add_label    (Header_label,    passFields.get_header_fields(),    Gtk::JUSTIFY_RIGHT);
    add_label    (Primary_label,   passFields.get_primary_fields(),   Gtk::JUSTIFY_LEFT);
    add_label    (Secondary_label, passFields.get_secondary_fields(), Gtk::JUSTIFY_LEFT);
    add_aux_label(Auxiliary_label, passFields.get_auxiliary_fields(), Gtk::JUSTIFY_LEFT);
    add_label    (Back_label,      passFields.get_back_fields(),      Gtk::JUSTIFY_LEFT);
  }

  /*!
   * \brief Pass::add_label
   * \param label
   * \param content
   * \param justify
   */
  void Pass::add_label(Gtk::Label *label, pkpass::IPassFieldContents content, Gtk::Justification justify)
  {
    string text = "<span foreground=\"" + Label_colour + "\">";
    for(auto const& field : content)
        if(!field.second->get_value().empty())
          text += "<b>"+field.second->get_label() + "</b>\n" + field.second->get_value() + "\n\n";

    text.resize(text.size()-2);
    text += "</span>";

    label->set_markup(text);
    label->set_xalign(justify == Gtk::JUSTIFY_RIGHT);
    label->set_justify(justify);
  }

  /*!
   * \brief Pass::add_aux_label
   * \param label
   * \param content
   * \param justify
   * \todo merge this with add_label
   */
  void Pass::add_aux_label(Gtk::Label *label, pkpass::IAuxiliaryFields content, Gtk::Justification justify)
  {
    string text = "<span foreground=\"" + Label_colour + "\">";
    for(auto const& field : content)
        if(!field.second->get_value().empty())
          text += "<b>"+field.second->get_label() + "</b>\n" + field.second->get_value() + "\n\n";

    text.resize(text.size()-2);
    text += "</span>";

    label->set_markup(text);
    label->set_xalign(justify == Gtk::JUSTIFY_RIGHT);
    label->set_justify(justify);
  }

  /*!
   * \brief Pass::set_label_colour
   */
  void Pass::set_label_colour()
  {
    regex pattern("rgb\\((1?[0-9]{1,2}|2[0-4][0-9]|25[0-5]),\\D?(1?[0-9]{1,2}|2[0-4][0-9]|25[0-5]),\\D?(1?[0-9]{1,2}|2[0-4][0-9]|25[0-5])\\)");
    string colour = Pk_pass->get_label_colour();

    std::smatch match;
    if (std::regex_match(colour, match, pattern))
      {
        if (match.size() == 4)
          {
            std::stringstream colourStr;
            colourStr << "#" << uppercase << setfill('0') << std::hex
                      << setw(2) << stoi(match[1])
                      << setw(2) << stoi(match[2])
                      << setw(2) << stoi(match[3]);

            Label_colour = colourStr.str();
          }
      }
  }

  /*!
   * \brief Pass::select_best_image_fit
   * \param width
   * \return
   */
  int Pass::select_best_image_fit(int width)
  {
    static int best_idx = -1;
    int new_idx = 0;

    for(;size_t(new_idx)< Background_images.size()-1; new_idx ++)
      {
        if(Background_images[new_idx]->get_natural_width() >= get_scale_factor()*width)
          break;
      }

    if(new_idx != best_idx)
      {
        bool isExpanded = false;
        if(best_idx != -1)
          {
            isExpanded = Background_image->is_expanded();
            Overlay->remove();
            Header_box->remove(*Logo_image);
          }

        best_idx = new_idx;

        Background_image = Background_images[best_idx];
        Logo_image       = Logo_images[best_idx];

        Overlay->add(*Background_image);
        Header_box->pack_start(*Logo_image, false, true, 0);

        Background_image->set_expanded(isExpanded);

        Background_image->show();
        Logo_image->show();

        // dont know why font size is so weird (*1024)
        int font_size = (best_idx == 0) ? 1024*8 : 1024*11;
        auto attrs = new Pango::AttrList ();
        auto attr = Pango::Attribute::create_attr_size(font_size);
        attrs->change(attr);

        Header_label->set_attributes(*attrs);
        Primary_label->set_attributes(*attrs);
        Secondary_label->set_attributes(*attrs);
        Auxiliary_label->set_attributes(*attrs);
        Back_label->set_attributes(*attrs);
      }

    return best_idx;
  }
}
