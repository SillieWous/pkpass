#include "widget/passbox.h"

#include <gtkmm/image.h>
#include <iostream>

namespace pkpass::gtk
{
  /*!
   * \brief PassBox::PassBox
   */
  PassBox::PassBox()
  {
    set_orientation(Gtk::Orientation::ORIENTATION_VERTICAL);
    set_spacing(10);
    set_margin_end(10);
    set_margin_start(10);
    set_valign(Gtk::ALIGN_END);
  }

  /*!
   * \brief PassBox::add
   * \param pass
   */
  void PassBox::add(shared_ptr<pkpass::IPass> pass)
  {
    auto child = new Pass(pass);
    pack_end(*child, false, true);
    child->set_height(100);
    child->show();

    Passes.push_back(child);
  }
}
