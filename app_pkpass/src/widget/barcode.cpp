#include "widget/barcode.h"

#include "exception.h"

#include <libbarcodegen.h>

#include <fstream>
#include <string>
#include <iostream>

namespace pkpass::gtk
{
  namespace barcode
  {
    /*!
     * \brief create_background_image
     * \param barcode
     * \return
     */
    ResizableImage* create_background_image(IBarcode& barcode)
    {
      barcodegen::EFormat format;
      string              barcodeStr;
      switch(barcode.get_format())
        {
        case IBarcode::EFormat::QR:
          {
            auto code = barcodegen::IQr::create(barcode.get_message());
            barcodeStr = code->to_svg_string(2);
            break;
          }
        case IBarcode::EFormat::CODE128:
          {
            format = barcodegen::EFormat::CODE128;
            auto code = barcodegen::IBarcode::create(format, barcode.get_message());
            barcodeStr = code->to_svg_string(2);
            break;
          }
        case IBarcode::EFormat::PDF417:
          {
            auto code = barcodegen::IPdf417::create(barcode.get_message());
            /// \todo variable columns
            barcodeStr = code->to_svg_string(1, 2);
            break;
          }
        case IBarcode::EFormat::AZTEC:
          {
            format = barcodegen::EFormat::AZTEC;
            auto code = barcodegen::IBarcode::create(format, barcode.get_message());
            barcodeStr = code->to_svg_string(2);
            break;
          }
        }
      return new ResizableImage(barcodeStr, ResizableImage::EImageType::SVG_STRING);
    }
  }
}
