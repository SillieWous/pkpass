#include "widget/resizableimage.h"

#include <giomm/memoryinputstream.h>
#include <gdkmm/pixbufloader.h>

namespace pkpass::gtk
{
  /*!
   * \brief ResizableImage::ResizableImage
   * \param text
   * \param type
   */
  ResizableImage::ResizableImage(string text, EImageType type)
    : Height(-1),
      Width(-1),
      Expanded(false)
  {
    switch(type)
      {
      case EImageType::FILE:
        Pixbuf = Gdk::Pixbuf::create_from_file(text);
        break;
      case EImageType::SVG_STRING:
        auto loader = Gdk::PixbufLoader::create();
        loader->write(reinterpret_cast<const guint8*>(text.c_str()), text.length());
        loader->close();
        Pixbuf = loader->get_pixbuf();
        break;
      }

    Natural_width  = Pixbuf->get_width();
    Natural_height = Pixbuf->get_height();
  }

  /*!
   * \brief ResizableImage::on_draw
   * \param context
   * \return
   */
  bool ResizableImage::on_draw(const Cairo::RefPtr<Cairo::Context> &context)
  {
    int width = Width <= 0 ? get_allocated_width() : Width;

    auto res = Pixbuf;

    if(width < Pixbuf->get_width())
      res = Pixbuf->scale_simple(width, width * Pixbuf->get_height() / Pixbuf->get_width(), Gdk::InterpType::INTERP_BILINEAR);
    else if(Width > 0)
      res = Pixbuf->scale_simple(width, width * Pixbuf->get_height() / Pixbuf->get_width(), Gdk::InterpType::INTERP_TILES);

    int height = res->get_height();
    if(!Expanded)
      {
        if(Height > 0 && height > Height)
          {
            height = Height;
            res = Gdk::Pixbuf::create_subpixbuf(res, 0, 0, width, height);
          }
      }

    set_size_request(Width, height);

    gchar *buffer;
    gsize size = 0;
    res->save_to_buffer(buffer, size, "png");

    auto stream = Gio::MemoryInputStream::create();
    stream->add_data(buffer, size);
    auto surface = Cairo::ImageSurface::create_from_png_stream
      ([stream]
        (unsigned char* data, unsigned int length)
          {
            stream->read(data, length);
            return CAIRO_STATUS_SUCCESS;
          }
      );

    context->set_source(surface, 0, 0);
    context->paint();

    return true;
  }

  /*!
   * \brief ResizableImage::get_natural_width
   * \return
   */
  int ResizableImage::get_natural_width()
  {
    return Natural_width;
  }

  /*!
   * \brief ResizableImage::get_natural_height
   * \return
   */
  int ResizableImage::get_natural_height()
  {
    return Natural_height;
  }

  /*!
   * \brief ResizableImage::blur
   */
  void ResizableImage::blur()
  {
    auto surface = pixbuf_to_image_surface(Pixbuf);

    uint32_t *s, *d;
    uint8_t kernel[25];
    const int size = sizeof (kernel);
    const int half = size / 2;

    if (surface->get_status())
      return;

    auto width  = surface->get_width();
    auto height = surface->get_height();

    auto tmp = Cairo::ImageSurface::create(Cairo::FORMAT_ARGB32, width, height);

    auto src        = surface->get_data();
    auto dst        = tmp->get_data();

    auto src_stride = surface->get_stride();
    auto dst_stride = tmp->get_stride();

    uint32_t a = 0;
    for (int i = 0; i < size; i++)
      {
        double f = i - half;
        a += kernel[i] = exp (- f * f / 30.0) * 80;
      }

    /* Horizontally blur from surface -> tmp */
    for (int i = 0; i < height; i++)
      {
        s = (uint32_t *) (src + i * src_stride);
        d = (uint32_t *) (dst + i * dst_stride);

        for (int j = 0; j < width; j++)
          {
            if (!(0 < j && j < width))
              {
                d[j] = s[j];
                continue;
              }

            int x = 0, y = 0, z = 0, w = 0;
            int div = a;
            for (int k = 0; k < size; k++)
              {
                if (j - half + k < 0 || j - half + k >= width)
                  {
                    div -= kernel[k];
                    continue;
                  }

                uint32_t p = s[j - half + k];

                x += ((p >> 24) & 0xff) * kernel[k];
                y += ((p >> 16) & 0xff) * kernel[k];
                z += ((p >>  8) & 0xff) * kernel[k];
                w += ((p >>  0) & 0xff) * kernel[k];
              }
            d[j] = (x / div << 24) | (y / div << 16) | (z / div << 8) | w / div;
          }
      }

    /* Then vertically blur from tmp -> surface */
    for (int i = 0; i < height; i++)
      {
        s = (uint32_t *) (dst + i * dst_stride);
        d = (uint32_t *) (src + i * src_stride);

        for (int j = 0; j < width; j++)
          {
            if (!(0 <= i && i < height))
              {
                d[j] = s[j];
                continue;
              }

          int x = 0, y = 0, z = 0, w = 0;
          int div = a;
          for (int k = 0; k < size; k++)
            {
              if (i - half + k < 0 || i - half + k >= height)
                {
                  div -= kernel[k];
                  continue;
                }

              s = (uint32_t *) (dst + (i - half + k) * dst_stride);
              uint32_t p = s[j];

              x += ((p >> 24) & 0xff) * kernel[k];
              y += ((p >> 16) & 0xff) * kernel[k];
              z += ((p >>  8) & 0xff) * kernel[k];
              w += ((p >>  0) & 0xff) * kernel[k];
            }
          d[j] = (x / div << 24) | (y / div << 16) | (z / div << 8) | w / div;
        }
      }

    Pixbuf = Gdk::Pixbuf::create(surface, 0, 0, surface->get_width(), surface->get_height());
  }

  /*!
   * \brief ResizableImage::set_height
   * \param height
   */
  void ResizableImage::set_height(int height)
  {
    Height = height;
    set_size_request(Width, Height);
    queue_draw();
  }

  /*!
   * \brief ResizableImage::set_width
   * \param width
   */
  void ResizableImage::set_width(int width)
  {
    Width = width;
    set_size_request(Width, Height);
    queue_draw();
  }

  /*!
   * \brief ResizableImage::set_expanded
   * \param expanded
   */
  void ResizableImage::set_expanded(bool expanded)
  {
    Expanded = expanded;
    queue_draw();
  }

  /*!
   * \brief ResizableImage::get_expanded
   * \return
   */
  bool ResizableImage::is_expanded()
  {
    return Expanded;
  }

  /*!
   * \brief ResizableImage::pixbuf_to_image_surface
   * \param pixbuf
   * \return
   */
  Cairo::RefPtr<Cairo::ImageSurface> ResizableImage::pixbuf_to_image_surface(Glib::RefPtr<Gdk::Pixbuf> pixbuf)
  {
    gchar *buffer;
    gsize size = 0;
    pixbuf->save_to_buffer(buffer, size, "png");

    auto stream = Gio::MemoryInputStream::create();
    stream->add_data(buffer, size);
    auto surface = Cairo::ImageSurface::create_from_png_stream
      ([stream]
        (unsigned char* data, unsigned int length)
          {
            stream->read(data, length);
            return CAIRO_STATUS_SUCCESS;
          }
      );

    return surface;
  }
}

