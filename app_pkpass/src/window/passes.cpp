#include "window/passes.h"

#include <libpkpass.h>

#include <gtkmm/messagedialog.h>
#include <gtkmm/builder.h>

#include <iostream>
#include <filesystem>

namespace pkpass::gtk
{
  /*!
   * \brief PassesWindow::PassesWindow
   */
  PassesWindow::PassesWindow()
    : Glib::ObjectBase("PassesWindow")
    , Gtk::Window()
    , Header_bar(nullptr)
    , Scrolled_window(nullptr)
    , Viewport(nullptr)
    , Add_button(nullptr)
  {
    auto builder = Gtk::Builder::create_from_resource("/org/gnome/passes/passes-window.ui");
    builder->get_widget("Header_bar",      Header_bar);
    builder->get_widget("Scrolled_window", Scrolled_window);
    builder->get_widget("Viewport",        Viewport);
    builder->get_widget("Add_button",      Add_button);

    Pass_box = new PassBox();

    set_titlebar(*Header_bar);
    add(*Scrolled_window);

    Viewport->add(*Pass_box);

    Scrolled_window->show();
    Header_bar->show();
    Pass_box->show();

    Add_button     ->signal_clicked().connect( sigc::mem_fun(*this, &PassesWindow::open_file_chooser) );

    on_load();
  }

  /*!
   * \brief PassesWindow::open_file_chooser
   */
  void PassesWindow::open_file_chooser()
  {
    auto dialog = Gtk::FileChooserDialog(*this, "Open pass", Gtk::FileChooserAction::FILE_CHOOSER_ACTION_OPEN);
    dialog.set_transient_for(*this);
    dialog.set_modal(true);
    dialog.signal_response().connect(sigc::bind(
      sigc::mem_fun(*this, &PassesWindow::on_file_dialog_response), &dialog));

    //Add response buttons to the dialog:
    dialog.add_button("Cancel", Gtk::ResponseType::RESPONSE_CANCEL);
    dialog.add_button("Open", Gtk::ResponseType::RESPONSE_OK);

    auto filter = Gtk::FileFilter::create();
    filter->set_name("PkPass files");
    filter->add_mime_type("application/zip");
    filter->add_pattern("*.pkpass");
    dialog.add_filter(filter);

    dialog.run();
  }

  /*!
   * \brief PassesWindow::on_file_dialog_response
   * \param response_id
   * \param dialog
   */
  void PassesWindow::on_file_dialog_response(int response_id, Gtk::FileChooserDialog *dialog)
  {
    /// \todo make path member variable
    const string passes_path = string(getenv("HOME")) + "/.local/share/pkpass/passes/";

    switch (response_id)
      {
      case Gtk::ResponseType::RESPONSE_OK:
        {
          auto filename = dialog->get_file()->get_path();

          try
          {
            int creationFlags = pkpass::IPass::ECreationFlags::ALLOW_UNSUPPORTED_CERTIFICATE_PURPOSE |
                                pkpass::IPass::ECreationFlags::VERIFY_MANIFEST;//|
            /// \todo fix missing cert
                                //pkpass::IPass::ECreationFlags::VERIFY_SIGNATURE;
            auto pass = pkpass::IPass::create(filename, creationFlags);

            if(!filesystem::exists(pass->get_storage_directory()))
              Pass_box->add(pass);
            else
              {
                dialog->hide();
                Gtk::MessageDialog("Pass already exists", false, Gtk::MESSAGE_INFO).run();
              }
          }
          catch (pkpass::IException& e)
          {
            Gtk::MessageDialog(e.what(),false,Gtk::MESSAGE_ERROR).run();
          }

          break;
        }
      default:
        break;
      }
  }

  /*!
   * \brief PassesWindow::on_load
   */
  void PassesWindow::on_load()
  {
    const string passes_path = string(getenv("HOME")) + "/.local/share/pkpass/passes/";

    if(!filesystem::exists(passes_path))
      return;

    for (const auto & entry : filesystem::directory_iterator(passes_path))
      {
        try
        {
          int creationFlags = pkpass::IPass::ECreationFlags::ALLOW_UNSUPPORTED_CERTIFICATE_PURPOSE |
                              pkpass::IPass::ECreationFlags::VERIFY_MANIFEST |
              /// \todo fix missing cert
                              //pkpass::IPass::ECreationFlags::VERIFY_SIGNATURE |
                              pkpass::IPass::ECreationFlags::LOAD_FROM_DIR;
          auto pass = pkpass::IPass::create(string(entry.path())+"/", creationFlags);

          Pass_box->add(pass);
        }
        catch (pkpass::IException& e)
        {
          Gtk::MessageDialog(e.what(),false,Gtk::MESSAGE_ERROR).run();
        }
      }
  }
}
