#ifndef BARCODEGEN_QRCODE_H
#define BARCODEGEN_QRCODE_H

#include "libbarcodegen.h"
#include "qrencoding.h"

namespace barcodegen
{
  /*! \internal
   * \brief Class which implements logic for creating Qr barcodes.
   * \sa https://en.wikipedia.org/wiki/QR_code
   */
  class Qr : virtual public IQr
  {
  public:
    Qr (string           data,
        EErrorCorrection ecc_lvl = IQr::EErrorCorrection::MEDIUM);

    virtual string get_data      () { return string(Data.begin(), Data.end()); };
    virtual string to_svg_string (uint32_t quiet_zone);
    virtual bits   get_bits      ();
    vector<bits>   get_bit_array ();

  protected:
    /*! \brief Encoded data in byte codewords (incl error correction) */
    u32string             Data;
    /*! \brief The encoding for QR codes. */
    QrEncoding            qrEncoding;
    /*! \brief The level of error correction (to be) used. */
    IQr::EErrorCorrection Ecc_lvl;

    void add_ecc_and_interleave_data ();

    void add_finder_pattern   (vector<bits>& bits_in,
                               vector<bits>& set_in);
    void add_timing_pattern   (vector<bits>& bits_in,
                               vector<bits>& set_in);
    void add_aligment_pattern (vector<bits>& bits_in,
                               vector<bits>& set_in);
    void add_dark_module      (vector<bits>& bits_in,
                               vector<bits>& set_in);
    void add_bits             (vector<bits>& bits_in,
                               vector<bits>& set_in);
    void reserve_areas        (vector<bits>& set_in);

    uint32_t find_best_mask      (vector<bits>  bits_in,
                                  vector<bits>  set_in);
    vector<bits> apply_mask      (vector<bits>  bits_in,
                                  vector<bits>  set_in,
                                  uint8_t       mask);
    void add_format_version_info (vector<bits>& bits_in,
                                  uint8_t       mask);
    uint32_t get_penalty_score   (vector<bits>  bits_in);

  };
}

#endif // BARCODEGEN_QRCODE_H
