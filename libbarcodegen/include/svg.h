#ifndef SVG_H
#define SVG_H

#include "libbarcodegen.h"

namespace barcodegen
{
  string create_svg_string(bits     bits,
                           uint32_t quiet_zone);
}

#endif // SVG_H
