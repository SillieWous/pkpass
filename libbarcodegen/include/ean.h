#ifndef EAN_H
#define EAN_H

#include "libbarcodegen.h"

#include <map>

namespace barcodegen
{
  /*! \internal
   * \brief Class which implements logic for creating EAN barcodes.
   * \sa https://en.wikipedia.org/wiki/International_Article_Number
   */
  class EAN : virtual public IEAN
  {
  public:
    EAN (uint8_t*              digits,
         size_t                number_of_digits,
         map<uint8_t, uint8_t> patterns);

    string get_data      () { return Data; }
    string to_svg_string (uint32_t quiet_zone);
    bits   get_bits      ();

    uint8_t get_digit    (uint8_t position);
    uint8_t get_checksum ();

  protected:
    /*! \brief Encoded data */
    string  Data;
    /*! \brief Pattern encoding if L-code (0 bit) or G-code  (1 bit) is to be used. */
    map<uint8_t, uint8_t> Patterns;

    string add_patterns();

    // Encoding of the digits.
    const map<uint8_t, uint8_t> L_code  = { {0, 0x0D},
                                            {1, 0x19},
                                            {2, 0x13},
                                            {3, 0x3D},
                                            {4, 0x23},
                                            {5, 0x31},
                                            {6, 0x2F},
                                            {7, 0x3B},
                                            {8, 0x37},
                                            {9, 0x0B} };
    const map<uint8_t, uint8_t> G_code  = { {0, 0x27},
                                            {1, 0x33},
                                            {2, 0x1B},
                                            {3, 0x21},
                                            {4, 0x1D},
                                            {5, 0x39},
                                            {6, 0x05},
                                            {7, 0x11},
                                            {8, 0x09},
                                            {9, 0x17} };

  };

  /*! \internal
   * \brief Class for creating EAN13 barcodes.
   *        Logic is implemented in generic EAN class.
   */
  class EAN13 : virtual public EAN
  {
  public:
    EAN13(uint8_t digits[12])
      : EAN(digits, 12,
           { {0, 0x00},
             {1, 0x0B},
             {2, 0x0D},
             {3, 0x0E},
             {4, 0x13},
             {5, 0x19},
             {6, 0x1C},
             {7, 0x15},
             {8, 0x16},
             {9, 0x1A} })
    {}
  };

  /*! \internal
   * \brief Class for creating EAN8 barcodes.
   *        Logic is implemented in generic EAN class.
   */
  class EAN8 : virtual public EAN
  {
  public:
    EAN8 (uint8_t digits[8])
      : EAN(digits, 8,
            { {0, 0x00},
              {1, 0x00},
              {2, 0x00},
              {3, 0x00},
              {4, 0x00},
              {5, 0x00},
              {6, 0x00},
              {7, 0x00},
              {8, 0x00},
              {9, 0x00} })
    {}
  };
}

#endif // EAN_H
