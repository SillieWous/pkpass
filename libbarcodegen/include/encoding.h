#ifndef ENCODING_H
#define ENCODING_H


#include "libbarcodegen.h"

#include <string>
#include <map>
#include <vector>

#include <boost/property_tree/ptree_fwd.hpp>


#include <filesystem>

#ifndef DEBUG
#if defined(_MSC_VER) || defined(WIN64) || defined(_WIN64) || defined(__WIN64__) || defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
const std::string Encoding_storage_dir     = std::string(getenv("APPDATA")) +"/local/barcodegen/";
#else
const std::string Encoding_storage_dir     = std::string(getenv("HOME")) +"/.local/share/barcodegen/";
#endif
#else
const std::string Encoding_storage_dir     = "./";
#endif

using namespace std;

namespace barcodegen
{
  /*! \internal
   * \brief Class used for minimising the number of codewords used in a barcode
   *        Barcode codesets are specified in .xml files
   */
  class Encoding
  {
  public:
    /*! \internal
     *  \brief The cost_t struct is used to evaluate cost to encode a data symbols, given a codeset.
     */
    struct cost_t
    {
      double cost;
      size_t idx;
      size_t prev_idx;
      bool   shift;
    };

    /*! \internal
     * \brief costmap_t is a matrix containing the encoding cost.
     *        The first index is the symbol index, the second index the different codesets.
     */
    typedef vector<vector<cost_t>> costmap_t;

    Encoding (string xmlFile);

    bits      get_bits       (u32string data);
    bits      get_bits       (u32string data,
                              uint32_t  table);
    u32string encode_message (string    data,
                              string    start_codeset = "");

  protected:
    /*! \brief The ESwitchType enum is used to indicate either a permanent switch or a temporary shift */
    enum class ESwitchType
    {
      SHIFT,
      SWITCH
    };

    /*! \internal
     *  \brief The codeset_t struct defines a codeset used for encoding.
     *
     * A codeset contains information about how each symbol is encoding. The encoding is contained in a map,
     * which maps a symbol to a code. A symbol may contain multiple bytes, which is specified by the symbol_size.
     * Similarly each code may span multiple codewords. e.g. for the Alphanumeric codeset of a QR code, there are
     * 45 codes out of a codeword size of 256. The symbol_cost for the Alphanumeric codeset will be log(45)/log(256)
     * ~ 0.6865 [codewords/code].
     */
    struct codeset_t
    {
      map<string, u32string> codes;
      string                 name;
      string                 sub_mode_of;
      uint32_t               symbol_size;
      double                 symbol_cost;
    };

    costmap_t get_message_cost      (string    data,
                                     string    start_codeset = "");
    virtual u32string minimise_cost (string    data,
                                     costmap_t costmap);

    u32string encoding_cost (size_t prev_codeset,
                             size_t codeset,
                             string symbol);
    u32string shift_cost    (size_t codeset,
                             string symbol);

    codeset_t   parse_codeset (boost::property_tree::ptree property_tree,
                             uint32_t                    parent_base);

    /*! \brief The codesets for this particular encoding */
    vector<codeset_t>                                       Codesets;
    /*! \brief A mapping from (prev_codeset, current_codeset, symbol) to an encoding. */
    map<tuple<size_t, size_t, string>, u32string>           EncodingCostMap;
    /*! \brief Codes used to switch an shift modes. */
    map<tuple<string, string, ESwitchType>, u32string>      Switches;
    /*! \brief A mapping from (code, table) to (pattern, pattern_length) */
    map<pair<uint32_t, uint32_t>, pair<uint32_t, uint32_t>> Bitmap;

  private:
  };
}

#endif // ENCODING_H
