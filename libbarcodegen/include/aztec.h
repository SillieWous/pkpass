#ifndef BARCODEGEN_AZTEC_H
#define BARCODEGEN_AZTEC_H

#include "libbarcodegen.h"
#include "encoding.h"

namespace barcodegen
{
  /*! \internal
   * \brief Class which implements logic for creating Aztec barcodes.
   * \sa https://en.wikipedia.org/wiki/Aztec_Code
   */
  class Aztec : virtual public IBarcode
  {
  public:
    Aztec (string data);

    virtual string get_data      () { return string(Data.begin(), Data.end()); }
    virtual string to_svg_string (uint32_t quiet_zone);
    virtual bits   get_bits      ();

    uint8_t get_checksum();

  protected:
    /*! \brief Encoded data. */
    u32string        Data;
    /*! \brief Data converted to codewords. */
    vector<uint32_t> Code_words;
    /*! \brief Error correction codewords. */
    vector<uint32_t> ECC_words;
    /*! \brief Words making up the mode message (incl error correction). */
    vector<uint32_t> Mode_words;
    /*! \brief The encoding for Aztec codes. */
    Encoding         Aztec_encoding;
    /*! \brief The number of layers the code is made up of. */
    uint32_t         Number_of_layers;
    /*! \brief The total number of coderwords used. */
    uint32_t         Number_of_codewords;
    /*! \brief Number of bits used for each codewords. */
    uint32_t         Codeword_size;
    /*! \brief Nuber of error correction codewords. */
    uint32_t         ECC_size;

    bits get_mode_bits ();

  private:
    uint32_t calculate_number_of_layers    ();
    uint32_t calculate_number_of_codewords ();
    void     data_to_codewords             ();
    void     bitstuff_codewords            ();
    void     add_ecc_codewords             ();
    void     add_modewords                 ();
  };
}

#endif // BARCODEGEN_AZTEC_H
