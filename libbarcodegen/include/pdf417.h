﻿#ifndef BARCODEGEN_PDF417_H
#define BARCODEGEN_PDF417_H

#include "libbarcodegen.h"
#include "encoding.h"

namespace barcodegen
{
  /*! \internal
   * \brief Class which implements logic for creating Pdf417 barcodes.
   * \sa https://en.wikipedia.org/wiki/PDF417
   */
  class Pdf417 : virtual public IPdf417
  {
  public:
    Pdf417(string data);

    /*!
     * \brief get_data
     * \return
     * \todo do not return empty string
     */
    virtual string get_data      () { return ""; };
    virtual string to_svg_string (uint32_t quiet_zone);
    virtual string to_svg_string (uint32_t columns,
                                  uint32_t quiet_zone);
    /*!
     * \brief get_bits
     * \return
     * \todo do not return empty bits
     */
    virtual bits   get_bits      () { return bits(); };

  protected:
    /*! \brief Encoded data */
    u32string Data;
    /*! \brief The encoding for PDF417 barcodes. */
    Encoding  PDF417Encoding;

    const map<char32_t, pair<string, uint32_t>> Mode_words = { {900, {"Text",    30} },
                                                               {901, {"Byte",   256} },
                                                               {902, {"Numeric", 10} } };

    static vector<uint32_t> get_check_words     (u32string message,
                                                 uint32_t  redundant_words,
                                                 int32_t   prime);
    static vector<uint32_t> generate_polynomial (uint32_t  redundant_words,
                                                 int32_t   prime);

  private:
    size_t compress_text_mode    (size_t start_idx,
                                  size_t end_idx);
    size_t compress_byte_mode    (size_t start_idx,
                                  size_t end_idx);
    size_t compress_numeric_mode (size_t start_idx,
                                  size_t end_idx);
  };
} // namespace barcodegen

#endif // BARCODEGEN_PDF417_H
