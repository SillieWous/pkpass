#ifndef EXCEPTION_H
#define EXCEPTION_H

#define ArgumentException(what)       barcodegen::IException(what, __FILE__, __FUNCTION__, __LINE__, barcodegen::IException::Exception_t::Argument)
#define InternalException(what)       barcodegen::IException(what, __FILE__, __FUNCTION__, __LINE__, barcodegen::IException::Exception_t::Internal)
#define UnsupportedException(what)    barcodegen::IException(what, __FILE__, __FUNCTION__, __LINE__, barcodegen::IException::Exception_t::Unsupported)
#define TimeoutException(what)        barcodegen::IException(what, __FILE__, __FUNCTION__, __LINE__, barcodegen::IException::Exception_t::Timeout)
#define NotImplementedException(what) barcodegen::IException(what, __FILE__, __FUNCTION__, __LINE__, barcodegen::IException::Exception_t::NotImplemented)
#define NotFoundException(what)       barcodegen::IException(what, __FILE__, __FUNCTION__, __LINE__, barcodegen::IException::Exception_t::NotFound)

#endif // EXCEPTION_H
