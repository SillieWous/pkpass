#ifndef REEDSOLOMON_H
#define REEDSOLOMON_H

#include "libbarcodegen.h"

#include <vector>

namespace barcodegen
{
  /*! \internal
   * \brief Galois field uint32_t.
   * \sa http://www.ee.unb.ca/cgi-bin/tervo/galois3.pl?p=4&A=0
   * \sa http://www.ee.unb.ca/cgi-bin/tervo/calc2.pl?p=4
   */
  struct gfuint32_t
  {
    /*! \brief Value of the Galois integer. */
    uint32_t Value;
    /*! \brief Prime of the Galois field. */
    uint32_t Prime;
    /*! \brief Maximum value of the Galois field. */
    uint32_t Symbol_size;

    /*!
     * \brief gfuint32_t constructor
     * \param value Value of the Galois integer.
     * \param prime Prime of the Galois field.
     * \param symbol_size Maximum value of the Galois field.
     */
    gfuint32_t(uint32_t value, uint32_t prime, uint32_t symbol_size)
    {
      Value = value;
      Prime = prime;
      Symbol_size = symbol_size;
    }

    gfuint32_t operator+ (const gfuint32_t& b);
    gfuint32_t operator- (const gfuint32_t& b);
    gfuint32_t operator* (const gfuint32_t& b);
    //gfuint32_t operator/ (const gfuint32_t& b);

    gfuint32_t& operator+= (const gfuint32_t& b);
    gfuint32_t& operator-= (const gfuint32_t& b);
    gfuint32_t& operator*= (const gfuint32_t& b);
    //gfuint32_t& operator/= (const gfuint32_t& b);

    gfuint32_t& operator++ ();
    gfuint32_t& operator-- ();

    bool operator==(const gfuint32_t& b) {return (this->Value==b.Value && this->Prime==b.Prime); }
    bool operator!=(const gfuint32_t& b) {return (this->Value!=b.Value || this->Prime!=b.Prime); }
  };

  /*! \internal
   * \brief (static) Class which implements functions to generate ECC words from a message.
   */
  class ReedSolomon
  {
  public:
    static u32string        get_check_words (u32string        message,
                                             uint8_t          message_bits,
                                             uint32_t         redundant_words,
                                             uint32_t         prime,
                                             bool             modulo_poly = false);
    static vector<uint32_t> get_check_words (vector<uint32_t> message,
                                             uint8_t          message_bits,
                                             uint32_t         redundant_words,
                                             uint32_t         prime,
                                             bool             modulo_poly = false);

  protected:
    static vector<gfuint32_t> generate_polynomial (uint32_t k,
                                                   uint32_t n,
                                                   uint32_t m,
                                                   uint32_t prime,
                                                   bool     modulo_poly);
  };
}

#endif // REEDSOLOMON_H
