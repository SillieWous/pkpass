#ifndef LIBBARCODEGEN_H
#define LIBBARCODEGEN_H

#if defined(_MSC_VER) || defined(WIN64) || defined(_WIN64) || defined(__WIN64__) || defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#  define Q_DECL_EXPORT __declspec(dllexport)
#  define Q_DECL_IMPORT __declspec(dllimport)
#else
#  define Q_DECL_EXPORT     __attribute__((visibility("default")))
#  define Q_DECL_IMPORT     __attribute__((visibility("default")))
#endif

#if defined(LIBBARCODEGEN_LIBRARY)
#  define LIBBARCODEGEN_EXPORT Q_DECL_EXPORT
#else
#  define LIBBARCODEGEN_EXPORT Q_DECL_IMPORT
#endif

#include <string>
#include <vector>
#include <memory>

/// \brief Main namespace
namespace barcodegen
{
  using namespace std;

  /*! \brief Vector of booleans used to represent bits in a barcode */
  typedef vector<bool> bits;

  /*!
   * \brief Format specifier for barcode types.
   *        UNSUPPORTED: CODE39, DATA_MATRIX, ITF
   * \todo Add support for CODE39, DATA_MATRIX, ITF
   */
  enum class EFormat
  {
    AZTEC,       ///< https://en.wikipedia.org/wiki/Aztec_Code
    CODE128,     ///< https://en.wikipedia.org/wiki/Code_128
    CODE39,      ///< https://en.wikipedia.org/wiki/Code_39
    DATA_MATRIX, ///< https://en.wikipedia.org/wiki/Data_Matrix
    EAN13,       ///< https://en.wikipedia.org/wiki/International_Article_Number
    EAN8,        ///< https://en.wikipedia.org/wiki/EAN-8
    ITF,         ///< https://en.wikipedia.org/wiki/Interleaved_2_of_5
    PDF417,      ///< https://en.wikipedia.org/wiki/PDF417
    QR,          ///< https://en.wikipedia.org/wiki/QR_code
  };

  /*!
   * \brief Interface for creating and interacting with generic barcodes.
   *
   * The IBarcode interface is used to generate generic barcodes. All formats are supported, however for some it is
   * recommended to use format specific interfaces. Format specific interfaces offer a more flexible way of interacting
   * with the barcode.
   *
   * \todo Support GS1 and ECI barcodes
   */
  class LIBBARCODEGEN_EXPORT IBarcode
  {
  public:
    /*!
     * \brief Barcode generation function.
     * \param format Type of barcode to be created.
     * \param data Data to be encoded by the barcode.
     * \return Barcode of the specified format and data.
     * \exception IException::Exception_t::Unsupported Not all formats are currently supported.
     */
    static shared_ptr<IBarcode> create(EFormat format, string data);

    /*!
     * \brief Getter function for the encoded data.
     * \return Encoded data of the barcode.
     */
    virtual string get_data() = 0;

    /*!
     * \brief Function to generate a string that can be used as an image of the barcode
     * \param quiet_zone Size of additional empty space to be added to the barcode.
     *                   e.g. 1 will add a one module thick ring of empty space around the barcode
     * \return A string in svg format of the barcode.
     */
    virtual string to_svg_string(uint32_t quiet_zone) = 0;

    /*!
     * \brief Getter function for the black/white barcode pattern.
     * \return The ones and zeros which construct the barcode.
     */
    virtual bits   get_bits() = 0;
  };

  /*!
   * \brief Interface for creating and interacting with EAN barcodes.
   *
   * The format specific IEAN interface extends the IBarcode interface with EAN specific functions. It allows to get
   * digits of the barcode by position and to get the checksum digit of the barcode.
   *
   * Supported formats: EAN8, EAN13
   */
  class LIBBARCODEGEN_EXPORT IEAN : virtual public IBarcode
  {
  public:
    /*!
     * \brief EAN barcode generation function.
     * \param format Type of EAN barcode to be created.
     * \param data Data to be encoded by the barcode.
     * \return EAN barcode of the specified format and data.
     * \exception IException::Exception_t::Argument Data may only consist of digits.
     * \exception IException::Exception_t::Unsupported Only EAN* formats supported.
     */
    static shared_ptr<IEAN> create(EFormat format, string data);

    /*!
     * \brief Get a specific digit of the barcode (checksum excluded).
     * \param position Position index of the requested digit.
     * \return Digit at requested position.
     */
    virtual uint8_t get_digit(uint8_t position) = 0;

    /*!
     * \brief Get the calculated checksum digit of the barcode.
     * \return The checksum digit of the barcode.
     */
    virtual uint8_t get_checksum() = 0;
  };

  /*!
   * \brief Interface for creating and interacting with Pdf417 barcodes.
   */
  class LIBBARCODEGEN_EXPORT IPdf417 : virtual public IBarcode
  {
  public:
    /*!
     * \brief PDF417 barcode generation function.
     * \param data Data to be encoded by the barcode.
     * \return PDF417 barcode of the specified format and data.
     */
    static shared_ptr<IPdf417> create(string data);

    using IBarcode::to_svg_string;

    /*!
     * \brief Function to generate a string that can be used as an image of the barcode.
     * \param columns Number of data columns to use for the barcode.
     *                If not used, value 1 column is generated.
     * \param quiet_zone Size of additional empty space to be added to the barcode,
     *                   e.g. 1 will add a one module thick ring of empty space around the barcode.
     * \return
     */
    virtual string to_svg_string(uint32_t columns, uint32_t quiet_zone) = 0;
  };

  /*!
   * \brief Interface for creating and interacting with Qr barcodes.
   */
  class LIBBARCODEGEN_EXPORT IQr : virtual public IBarcode
  {
  public:
    /*!
     * \brief Specifier for the error correction level.
     *        LOW: ~7%
     *        MEDIUM: ~15%
     *        QUARTILE: ~25%
     *        HIGH: ~30%
     */
    enum class EErrorCorrection
    {
      LOW,
      MEDIUM,
      QUARTILE,
      HIGH
    };

    /*!
     * \brief QR barcode generation function.
     * \param data Data to be encoded by the barcode.
     * \param ecc_lvl Error correction level to use for generating the barcode.
     *                Default value: MEDIUM.
     * \return QR barcode of the specified format and data.
     */
    static shared_ptr<IQr> create(string data, EErrorCorrection ecc_lvl = EErrorCorrection::MEDIUM);
  };

  /*!
   * \brief Exception interface.
   */
  class LIBBARCODEGEN_EXPORT IException
  {
  public:
    /*!
     * \brief Possible types of IExceptions.
     */
    enum class Exception_t
    {
        Argument,       ///< Invalid argument received.
        Internal,       ///< Something went wrong internally.
        Unsupported,    ///< Functionality is not supported.
        Timeout,        ///< Execution took too long.
        NotImplemented, ///< Functionality is not implemented.
        NotFound        ///< Something that is needed is not found, e.g. a file.
    };

    /*!
     * \brief Constructor for IException class.
     * \param what Description of what the exception is about.
     * \param file File that caused the exception.
     * \param function Function that caused the exception.
     * \param line Line that caused the exception.
     * \param type Type of exception being thrown.
     */
    IException(string what, string file, string function, int line, Exception_t type)
      : What(what),
        Type(type)
    {
        Where = "File:" + file + " Function:" + function + " Line:" + to_string(line);
    }

    /*!
     * \brief Get detailed information about the exception.
     * \return Description of what the exception is about.
     */
    string      what() { return What; }

    /*!
     * \brief Get where the exception occured.
     * \return Description of where the exception occured.
     */
    string      where(){ return Where; }

    /*!
     * \brief Get the type of the exception
     * \return Type of the exception.
     */
    Exception_t type() { return Type; }

  protected:
    /*!
     * \brief Description of what the exception is about.
     */
    string      What;
    /*!
     * \brief Description of where the exception occured.
     */
    string      Where;
    /*!
     * \brief Type of the exception.
     */
    Exception_t Type;
  };
}

#endif // LIBBARCODEGEN_H
