#ifndef BARCODEGEN_CODE128_H
#define BARCODEGEN_CODE128_H

#include "libbarcodegen.h"
#include "encoding.h"

#include <map>
#include <tuple>

namespace barcodegen
{
  /*! \internal
   * \brief Class which implements logic for creating Code128 barcodes.
   * \sa https://en.wikipedia.org/wiki/Code_128
   */
  class Code128 : virtual public IBarcode
  {
  public:
    Code128 (string data);

    virtual string get_data      () { return string(Data.begin(), Data.end()); }
    virtual string to_svg_string (uint32_t quiet_zone);
    virtual bits   get_bits      ();

    uint8_t get_checksum ();

  protected:
    /*! \brief Encoded data */
    u32string Data;
    /*! \brief The encoding for Code128 barcodes */
    Encoding  Code128Encoding;
  };
}

#endif // BARCODEGEN_CODE128_H


