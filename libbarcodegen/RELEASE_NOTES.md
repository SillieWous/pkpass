# Release notes

### Release 0.1.0
**DATE**\n
*Initial release. Adds support for Code128, EAN13, EAN8, PDF417 and Qr barcodes.*

#### Features
* Added IBarcode, IEAN, IPdf417, and IQR interfaces for generating barcodes.
* Added IException class, used for exception handling.

#### Bug fixes
*Nothing to show here*

#### Deprecated functionality
*Nothing to show here*
