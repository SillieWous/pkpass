#include "reedsolomon.h"

#include "exception.h"

namespace barcodegen
{
  /*!
   * \brief Addition opperator for gfuint32_t
   * \param b Value to be added.
   * \return Galois field sum of two values.
   * \exception IException::Exception_t::Internal If other value is from a different Galois field.
   */
  gfuint32_t gfuint32_t::operator+(const gfuint32_t &b)
  {
    if(this->Prime != b.Prime)
      throw InternalException("cannot add gf integers with different primes");

    if(this->Symbol_size != b.Symbol_size)
      throw InternalException("cannot add gf integers with different symbol sizes");

    return gfuint32_t(this->Value^b.Value, this->Prime, this->Symbol_size);
  }

  /*!
   * \brief Subtraction opperator for gfuint32_t
   * \param b Value to be subtracted.
   * \return Galois field subtraction of two values.
   * \exception IException::Exception_t::Internal If other value is from a different Galois field.
   */
  gfuint32_t gfuint32_t::operator-(const gfuint32_t &b)
  {
    if(this->Prime != b.Prime)
      throw InternalException("cannot subtract gf integers with different primes");

    if(this->Symbol_size != b.Symbol_size)
      throw InternalException("cannot subtract gf integers with different symbol sizes");

    return gfuint32_t(this->Value^b.Value, this->Prime, this->Symbol_size);
  }

  /*!
   * \brief Multiplication operator for gfuint32_t
   * \param b Value to be multiplied by.
   * \return Galois field multiplication of two values.
   * \exception IException::Exception_t::Internal If other value is from a different Galois field.
   */
  gfuint32_t gfuint32_t::operator*(const gfuint32_t &b)
  {
    if(this->Prime != b.Prime)
      throw InternalException("cannot multiply gf integers with different primes");

    uint32_t prime = this->Prime;

    if(this->Symbol_size != b.Symbol_size)
      throw InternalException("cannot multiply gf integers with different symbol sizes");

    uint32_t symbol_size= this->Symbol_size;

    uint32_t res = 0; // initialize result
    uint32_t aval = this->Value;
    uint32_t bval = b.Value;

    while (aval != 0 && bval != 0)
      {
        if (aval & 1)
          res = (res ^ bval);

        aval  >>= 1;
        bval  <<= 1;

        if(bval >= symbol_size)
          bval ^= prime;
      }


    return gfuint32_t(res, prime, symbol_size);
  }

  /*!
   * \brief Addition asignment operator for gfuint32_t
   * \param b Value to be added.
   * \return Galois field sum of two values.
   * \exception IException::Exception_t::Internal If other value is from a different Galois field.
   */
  gfuint32_t& gfuint32_t::operator+=(const gfuint32_t &b)
  {
    auto val = (*this)+b;
    this->Value = val.Value;
    return *this;
  }

  /*!
   * \brief Subtraction asignment operator for gfuint32_t
   * \param b Value to be subtracted.
   * \return Galois field subtraction of two values.
   * \exception IException::Exception_t::Internal If other value is from a different Galois field.
   */
  gfuint32_t& gfuint32_t::operator-=(const gfuint32_t &b)
  {
    auto val = (*this)-b;
    this->Value = val.Value;
    return *this;
  }

  /*!
   * \brief Multiplication assignment operator for gfuint32_t
   * \param b Value to be multiplied by.
   * \return Galois field multiplication of two values.
   * \exception IException::Exception_t::Internal If other value is from a different Galois field.
   */
  gfuint32_t &gfuint32_t::operator*=(const gfuint32_t &b)
  {
    auto val = (*this)*b;
    this->Value = val.Value;
    return *this;
  }

  /*!
   * \brief Increment operator for gfuint32_t
   * \return Incremented gfuint32_t.
   */
  gfuint32_t &gfuint32_t::operator++()
  {
    this->Value ^= 1;
    return *this;
  }

  /*!
   * \brief Decrement operator for gfuint32_t
   * \return Decremented gfuint32_t.
   */
  gfuint32_t &gfuint32_t::operator--()
  {
    this->Value ^= 1;
    return *this;
  }


  /*!
   * \brief Function to retrieve checkwords for a message.
   * \param message Message to determine the checkwords for as u32string.
   * \param message_bits Number of bits a message or check codeword may contain.
   * \param redundant_words Number of redundant words to calculate.
   * \param prime Prime polynomial to use for the Galoi field.
   * \param modulo_poly TBD
   * \return Reed Solomon check words as a u32string.
   */
  u32string ReedSolomon::get_check_words(u32string message, uint8_t message_bits, uint32_t redundant_words, uint32_t prime, bool modulo_poly)
  {
    vector<uint32_t> message_int(message.begin(), message.end());
    auto check_words = get_check_words(message_int, message_bits, redundant_words, prime, modulo_poly);

    return u32string(check_words.begin(), check_words.end());
  }

  /*!
   * \brief Function to retrieve checkwords for a message.
   * \param message Message to determine the checkwords for as a vector of uint32_t.
   * \param message_bits Number of bits a message or check codeword may contain.
   * \param redundant_words Number of redundant words to calculate.
   * \param prime Prime polynomial to use for the Galoi field.
   * \param modulo_poly TBD
   * \return Reed Solomon check words as a vector of uint32_t.
   */
  vector<uint32_t> ReedSolomon::get_check_words(vector<uint32_t> message, uint8_t message_bits, uint32_t redundant_words, uint32_t prime, bool modulo_poly)
  {
    // generate polynomial, convolute
    uint32_t k = message.size();
    uint32_t n = k + redundant_words;
    uint32_t m = 2 << (message_bits-1);

    auto g = generate_polynomial(k, n, m, prime, modulo_poly);

    // pad message
    vector<gfuint32_t> msg;
    for(uint32_t i=0; i<n; i++)
      {
        if(i<k)
          msg.push_back(gfuint32_t(message[i], prime, m));
        else
          msg.push_back(gfuint32_t(0, prime, m));
      }

    // calculate remainder, deconvolute
    for(uint32_t i=0; i<k; i++)
      {
        auto multiplier = msg[i];
        for(size_t j=i+1; j<(i+g.size()); j++)
            msg[j] += (g[j-i]*multiplier);
      }

    vector<uint32_t> result;
    for(uint32_t i=0; i<n-k; i++)
      result.push_back(msg[k+i].Value);

    return result;
  }

  /*!
   * \brief Generator function for the characteristic polynomial
   * \param arg_k Number of codewords
   * \param arg_n Number of total words (code + check)
   * \param arg_m Maximum value of the Galois field. 2^(x-1)
   * \param prime Prime polynomial of the Galois field.
   * \param modulo_poly TBD
   * \return Characteristic polynomial for the given field and number of words.
   * \todo Find out why/when modulo_poly is needed
   * \todo Add checks for input arguments
   */
  vector<gfuint32_t> ReedSolomon::generate_polynomial(uint32_t arg_k, uint32_t arg_n, uint32_t arg_m, uint32_t prime, bool modulo_poly)
  {
    vector<gfuint32_t> g = {gfuint32_t(1, prime, arg_m)};

    const gfuint32_t alpha_mult(2, prime, arg_m);
    vector<gfuint32_t> alpha = {gfuint32_t(1, prime, arg_m), alpha_mult};


    for(uint32_t i=0; i<arg_n-arg_k; i++)
      {
        vector<gfuint32_t> w;
        const auto u = g;
        const auto v = alpha;

        const int32_t m = u.size();
        const int32_t n = v.size();

        const int32_t w_size = m+n-1;

        for(int32_t k=0; k<w_size; k++)
          {
            const auto jstart = (k-n+1 > 0 ? k-n+1 : 0);
            const auto jend   = (k+1 < m   ? k+1   : m);

            gfuint32_t tempj(0, prime, arg_m);
            for(int32_t j=jstart; j<jend; j++)
              {
                tempj += g[j]*alpha[k-j];
              }
            w.push_back(tempj);
          }

        g = w;

        alpha[1] *= alpha_mult;

        if(i==(arg_n-arg_k)-2 && modulo_poly)
          alpha[1] = gfuint32_t(1, prime, arg_m);
      }

    return g;
  }
}
