#include "pdf417.h"

#include "reedsolomon.h"

#include <iostream>
#include <math.h>

namespace barcodegen
{
  /*!
   * \brief Pdf417::Pdf417 constructor
   * \param data Data to be encoded.
   */
  Pdf417::Pdf417(string data)
    : PDF417Encoding("pdf417.xml")
  {
    /// \todo first be a possibly be a switch
    Data = PDF417Encoding.encode_message(data, "Text_Upper");

    /// \todo check if Text_upper
    if(!Mode_words.count(Data[0]))
      throw runtime_error("no start mode word");

    auto   mode      = Mode_words.at(Data[0]);
    size_t start_idx = 0;

    size_t i = 1;
    while(i<Data.size())
      {
        auto cw = Data[i];
        if(Mode_words.count(cw))
          {
            if(mode.first == "Text")
              i = compress_text_mode(start_idx+1,i-start_idx);
            if(mode.first == "Byte")
              i = compress_byte_mode(start_idx+1,i-start_idx);
            if(mode.first == "Numeric")
              i = compress_numeric_mode(start_idx+1,i-start_idx);

            mode = Mode_words.at(cw);
            start_idx = i;
            continue;
          }
        i++;
      }

    if(mode.first == "Text")
      compress_text_mode(start_idx+1,i-start_idx);
    if(mode.first == "Byte")
      compress_byte_mode(start_idx+1,i-start_idx);
    if(mode.first == "Numeric")
      compress_numeric_mode(start_idx+1,i-start_idx);

    Data[0] = Data.length();
  }

  /*!
   * \brief Function to generate a string that can be used as an image of the barcode
   * \param quiet_zone Size of additional empty space to be added to the barcode.
   *                   e.g. 1 will add a one module thick ring of empty space around the barcode
   * \return A string in svg format of the barcode.
   */
  string Pdf417::to_svg_string(uint32_t quiet_zone)
  {
    return to_svg_string(1, quiet_zone);
  }

  /*!
   * \brief Function to generate a string that can be used as an image of the barcode
   * \param columns Number of data codeword columns to use for generating the image.
   * \param q_zone Size of additional empty space to be added to the barcode.
   *               e.g. 1 will add a one module thick ring of empty space around the barcode
   * \return A string in svg format of the barcode.
   * \todo refactor into smaller functions
   */
  string Pdf417::to_svg_string(uint32_t columns, uint32_t q_zone)
  {
    uint32_t prime = 929;

    auto n = Data.length();
    auto sec_lvl =  (2 + (n>40) + (n>160) + (n>320));
    auto k = 2 << sec_lvl;

    auto checkwords = get_check_words(Data, k, prime);

    size_t nwords = checkwords.size() + Data.size();
    uint8_t rows = nwords/columns + (nwords%columns!=0);

    size_t padding = rows*columns - nwords;

    auto height = rows*3;
    auto width  = (columns+4)*17 + 1;

    string svg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                 "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"
                 "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 ";

    svg += to_string(width+2*q_zone) + " " + to_string(height+2*q_zone);

    svg += "\" stroke=\"none\">\n"
           "  <rect width=\"100%\" height=\"100%\" fill=\"#FFFFFF\"/>\n"
           "  <path d=\"";

    size_t idx=0;
    for(size_t i=0; i<rows; i++)
      {
        uint32_t startword, stopword;
        switch(i%3)
          {
          case 0:
            startword = (i/3)*30 + (rows-1)/3;
            stopword = (i/3)*30 + columns-1;
            break;
          case 1:
            startword = (i/3)*30 + sec_lvl*3 + (rows-1)%3;
            stopword = (i/3)*30 + (rows-1)/3;
            break;
          case 2:
            startword = (i/3)*30 + columns-1;
            stopword = (i/3)*30 + sec_lvl*3 + (rows-1)%3;
            break;
          }

        u32string row;
        row += startword;

        for(size_t j=0; j<columns; j++)
          {
            if(idx<n)
              row += Data[idx];
            else if(idx<n+padding)
              row += 900;
            else
              row += checkwords[idx-n-padding];

            idx++;
          }

        row += stopword;

        bits bits = PDF417Encoding.get_bits(row, (i%3) +1);
        for(size_t j=0; j<bits.size(); j++)
          {
            if(bits[j])
              svg += "M" + to_string(17+j+q_zone) + "," + to_string(i*3+q_zone) + "h1v3h-1z ";
          }

      }

    bits startbits = {1,1,1,1,1,1,1,1,0,1,0,1,0,1,0,0,0};
    bits stopbits  = {1,1,1,1,1,1,1,0,1,0,0,0,1,0,1,0,0,1};

    for(size_t j=0; j<startbits.size(); j++)
      {
        if(startbits[j])
          svg += "M" + to_string(j+q_zone) + "," + to_string(q_zone) + "h1v" + to_string(rows*3) + "h-1z ";
      }

    for(size_t j=0; j<stopbits.size(); j++)
      {
        if(stopbits[j])
          svg += "M" + to_string(j+17*(columns+3)+q_zone) + "," + to_string(q_zone) + "h1v" + to_string(rows*3) + "h-1z ";
      }

    svg += "\" fill=\"#000000\"/>\n"
           "</svg>";

    return svg;
  }

  /*!
   * \brief Calculates the error correction codewords from a message.
   * \param message Message to calculate error correction for.
   * \param redundant_words Number of error correction codewords to calculate.
   * \param prime Prime polynomial of the Galois field.
   * \return Vector containing the error correction codewords.
   */
  vector<uint32_t> Pdf417::get_check_words(u32string message, uint32_t redundant_words, int32_t prime)
  {
    auto poly = generate_polynomial(redundant_words, prime);

    vector<uint32_t> result(redundant_words, 0);

    for(size_t i=0; i<message.size(); i++)
      {
        uint32_t temp = (message[i] + result[0]) % prime;

        for(size_t j=0; j<redundant_words; j++)
            result[j] = j==redundant_words-1 ?
                  (              prime - ((temp * poly[j]) % prime)) % prime :
                  (result[j+1] + prime - ((temp * poly[j]) % prime)) % prime;
      }

    for(size_t i=0; i<redundant_words; i++)
        result[i] = (prime-result[i]) % prime;

    return result;
  }

  /*!
   * \brief Generator function for the characteristic polynomial
   * \param redundant_words Number of error correction codewords to calculate.
   * \param prime Prime polynomial of the Galois field.
   * \return The characteristic polynomial factors.
   */
  vector<uint32_t> Pdf417::generate_polynomial(uint32_t redundant_words, int32_t prime)
  {
    vector<uint32_t> g = { 1 };

    const uint32_t alpha_mult = 3;
    vector<int32_t> alpha = {1, alpha_mult };

    for(uint32_t i=0; i<redundant_words; i++)
      {
        vector<uint32_t> w;
        const auto u = g;
        const vector<int32_t> v = { alpha[0], -alpha[1] };

        const int32_t m = u.size();
        const int32_t n = v.size();

        const int32_t w_size = m+n-1;

        for(int32_t k=0; k<w_size; k++)
          {
            const auto jstart = (k-n+1 > 0 ? k-n+1 : 0);
            const auto jend   = (k+1 < m   ? k+1   : m);

            int32_t tempj = 0;
            for(int32_t j=jstart; j<jend; j++)
                tempj += g[j]*v[k-j];

            tempj = ( ( tempj % prime ) + prime ) % prime;
            w.push_back(tempj);
          }

        g = w;

        alpha[1] = (alpha[1]*alpha_mult)%prime;
      }

    g.erase(g.begin());
    return g;
  }

  /*!
   * \brief Stuffs two text codewords together.
   * \param start_idx Index of the first codeword in the data.
   * \param end_idx Index of the last codeword in the data.
   * \return New end index of the compressed data.
   */
  size_t Pdf417::compress_text_mode(size_t start_idx, size_t end_idx)
  {
    u32string replacement;
    auto nchars = (end_idx-start_idx);
    auto mod2   = nchars%2;

    for(size_t i=start_idx; i<end_idx-mod2; i+=2)
      {
        replacement.push_back(Data[i]*30 + Data[i+1]);
      }

    if(mod2)
      replacement += Data.substr(end_idx-mod2, mod2);

    Data.replace(start_idx, nchars, replacement);

    return start_idx+replacement.size();
  }

  /*!
   * \brief Stuffs 6 (or less) byte codewords together.
   * \param start_idx Index of the first codeword in the data.
   * \param end_idx Index of the last codeword in the data.
   * \return New end index of the compressed data.
   */
  size_t Pdf417::compress_byte_mode(size_t start_idx, size_t end_idx)
  {
    u32string replacement;
    auto nchars = (end_idx-start_idx);
    auto mod6   = nchars%6;

    // multiple of 6 bytes, change mode word
    if(!mod6)
      Data[start_idx-1] = 924;

    for(size_t i=start_idx; i<end_idx-mod6; i+=6)
      {
        auto str = Data.substr(i, 6);

        int64_t val = 0;
        for(uint8_t j=0; j<6; j++)
            val = (val<<8) + str[j];

        str = U"";
        while(val>0)
          {
            str.push_back(val%900);
            val = val/900;
          }
        reverse(str.begin(), str.end());

        replacement += str;
      }

    if(mod6)
      replacement += Data.substr(end_idx-mod6, mod6);

    Data.replace(start_idx, nchars, replacement);

    return start_idx+replacement.size();
  }

  /*!
   * \brief Stuffs 44 (or less) numeric codewords together.
   * \param start_idx Index of the first codeword in the data.
   * \param end_idx Index of the last codeword in the data.
   * \return New end index of the compressed data.
   * \todo support >18 digits
   */
  size_t Pdf417::compress_numeric_mode(size_t start_idx, size_t end_idx)
  {
    u32string replacement;
    auto nchars = (end_idx-start_idx);
    auto mod44  = nchars%44;

    for(size_t i=start_idx; i<end_idx-mod44; i+=44)
      {
        throw runtime_error("not sure how to deal with 44 digits");
      }

    int64_t val = 0;
    u32string str = u32string(1,0x1);
    str += Data.substr(end_idx-mod44, mod44);
    for(size_t i=0; i<=mod44; i++)
      {
        if(i>=18)
          throw runtime_error("not sure how to deal with >18 digits");

        val = val*10 + str[i];
      }

    str = U"";
    while(val>0)
      {
        str.push_back(val%900);
        val = val/900;
      }
    reverse(str.begin(), str.end());
    replacement += str;

    Data.replace(start_idx, nchars, replacement);

    return start_idx+replacement.size();
  }
}
