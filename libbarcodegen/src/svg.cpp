#include "svg.h"

namespace barcodegen
{
  /*! \internal
   * \brief Creats a (horizontal) linear barcode string from a vector of booleans
   * \param bits Black (1)/white (0) pattern to create the barcode.
   * \param quiet_zone Amount of whitespace to leave around the barcode.
   * \return An string that can be used to create an svg image of the barcode.
   */
  string create_svg_string(bits bits, uint32_t quiet_zone)
  {
    auto width = bits.size();
    auto height = 20;

    string svg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                 "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"
                 "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 ";

    svg += to_string(width+2*quiet_zone) + " " + to_string(height+2*quiet_zone);

    svg += "\" stroke=\"none\">\n"
           "  <rect width=\"100%\" height=\"100%\" fill=\"#FFFFFF\"/>\n"
           "  <path d=\"";


    for(size_t i=0; i<bits.size(); i++)
      {
        if(bits[i])
          svg += "M" + to_string(i+quiet_zone) + "," + to_string(quiet_zone) + "h1v" + to_string(height) + "h-1z ";
      }

    svg += "\" fill=\"#000000\"/>\n"
           "</svg>";

    return svg;
  }
}
