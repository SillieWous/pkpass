#include "qrcode.h"

#include "reedsolomon.h"

#include <math.h>
#include <stdexcept>

namespace barcodegen
{
  /*!
   * \brief Qr::Qr constructor
   * \param data Data to be encoded.
   * \param ecc_lvl Error correction level to use.
   */
  Qr::Qr(string data, EErrorCorrection ecc_lvl)
    : qrEncoding("qr.xml", ecc_lvl),
      Ecc_lvl(ecc_lvl)
  {
    Data = qrEncoding.encode_message(data);

    add_ecc_and_interleave_data();
  }

  /*!
   * \brief Function to generate a string that can be used as an image of the barcode
   * \param q_zone Size of additional empty space to be added to the barcode.
   *               e.g. 1 will add a one module thick ring of empty space around the barcode
   * \return A string in svg format of the barcode.
   */
  string Qr::to_svg_string(uint32_t q_zone)
  {
    uint32_t size = 4*qrEncoding.Version + 17;
    auto bit_array = get_bit_array();

    auto svg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
               "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"
               "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 " +
                  to_string(size+2*q_zone) + " " + to_string(size+2*q_zone) + "\">\n"
               "  <rect width=\"100%\" height=\"100%\" fill=\"#FFFFFF\"/>\n"
               "  <path d=\"";

    for (size_t row=0; row<size; row++)
      for (size_t col=0; col<size; col++)
        if (bit_array[row][col])
          svg += "M" + to_string(col+q_zone)   + "," + to_string(row+q_zone) + "h1v1h-1z ";

    svg += "\" fill=\"#000000\"/>\n"
           "</svg>";

    return svg;
  }

  /*!
   * \brief Getter for the codewords converted to a vector of booleans.
   * \return The codewords converted to a vector of booleans.
   */
  bits Qr::get_bits()
  {
    bits bits;

    for(const auto& datum : Data)
        for(int i=1<<7; i>0; i>>=1)
            bits.push_back(datum & i);

    return bits;
  }

  /*!
   * \brief Converts the encoded message, including ECC, to a matrix containing the optimal QR pattern.
   * \return A matrix containing the optimal QR pattern.
   */
  vector<bits> Qr::get_bit_array()
  {
    auto version = qrEncoding.Version;
    uint32_t size  = 4*version + 17;

    vector<bits> bit_array(size, bits(size, false));
    vector<bits> set_array(size, bits(size, false));

    pair<vector<bits>, vector<bits>> array = { bit_array, set_array };

    add_finder_pattern  (bit_array, set_array);
    add_timing_pattern  (bit_array, set_array);
    add_aligment_pattern(bit_array, set_array);
    add_dark_module     (bit_array, set_array);

    reserve_areas (set_array);

    add_bits (bit_array, set_array);

    auto best_mask = find_best_mask(bit_array, set_array);

    return apply_mask(bit_array, set_array, best_mask);
  }

  /*!
   * \brief Split data into approriate block, add ECC, and interleave the blocks.
   */
  void Qr::add_ecc_and_interleave_data()
  {
    auto blocks = qrEncoding.Blocks.at(Ecc_lvl)[qrEncoding.Version-1];

    auto ecc_words     = blocks.ECC_WORDS;
    auto group1_blocks = blocks.GROUP1_BLOCKS;
    auto group1_words  = blocks.GROUP1_WORDS;
    auto group2_blocks = blocks.GROUP2_BLOCKS;
    auto group2_words  = blocks.GROUP2_WORDS;

    auto required_len = group1_blocks*group1_words + group2_blocks*group2_words;
    auto padding_len = required_len-Data.size();

    for(size_t i=0; i<padding_len; i++)
      Data += (0x11 + 0xDB*((i+1)%2));

    vector<pair<u32string, u32string>> groups;

    for(int i=0; i<group1_blocks; i++)
      {
        auto message = Data.substr(group1_words*i, group1_words);
        auto ecc = ReedSolomon::get_check_words(message, 8, ecc_words, 285, true);
        groups.push_back(pair(message, ecc));
      }

    for(int i=0; i<group2_blocks; i++)
      {
        auto message = Data.substr(group1_blocks*group1_words + group2_words*i, group2_words);
        auto ecc = ReedSolomon::get_check_words(message, 8, ecc_words, 285, true);
        groups.push_back(pair(message, ecc));
      }

    Data.clear();

    size_t j = 0;
    while(j<groups[0].first.size())
      {
        for(size_t i=0; i<groups.size(); i++)
          if(j<groups[i].first.size())
            Data.push_back(groups[i].first[j]);
        j++;
      }
    j = 0;
    while(j<groups[0].second.size())
      {
        for(size_t i=0; i<groups.size(); i++)
          if(j<groups[i].second.size())
            Data.push_back(groups[i].second[j]);
        j++;
      }
  }

  /*!
   * \brief Adds the finder pattern to the barcode matrix, and sets indicators which bits have been set.
   * \param bits_in Matrix to add the finder pattern to.
   * \param set_in Matrix to set the bit indicators in.
   */
  void Qr::add_finder_pattern(vector<bits>& bits_in, vector<bits>& set_in)
  {
    int32_t last  = bits_in.size()-1;

    int32_t pos[] = { 3, last - 3 };

    for(const auto& row : pos)
      for(const auto& col : pos)
        {
          if(row==col && row != 3)
            continue;

          for(int i=-4; i<=4; i++)
            for(int j=-4; j<=4; j++)
              {
                if(row+i > last || col+j > last || row+i < 0 || col+j < 0)
                  continue;

                bits_in[row+i][col+j] = (max(abs(i), abs(j)) % 2); // true if chebyshev distance mod 2 = 1
                set_in [row+i][col+j] = true;
              }

          bits_in[row][col] = true; // and in the center
        }
  }

  /*!
   * \brief Adds the timing pattern to the barcode matrix, and sets indicators which bits have been set.
   * \param bits_in Matrix to add the timing pattern to.
   * \param set_in Matrix to set the bit indicators in.
   */
  void Qr::add_timing_pattern(vector<bits>& bits_in, vector<bits>& set_in)
  {
    uint32_t size = bits_in.size();

    for(size_t i=8; i<size-8; i++)
      {
        bits_in[i][6] = !(i%2);
        bits_in[6][i] = !(i%2);

        set_in[i][6] = true;
        set_in[6][i] = true;
      }
  }

  /*!
   * \brief Adds the alignement pattern (if needed) to the barcode matrix, and sets indicators which bits have been set.
   * \param bits_in Matrix to add the alignment pattern to.
   * \param set_in Matrix to set the bit indicators in.
   */
  void Qr::add_aligment_pattern(vector<bits>& bits_in, vector<bits>& set_in)
  {
    vector<uint32_t> alignment_loc;

    uint32_t size  = bits_in.size();
    uint32_t version = (size - 17)/ 4;

    if(version ==1)
      return;

    // determine aligment locations
    int numAlign = version / 7 + 2;
    int step = (version == 32) ? 26 : (version * 4 + numAlign * 2 + 1) / (numAlign * 2 - 2) * 2;

    for(int i=0, pos=size-7; i<numAlign-1; i++, pos-=step)
      alignment_loc.insert(alignment_loc.begin(), pos);

    alignment_loc.insert(alignment_loc.begin(), 6);

    // set alignment pattern bits
    for(const auto& row : alignment_loc)
      for(const auto& col : alignment_loc)
        {
          // continue if location overlaps with finder pattern
          if((row==alignment_loc.front() && col==alignment_loc.front()) ||
             (row==alignment_loc.front() && col==alignment_loc.back() ) ||
             (row==alignment_loc.back()  && col==alignment_loc.front()))
            continue;

          for(int i=-2; i<=2; i++)
            for(int j=-2; j<=2; j++)
              {
                // true if chebyshev distance mod 2 = 0
                bits_in[row+i][col+j] = !(max(abs(i), abs(j)) % 2);
                set_in [row+i][col+j] = true;
              }
        }
  }

  /*!
   * \brief Adds the dark module to the barcode matrix, and sets indicator which bit has been set.
   * \param bits_in Matrix to add the dark module to.
   * \param set_in Matrix to set the bit indicator in.
   */
  void Qr::add_dark_module(vector<bits> &bits_in, vector<bits> &set_in)
  {
    auto size = bits_in.size();
    bits_in[size-8][8] = true;
    set_in [size-8][8] = true;
  }

  /*!
   * \brief Adds the data pattern to the barcode matrix.
   * \param bits_in Matrix to add the data pattern to.
   * \param set_in Matrix with indicators which bits have been set. Returns with indicators where data is located.
   */
  void Qr::add_bits(vector<bits> &bits_in, vector<bits> &set_in)
  {
    auto size = bits_in.size();

    vector<bits> set_out(size, bits(size, false));

    auto bits = get_bits();

    size_t idx = 0;
    bool up = true;
    for(int col=size-1; col>0; col-=2)
      {
        if(col==6) // skip column 7 (add one, continue=>-2, net -1)
          {
            col++;
            continue;
          }

        for(size_t j=0; j<size; j++)
          {
            if(idx > bits.size())
              continue;

            auto row = up*(size-1-j) + !up*j;


            if(!set_in[row][col])
              {
                bits_in[row][col] = bits[idx];
                set_out[row][col] = true;
                idx++;
              }
            if(!set_in[row][col-1])
              {
                bits_in[row][col-1] = bits[idx];
                set_out[row][col-1] = true;
                idx++;
              }
          }
        up = !up;
      }

    set_in = set_out;
  }

  /*!
   * \brief Reserve bits for formatting and version information.
   * \param set_in Matrix to set the bit indicator in, reserving space for the special areas.
   */
  void Qr::reserve_areas(vector<bits> &set_in)
  {
    uint32_t last = set_in.size() - 1;
    uint32_t version = (last+1 - 17)/ 4;

    set_in[8][8] = true;
    for(int i=0; i<8; i++)
      {
        set_in[i][8] = true;
        set_in[8][i] = true;
        set_in[last-i][8] = true;
        set_in[8][last-i] = true;
      }

    if(version < 7)
      return;

    for(int i=0; i<6; i++)
      for(int j=0; j<3; j++)
        {
          set_in[last-8-j][i] = true;
          set_in[i][last-8-j] = true;
        }
  }

  /*!
   * \brief Applies one of the 8 masks to the matrix.
   * \param bits_in Matrix to apply the mask to.
   * \param set_in Matrix containing indicators where data is located.
   * \param mask Mask to be applied.
   * \return Matrix with the applied mask.
   */
  vector<bits> Qr::apply_mask(vector<bits> bits_in, vector<bits> set_in, uint8_t mask)
  {
    auto size     = bits_in.size();
    auto bits_out = bits_in;

    for (size_t row = 0; row < size; row++)
      {
        for (size_t col = 0; col < size; col++)
          {
            bool invert;

            switch (mask)
              {
              case 0:  invert = (row + col) % 2 == 0; break;
              case 1:  invert = row % 2 == 0; break;
              case 2:  invert = col % 3 == 0; break;
              case 3:  invert = (row + col) % 3 == 0; break;
              case 4:  invert = (row / 2 + col / 3) % 2 == 0; break;
              case 5:  invert = row * col % 2 + row * col % 3 == 0; break;
              case 6:  invert = (row * col % 2 + row * col % 3) % 2 == 0; break;
              case 7:  invert = ((row + col) % 2 + row * col % 3) % 2 == 0; break;
              default:  throw runtime_error("Assertion error");
              }

            bits_out[row][col] = bits_in[row][col] ^ (invert & set_in[row][col]);
          }
      }

    add_format_version_info(bits_out, mask);

    return bits_out;
  }

  /*!
   * \brief Calculates the penalty score for a given matrix.
   * \param bits_in Matrix to calculate the penalty score for.
   * \return Penalty score.
   */
  uint32_t Qr::get_penalty_score(vector<bits> bits_in)
  {
    const uint32_t PENALTY1 =  3;
    const uint32_t PENALTY2 =  3;
    const uint32_t PENALTY3 = 40;
    const uint32_t PENALTY4 = 10;

    const uint16_t FORBIDDEN_FRUIT1 = 0b10111010000;
    const uint16_t FORBIDDEN_FRUIT2 = 0b00001011101;

    auto     size   = bits_in.size();
    uint32_t penalty1 = 0;
    uint32_t penalty2 = 0;
    uint32_t penalty3 = 0;
    uint32_t penalty4 = 0;

    for (size_t i=0; i<size; i++)
      {
        bool run_val_col = bits_in[i][0];
        bool run_val_row = bits_in[0][i];

        size_t run_count_col = 1;
        size_t run_count_row = 1;

        uint16_t mask_col = bits_in[i][0] << 1;
        uint16_t mask_row = bits_in[0][i] << 1;

        for(size_t j=1; j<size; j++)
          {
            if(bits_in[i][j] == run_val_col)
              run_count_col++;
            else
              {
                run_val_col   = bits_in[i][j];
                run_count_col = 1;
              }

            if(bits_in[j][i] == run_val_row)
              run_count_row++;
            else
              {
                run_val_row   = bits_in[j][i];
                run_count_row = 1;
              }

            penalty1 += (PENALTY1 * (run_count_col == 5)) +
                           (run_count_col>5);
            penalty1 += (PENALTY1 * (run_count_row == 5)) +
                           (run_count_row>5);

            mask_col |= bits_in[i][j];
            mask_row |= bits_in[j][i];

            if(j>=10 && (mask_col == FORBIDDEN_FRUIT1 || mask_col == FORBIDDEN_FRUIT2))
              penalty3 += PENALTY3;
            if(j>=10 && (mask_row == FORBIDDEN_FRUIT1 || mask_row == FORBIDDEN_FRUIT2))
              penalty3 += PENALTY3;

            mask_col = (mask_col << 1) & ((1<<11) -1);
            mask_row = (mask_row << 1) & ((1<<11) -1);

          }
      }

    // 2*2 blocks of modules having same color
    for (size_t row = 0; row < size - 1; row++)
        for (size_t col = 0; col < size - 1; col++)
            penalty2 += PENALTY2 * (bits_in[row][col] == bits_in[row+1][col]   &&
                                   bits_in[row][col] == bits_in[row]  [col+1] &&
                                   bits_in[row][col] == bits_in[row+1][col+1]);

    // Balance of dark and light modules
    uint32_t dark = 0;
    for (const auto& row : bits_in)
        for (bool set : row)
            dark += set;

    uint32_t total = size * size;

    int percentage = 100*dark/total;

    uint32_t next = abs(percentage/5 + (percentage % 5 != 0) - 10);
    uint32_t prev = abs(percentage/5 - 10);

    penalty4 += min(next, prev) * PENALTY4;

    return (penalty1 + penalty2 + penalty3 + penalty4);
  }

  /*!
   * \brief Adds the version and format information to the matrix.
   * \param bits_in Matrix to add the format and version information to.
   * \param mask Mask used to create the given matrix.
   */
  void Qr::add_format_version_info(vector<bits> &bits_in, uint8_t mask)
  {
    const uint32_t size = bits_in.size();
    const uint32_t version = (size - 17)/ 4;

    /// \todo check if 14 is correct, was 15 but caused issue
    uint32_t gen_poly = 0b10100110111 << 4;

    uint32_t info = (mask & 0b111) << 10;

    switch (Ecc_lvl)
      {
      case IQr::EErrorCorrection::LOW:      info |= 1<<13; break;
      case IQr::EErrorCorrection::MEDIUM:   info |= 0<<13; break;
      case IQr::EErrorCorrection::QUARTILE: info |= 3<<13; break;
      case IQr::EErrorCorrection::HIGH:     info |= 2<<13; break;
      }

    uint32_t masked = info;

    for(auto i=1<<(15-1); i>=1<<10; i>>=1)
      {
        if(masked & i)
          masked ^= gen_poly;

        gen_poly >>= 1;
      }
    masked ^= info;
    masked ^= 0b101010000010010;

    for(int i=0; i<15; i++)
      {
        uint32_t col = min(i, 7) + (i>5);
        uint32_t row = (8 - (i>7)*(i%7) - (i>8)) * (i!=14);

        bits_in[row][col] = (masked & (1<<(14-i)));

        col = i<7 ? 8        : size-8 + (i - 7);
        row = i<7 ? size-1-i : 8;

        bits_in[row][col] = (masked & (1<<(14-i)));
      }

    // Version info >= 7 needs extra version information block
    if(version < 7)
      return;

    gen_poly = 0b1111100100101 << 6;
    masked = version << 12;

    for(auto i=1<<(18-1); i>=1<<12; i>>=1)
      {
        if(masked & i)
          masked ^= gen_poly;

        gen_poly >>= 1;
      }

    masked = (version << 12) + masked;

    for(int i=0; i<18; i++)
      {
        uint32_t a = size - 11 + i%3;
        uint32_t b = size - 11 + i/3;

        bits_in[a][b] = (masked & (1<<i));
        bits_in[b][a] = (masked & (1<<i));
      }
  }

  /*!
   * \brief Optimisation function for finding the mask with the lowest penalty score.
   * \param bits_in Matrix without any maks applied.
   * \param set_in Matrix containing indicators where data is located.
   * \return The best mask to use.
   */
  uint32_t Qr::find_best_mask(vector<bits> bits_in, vector<bits> set_in)
  {
    uint32_t best_mask = 0;
    auto lowest_penalty = UINT32_MAX;
    for(uint8_t mask = 0; mask < 8; mask++)
      {
        auto masked_array = apply_mask(bits_in, set_in, mask);
        auto penalty = get_penalty_score(masked_array);

        if(penalty < lowest_penalty)
          {
            lowest_penalty = penalty;
            best_mask = mask;
          }
      }

    return best_mask;
  }
}
