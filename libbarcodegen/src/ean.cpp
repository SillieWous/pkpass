#include "ean.h"

#include "svg.h"
#include "exception.h"

#include <stdexcept>

namespace barcodegen
{
  /*!
   * \brief EAN::EAN constructor
   * \param digits Array of single decimal digit numbers.
   * \param size Number of digits in the array.
   * \param patterns G/L-code encoding for each digit from 0-9.
   * \exception IException::Exception_t::Argument Found a digit that is >=10.
   */
  EAN::EAN(uint8_t *digits, size_t size, map<uint8_t, uint8_t> patterns)
  {
    Patterns = patterns;

    for(size_t i=0; i<size; i++)
      {
        if(digits[i] >= 10)
          throw ArgumentException("only decimal digits allowed for EAN code");

        Data.push_back(digits[i]);
      }
  }

  /*!
   * \brief Function to generate a string that can be used as an image of the barcode
   * \param quiet_zone Size of additional empty space to be added to the barcode.
   *                   e.g. 1 will add a one module thick ring of empty space around the barcode
   * \return A string in svg format of the barcode.
   */
  string EAN::to_svg_string(uint32_t quiet_zone)
  {
    return create_svg_string(get_bits(), quiet_zone);
  }

  /*!
   * \brief Getter function for the black/white barcode pattern.
   * \return The ones and zeros which construct the barcode.
   */
  bits EAN::get_bits()
  {
    auto data = add_patterns();
    auto checksum = get_checksum();

    data += 0xff - L_code.at(checksum);

    bits dataBits = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // quiet zone 11 bits
                      1, 0, 1 };

    for(size_t i=0; i<data.size(); i++)
      {
        if(i == data.size() / 2)
          dataBits.insert(dataBits.end(), { 0, 1, 0, 1, 0 });

        uint8_t byte = data[i];
        for(int j=1<<6; j>0; j>>=1)
            dataBits.push_back(byte&j);
      }
    dataBits.insert(dataBits.end(), { 1, 0, 1,
                                      0, 0, 0, 0, 0, 0, 0 }); // quite zone 7 bits

    return dataBits;
  }

  /*!
   * \brief Get a specific digit of the barcode (checksum excluded).
   * \param position Position index of the requested digit.
   * \return Digit at requested position.
   */
  uint8_t EAN::get_digit(uint8_t position)
  {
    if(position >= Data.size())
      throw runtime_error("requested position larger than size");

    return Data[position];
  }

  /*!
   * \brief Get the calculated checksum digit of the barcode.
   * \return The checksum digit of the barcode.
   */
  uint8_t EAN::get_checksum()
  {
    uint32_t checksum = 0;
    uint32_t size = Data.size();

    for(size_t i=0; i<Data.size(); i++)
        checksum += (1 + 2*((size-1-i)%2)) * Data[i];

    checksum = 10 - (checksum % 10);

    return checksum;
  }

  /*!
   * \brief Adds words as per either G or L code according to data
   * \return Encoded data.
   * \todo rename
   * \todo doesn't work for EAN8? or other R only EAN codes
   */
  string EAN::add_patterns()
  {
    char pattern = Patterns[Data[0]];
    string data;

    size_t size = Data.size();

    for(size_t i=1; i<size; i++)
      {
        if(i<= (size+1)/2)
          {
            if(pattern & 1 << ((size+1)/2-i))
              data += G_code.at(Data[i]);
            else
              data += L_code.at(Data[i]);
          }
        else
          data += 0xff - L_code.at(Data[i]); // R code
      }

    return data;
  }
}
