#include "qrencoding.h"

#include <stdexcept>

namespace barcodegen
{
  /*!
   * \brief Determines the best way to encode a message from the costmap (starting at version 1).
   * \param data Message to be encoded.
   * \param costmap A cost map for each symbol using the available codesets.
   * \return Minimal cost encoded message.
   */
  u32string QrEncoding::minimise_cost(string data, costmap_t costmap)
  {
    return minimise_cost(data, costmap, 1);
  }

  /*!
   * \brief Determines the best way to encode a message from the costmap (called recursively until the data fits the
   *        version).
   * \param data Message to be encoded.
   * \param costmap A cost map for each symbol using the available codesets.
   * \param version QR version to optimise the cost for.
   * \return Minimal cost encoded message.
   * \todo refactor into smaller functions
   */
  u32string QrEncoding::minimise_cost(string                data,
                                      costmap_t             costmap,
                                      uint8_t               version)
  {
    if(version>40)
      throw std::runtime_error("Data doesn't fit QR code version 40.");

    Version = version;

    size_t nsets = Codesets.size();
    size_t nchar = data.length();

    auto mincost = costmap[nchar-1][0];

    for(size_t j=1; j<nsets; j++)
        if(costmap[nchar-1][j].cost < mincost.cost)
            mincost = costmap[nchar-1][j];

    pair<u32string, u32string> encoding;

    auto blocks = Blocks.at(Ecc_lvl)[version-1];
    uint32_t max_ver_bits = 8*(blocks.GROUP1_BLOCKS*blocks.GROUP1_WORDS + blocks.GROUP2_BLOCKS*blocks.GROUP2_WORDS);

    size_t i       = nchar-1;
    size_t end_idx = nchar;
    auto data_copy = data;
    while(i>0)
      {
        uint32_t symbol_size = Codesets[mincost.idx].symbol_size;

        if(mincost.idx != mincost.prev_idx)
          {
            uint32_t len = end_idx-i+1;

            auto substr = data_copy.substr(i-1, len);

            auto codeset_name = Codesets[mincost.idx].name;

            pair<u32string, u32string> compressed_str;
            if(codeset_name == "Numeric")
              compressed_str = compress_numeric_mode(substr, Codesets[mincost.idx]);
            else if(codeset_name == "Alphanumeric")
              compressed_str = compress_alphanumeric_mode(substr, Codesets[mincost.idx]);
            //else if(codeset_name == "Kanji")
            else
              compressed_str = pair(u32string(substr.rbegin(), substr.rend()), u32string(substr.size(), 8));

            encoding.first  += compressed_str.first;
            encoding.second += compressed_str.second;

            auto len_field = length_field.at(Codesets[mincost.idx].name)[(version>9) + (version>26)];
            encoding.first += (Switches[tuple(codeset_name, codeset_name, ESwitchType::SWITCH)][0] << len_field) + len;
            encoding.second += len_field + 4;

            end_idx = i-1;

            data_copy.erase(i-1);
          }

        i-=symbol_size;

        if(i>0)
          mincost = costmap[i-1][mincost.prev_idx];
      }

    auto substr = data_copy;

    auto codeset_name = Codesets[mincost.idx].name;

    pair<u32string, u32string> compressed_str;
    if(codeset_name == "Numeric")
      compressed_str = compress_numeric_mode(substr, Codesets[mincost.idx]);
    else if(codeset_name == "Alphanumeric")
      compressed_str = compress_alphanumeric_mode(substr, Codesets[mincost.idx]);
    //else if(codeset_name == "Kanji")
    else
      compressed_str = pair(u32string(substr.rbegin(), substr.rend()), u32string(substr.size(), 8));

    encoding.first  += compressed_str.first;
    encoding.second += compressed_str.second;

    auto len_field = length_field.at(Codesets[mincost.idx].name)[(version>9) + (version>26)];
    encoding.first += (Switches[tuple(codeset_name, codeset_name, ESwitchType::SWITCH)][0] << len_field) +  end_idx;
    encoding.second += len_field + 4;

    reverse(encoding.first.begin(), encoding.first.end());
    reverse(encoding.second.begin(), encoding.second.end());

    encoding.first.push_back(0);
    encoding.second.push_back(4);

    u32string cost;
    uint8_t filled = 0;
    uint32_t total_bits = 0;
    uint32_t c = 0;
    for(size_t i=0; i<encoding.first.size(); i++)
      {
        auto symbol      = encoding.first[i];
        auto symbol_size = encoding.second[i];
        total_bits += symbol_size;

        for(int i=1<<(symbol_size-1); i>0; i>>=1)
          {
            c += bool(symbol & i);
            filled = (filled+1) % 8;

            if(filled==0)
              {
                cost += c;
                c = 0;
              }

            c <<= 1;
          }
      }
    if(filled!=0)
      {
        c >>= 1;
        cost += c << (8-filled);
      }

    if(total_bits > max_ver_bits)
      return minimise_cost(data, costmap, version+1);

    return cost;
  }

  /*!
   * \brief Stuffs 3 (or less) alphanumeric codewords together.
   * \param data Data to be compressed.
   * \param codeset Alphanumeric codeset containing the encoding.
   * \return A pair containing the compressed data and length of each codeword in that data.
   */
  pair<u32string, u32string> QrEncoding::compress_alphanumeric_mode(string data, codeset_t codeset)
  {
      u32string replacement;
      u32string length;
      auto nchars = data.size();
      auto mod2   = nchars%2;

      for(size_t i=0; i<data.size()-mod2; i+=2)
        {
          auto val = codeset.codes[string(1,data[i]  )][0]*45 +
                     codeset.codes[string(1,data[i+1])][0];
          replacement.push_back(val);
        }

      length += u32string((data.size()-mod2)/2, 11);

      if(mod2)
        {
          replacement += codeset.codes[string(1,data[(nchars-1)])][0];
          length += 6;
        }

      reverse(replacement.begin(), replacement.end());
      reverse(length.begin(),      length.end());

      return pair(replacement, length);
  }

  /*!
   * \brief Stuffs 3 (or less) numeric codewords together.
   * \param data Data to be compressed.
   * \param codeset Numeric codeset containing the encoding.
   * \return A pair containing the compressed data and length of each codeword in that data.
   */
  pair<u32string, u32string> QrEncoding::compress_numeric_mode(string data, codeset_t codeset)
  {
    u32string replacement;
    u32string length;
    auto nchars = data.size();
    auto mod3   = nchars%3;

    for(size_t i=0; i<data.size()-mod3; i+=2)
      {
        auto val = codeset.codes[string(1,data[i]  )][0]*100 +
                   codeset.codes[string(1,data[i+1])][0]*10 +
                   codeset.codes[string(1,data[i+2])][0];
        replacement.push_back(val);
      }

    length += u32string((data.size()-mod3)/3, 11);

    if(mod3 == 1)
      {
        replacement += codeset.codes[string(1,data[(nchars-1)])][0];
        length += 4;
      }
    if(mod3 == 2)
      {
        replacement += codeset.codes[string(1,data[(nchars-2)])][0]*10 +
                       codeset.codes[string(1,data[(nchars-1)])][0];
        length += 7;
      }

    reverse(replacement.begin(), replacement.end());
    reverse(length.begin(),      length.end());

    return pair(replacement, length);
  }
}
