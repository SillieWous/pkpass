#include "libbarcodegen.h"

#include "exception.h"

#include "ean.h"
#include "code128.h"
#include "aztec.h"
#include "pdf417.h"
#include "qrcode.h"

namespace barcodegen
{
  // documented in libbarcodegen.h
  shared_ptr<IBarcode> IBarcode::create(EFormat format, string data)
  {
    switch (format)
      {
      case EFormat::EAN13:
      case EFormat::EAN8:
        {
          vector<uint8_t> digits;
          for(const auto& c : data)
            {
              if(!isdigit(c))
                throw std::exception();
              else
                digits.push_back(c-48);
            }

          switch (format)
            {
            case EFormat::EAN8:  return make_shared<EAN8> (&digits[0]); break;
            case EFormat::EAN13: return make_shared<EAN13>(&digits[0]); break;
            default: throw NotImplementedException("Only EAN8 and EAN13 are currently supported.");
            }
        }

      case EFormat::CODE128:
          return make_shared<Code128>(data);

      case EFormat::AZTEC:
          return make_shared<Aztec>(data);

      case EFormat::PDF417:
          return make_shared<Pdf417>(data);

      case EFormat::QR:
          return make_shared<Qr>(data);

      default:
        throw UnsupportedException("Requested barcode format is currently not supported.");
      }
  }

  // documented in libbarcodegen.h
  shared_ptr<IPdf417> IPdf417::create(string data)
  {
    return make_shared<Pdf417>(data);
  }

  // documented in libbarcodegen.h
  shared_ptr<IEAN> IEAN::create(EFormat format, string data)
  {
    switch (format)
      {
      case EFormat::EAN13:
      case EFormat::EAN8:
        {
          vector<uint8_t> digits;
          for(const auto& c : data)
            {
              if(!isdigit(c))
                throw ArgumentException("EAN barcodes can only contain digits.");
              else
                digits.push_back(c-48);
            }

          switch (format)
            {
            case EFormat::EAN8:  return make_shared<EAN8> (&digits[0]); break;
            case EFormat::EAN13: return make_shared<EAN13>(&digits[0]); break;
            default: throw NotImplementedException("Only EAN8 and EAN13 are currently supported.");
            }
        }
      default:
        throw UnsupportedException("Requested format is not an EAN format.");
      }
  }

  // documented in libbarcodegen.h
  shared_ptr<IQr> IQr::create(string data, EErrorCorrection ecc_lvl)
  {
    return make_shared<Qr>(data, ecc_lvl);
  }
}
