﻿#include "encoding.h"

#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/foreach.hpp>
#include <codecvt>  // codecvt_utf8
#include <iostream>
#include <locale>   // wstring_convert
#include <math.h>

namespace barcodegen
{
  /*!
   * \brief Encoding::Encoding constructor.
   * \param xmlFile xml file which contains the description of the encoding.
   * \todo provide .xsd for xml
   */
  Encoding::Encoding(string xmlFile)
  {
    xmlFile = Encoding_storage_dir + xmlFile;

    boost::property_tree::ptree propertyTree;
    read_xml(xmlFile, propertyTree, boost::property_tree::xml_parser::no_comments);

    auto encoding = propertyTree.get_child("encoding");
    auto base     = encoding.get<uint32_t>("<xmlattr>.base");

    BOOST_FOREACH(auto const& node, encoding.get_child("codesets"))
    {
      if(node.first == "codeset")
        {
          auto codeset = parse_codeset(node.second, base);
          Codesets.push_back(codeset);
        }
    }

    BOOST_FOREACH(auto const& node, encoding.get_child("bitmaps"))
    {
      auto table = node.second.get<uint32_t>("<xmlattr>.table", 0);
      if(node.first == "bitmap")
        {
          auto symbolStr = node.second.get<string>("<xmlattr>.symbol");
          auto valueStr  = node.second.get<string>("");

          uint32_t symbol;
          switch(symbolStr[1])
            {
            case 'x': symbol=stoul(symbolStr.substr(2), 0, 16); break;
            case 'b': symbol=stoul(symbolStr.substr(2), 0, 2); break;
            default:  symbol=stoul(symbolStr, 0, 10); break;
            }

          unsigned long bitmap;
          size_t len;

          switch(valueStr[1])
            {
            case 'x': bitmap=stoul(valueStr.substr(2), 0, 16); len = valueStr.length()-2; break;
            case 'b': bitmap=stoul(valueStr.substr(2), 0, 2);  len = valueStr.length()-2; break;
            default:  bitmap=stoul(valueStr, 0, 10);           len = valueStr.length();   break;
            }

          Bitmap[pair(symbol, table)] = pair(bitmap, len);
        }
    }
  }

  /*!
   * \brief Converts data to bit pattern according to the encoding specification.
   * \param data Data to convert to bits.
   * \return The data converted to the bit pattern for the encoding.
   */
  bits Encoding::get_bits(u32string data)
  {
    bits bitdata;

    for(const auto& byte : data)
      {
        auto len     = Bitmap[pair(byte,0)].second;
        auto pattern = Bitmap[pair(byte,0)].first;

        for(size_t mask=1<<(len-1); mask>0; mask >>= 1)
          bitdata.push_back(pattern & mask);
      }

    return bitdata;
  }

  /*!
   * \brief Converts data to bit pattern according to the encoding specification.
   * \param data Data to convert to bits.
   * \param table Table index to use for the converion. Used if there are more ways to encode a codeword to a bit
   *              pattern.
   * \return The data converted to the bit pattern for the encoding.
   */
  bits Encoding::get_bits(u32string data, uint32_t table)
  {
    bits bitdata;

    for(const auto& byte : data)
      {
        auto len     = Bitmap[pair(byte, table)].second;
        auto pattern = Bitmap[pair(byte, table)].first;

        for(size_t mask=1<<(len-1); mask>0; mask >>= 1)
          bitdata.push_back(pattern & mask);
      }

    return bitdata;
  }

  /*!
   * \brief Encodes the message using the encoding specified in the .xml used to create the object.
   * \param data Message to be encoded.
   * \param start_codeset Name of the codeset to use for encoding the first symbol.
   * \return Encoded message.
   */
  u32string Encoding::encode_message(string data, string start_codeset)
  {
    auto cost = get_message_cost(data, start_codeset);

    return minimise_cost(data, cost);
  }

  /*!
   * \brief Cost calculation function.
   * \param data Message to be encoded.
   * \param start_codeset Name of the codeset to use for encoding the first symbol.
   * \return A cost map for each symbol using the available codesets.
   */
  Encoding::costmap_t Encoding::get_message_cost(string data, string start_codeset)
  {
    size_t nsets = Codesets.size();
    size_t nchar = data.length();

    costmap_t cost(nchar, vector(nsets, cost_t{UINT32_MAX, 0, 0, false}));

    for(size_t i=0; i<nchar; i++)
      {
        for(size_t j=0; j<nsets; j++)
          {
            auto codeset_j = Codesets[j];
            auto mincost_j = cost[i][j];

            mincost_j.idx = j;

            // first character comes from startcodeset
            if(i==0 && !start_codeset.empty() && start_codeset!=codeset_j.name)
                continue;

            auto symbol_size_j = codeset_j.symbol_size;
            if(i < (symbol_size_j-1))
                continue;

            auto symbol = data.substr(i-symbol_size_j+1, symbol_size_j);

            // shift not possible for first two symbols
            if(i>2*symbol_size_j)
              {
                // shift cost
                auto prev_symbol = data.substr(i-symbol_size_j,symbol_size_j);
                auto prev_str    = shift_cost(j, prev_symbol);
                auto cost_str    = encoding_cost(j, j, symbol);
                auto cost_str_j  = (cost_str.empty() || prev_str.empty()) ? U"" : (prev_str+cost_str);

                double prev_cost = cost[i-symbol_size_j*2][j].cost;
                /// \todo shift might have different cost to symbol_cost
                double cost_j = (cost_str_j.empty() || prev_cost==UINT32_MAX) ?
                                    UINT32_MAX :
                                    prev_cost + cost_str_j.length()*codeset_j.symbol_cost;

                if(cost_j < mincost_j.cost)
                  {
                    mincost_j.cost     = cost_j;
                    mincost_j.prev_idx = j;
                    mincost_j.shift    = true;
                  }
              }

            for(size_t m=0; m<nsets; m++)
              {
                if(i < symbol_size_j && i>0)
                    continue;

                /// \todo Encoding cost returns string, actual cost might be faster
                auto cost_str_mj = encoding_cost(m, j, symbol);

                double prev_cost = ((i>0)? cost[i-symbol_size_j][m].cost : 0);

                /// \todo cost might be due to shift (e.g. last symbol)
                auto encoding_cost = (codeset_j.sub_mode_of == Codesets[m].sub_mode_of) ?
                                          prev_cost + cost_str_mj.length()*codeset_j.symbol_cost :
                                          ceil(prev_cost) + 1 + (cost_str_mj.length()-1)*codeset_j.symbol_cost;

                double cost_mj = (cost_str_mj.empty() || prev_cost==UINT32_MAX) ? UINT32_MAX : encoding_cost;

                if(cost_mj < mincost_j.cost)
                  {
                    mincost_j.cost     = cost_mj;
                    mincost_j.prev_idx = m;
                    mincost_j.shift    = false;
                  }
              }

            cost[i][j] = mincost_j;
          }
      }

    return cost;
  }

  /*!
   * \brief Determines the best way to encode a message from the costmap.
   * \param data Message to be encoded.
   * \param costmap A cost map for each symbol using the available codesets.
   * \return Minimal cost encoded message.
   */
  u32string Encoding::minimise_cost(string data, Encoding::costmap_t costmap)
  {
    size_t nsets = Codesets.size();
    size_t nchar = data.length();

    auto mincost = costmap[nchar-1][0];

    for(size_t j=1; j<nsets; j++)
        if(costmap[nchar-1][j].cost < mincost.cost)
            mincost = costmap[nchar-1][j];

    u32string encoding = Codesets[mincost.idx].codes.count("stop") ?
                            Codesets[mincost.idx].codes["stop"] :
                            U"";

    size_t i = nchar;
    while(i>0)
      {
        uint32_t symbol_size;
        if(mincost.shift)
          {
            symbol_size = Codesets[mincost.idx].symbol_size;

            auto symbol      = data.substr(data.size()-symbol_size);
            auto symbol_prev = data.substr(data.size()-symbol_size*2,symbol_size);

            i-=symbol_size; // move two symbols

            encoding += encoding_cost(mincost.idx, mincost.idx, symbol) +
                        encoding_cost(mincost.idx, mincost.idx, symbol_prev);

          }
        else
          {
            symbol_size = Codesets[mincost.idx].symbol_size;

            auto symbol = data.substr(data.size()-symbol_size);

            encoding += encoding_cost(mincost.prev_idx, mincost.idx, symbol);
          }

        i-=symbol_size;
        data.erase(i);

        if(i>0)
          mincost = costmap[i-1][mincost.prev_idx];
      }

    encoding += Codesets[mincost.idx].codes.count("start") ?
                    Codesets[mincost.idx].codes["start"] :
                    U"";

    reverse(encoding.begin(), encoding.end());

    return encoding;
  }

  /*!
   * \brief Finds the cost to encode a symbol given the current an previous codeset.
   * \param prev_codeset_idx Index of the previous codeset.
   * \param codeset_idx Index of the current codeset.
   * \param symbol Symbol to encode.
   * \return Cost to encode a sysmbol, empty if it cannot be encoded with the given codesets.
   */
  u32string Encoding::encoding_cost(size_t prev_codeset_idx, size_t codeset_idx, string symbol)
  {
    auto key = tuple(prev_codeset_idx, codeset_idx, symbol);

    if(EncodingCostMap.count(key))
      return EncodingCostMap[key];

    u32string cost = U"";

    auto codeset      = Codesets[codeset_idx];
    auto prev_codeset = Codesets[prev_codeset_idx];

    if(prev_codeset.name != codeset.name)
      {
        tuple switch_key(prev_codeset.name, codeset.name, ESwitchType::SWITCH);
        if(Switches.count(switch_key))
            cost = Switches[switch_key];
        else
            return EncodingCostMap[key] = U"";
      }

    if(Codesets[codeset_idx].codes.count(symbol))
      {
        cost = Codesets[codeset_idx].codes[symbol] + cost;
      }
    else
      {
        // cannot switch and shift
        if(!cost.empty())
          return EncodingCostMap[key] = U"";

        cost = shift_cost(codeset_idx, symbol);
      }

    return EncodingCostMap[key] = cost;
  }

  /*!
   * \brief Find the cost to encode a symbol by shifting modes.
   * \param codeset_idx Codeset to use for encoding.
   * \param symbol Symbol to encode.
   * \return Cost to encode the symbol by shifting modes, empty if it cannot be encoded using by shifting modes.
   */
  u32string Encoding::shift_cost(size_t codeset_idx, string symbol)
  {
    u32string cost = U"";

    auto codeset = Codesets[codeset_idx];
    for(const auto& shift_codeset : Codesets)
      {
        if(shift_codeset.name != codeset.name)
          {
            tuple key(codeset.name, shift_codeset.name, ESwitchType::SHIFT);
            if(Switches.count(key) && shift_codeset.codes.count(symbol))
              {
                auto code = shift_codeset.codes.at(symbol);
                cost += code;

                auto shift = Switches[key];
                cost += shift;

                break;
              }
          }
      }
    return cost;
  }

  /*!
   * \brief Parser function to fill the settings of a codeset.
   * \param property_tree XML tree object of a \<codeset\> element
   * \param parent_base Number of bits the parent encoding uses to encode a codeword.
   * \return Codeset filled with settins of the object.
   */
  Encoding::codeset_t Encoding::parse_codeset(boost::property_tree::ptree property_tree, uint32_t parent_base)
  {
    auto name              = property_tree.get<std::string>("<xmlattr>.name");
    auto symbol_size       = property_tree.get<std::string>("<xmlattr>.symbol_size", "");
    auto base              = property_tree.get<uint32_t>("<xmlattr>.base", 10);
    auto submode_delimiter = name.find_last_of('_')==string::npos ? 0 : name.find_last_of('_');

    auto codeset = codeset_t();
    codeset.name        = name;
    codeset.sub_mode_of = name.substr(0, submode_delimiter);
    codeset.symbol_size = symbol_size.empty() ? 1 : stoul(symbol_size);
    codeset.symbol_cost = log(base)/log(parent_base);

    auto specialset = codeset_t();

    BOOST_FOREACH(auto const& node, property_tree.get_child("symbols"))
      {
        auto symbolTree = node.second;

        auto encodingStr = symbolTree.get<string>("<xmlattr>.encoding", "");
        auto special     = symbolTree.get<string>("<xmlattr>.special", "");
        auto symbolStr   = symbolTree.get<string>("");

        if(encodingStr.empty())
          { cout << symbolStr; }

        if(special.empty())
          {
            string symbol = "";
            for(size_t i=2; i<symbolStr.length(); i+=2)
              {
                symbol += stoul(symbolStr.substr(i, 2), 0, 16);
              }
            codeset.codes[symbol] = stoul(encodingStr, 0, 16);
          }
        else
          {
            if(special == "shift")
              {
                auto key = tuple(name, symbolStr, ESwitchType::SHIFT);
                Switches[key] = stoul(encodingStr, 0, 16);
              }
            else if(special == "switch")
              {
                auto key = tuple(name, symbolStr, ESwitchType::SWITCH);
                Switches[key] = stoul(encodingStr, 0, 16);
              }

            else if(special == "start" && (symbolStr == name || symbolStr.empty()))
              specialset.codes[special] = stoul(encodingStr, 0, 16);

            else if(special == "stop"  && (symbolStr == name || symbolStr.empty()))
              specialset.codes[special] = stoul(encodingStr, 0, 16);

            /// \todo support GS1 and ECI
            //else if(special == "fnc")
            // not sure what to do with these
          }
      }

    if(codeset.codes.empty() && name=="Byte")
        for(int i=0; i<=0xff; i++)
            codeset.codes[string(1,i)] = i;

    if(codeset.codes.empty() && name=="Numeric")
        for(int i=0; i<10; i++)
            codeset.codes[string(1,0x30+i)] = i;

    codeset.codes.insert(specialset.codes.begin(),specialset.codes.end());

    return codeset;
  }
}
