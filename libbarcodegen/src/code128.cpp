#include "code128.h"

#include "svg.h"

namespace barcodegen
{
  /*!
   * \brief Code128::Code128 constructor
   * \param data Data to be encoded.
   */
  Code128::Code128(string data)
    : Code128Encoding("code128.xml")
  {
    Data = Code128Encoding.encode_message(data);
    Data.insert(Data.end()-1, get_checksum());
  }

  /*!
   * \brief Function to generate a string that can be used as an image of the barcode
   * \param quiet_zone Size of additional empty space to be added to the barcode.
   *                   e.g. 1 will add a one module thick ring of empty space around the barcode
   * \return A string in svg format of the barcode.
   */
  string Code128::to_svg_string(uint32_t quiet_zone)
  {
    return create_svg_string(get_bits(), quiet_zone);
  }

  /*!
   * \brief Getter function for the black/white barcode pattern.
   * \return The ones and zeros which construct the barcode.
   */
  bits Code128::get_bits()
  {
    auto encodingBits = Code128Encoding.get_bits(Data);

    bits bitdata = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; // quiet zone

    bitdata.insert(bitdata.end(), encodingBits.begin(), encodingBits.end());

    bitdata.insert(bitdata.end(), {1, 1, // final bar
                                   0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }); // quiet zone

    return bitdata;
  }

  /*!
   * \brief Calculates the checksum from the encoded data.
   * \return The checksum of the encoded data.
   */
  uint8_t Code128::get_checksum()
  {
    const uint8_t mod = 103;

    uint8_t checksum = Data[0] % mod;
    for(size_t i=1; i<Data.size()-1; i++)
        checksum = (checksum + i*Data[i]) % mod;

    return checksum;
  }
}
