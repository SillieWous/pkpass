#include "aztec.h"

#include "reedsolomon.h"
#include "svg.h"

#include <stdexcept>
#include <math.h>

namespace barcodegen
{
  /*!
   * \brief Aztec::Aztec constructor
   * \param data Data to be encoded.
   */
  Aztec::Aztec(string data)
    : Aztec_encoding("aztec.xml")
  {
    Data = Aztec_encoding.encode_message(data, "Upper");

    calculate_number_of_codewords();
    data_to_codewords();
    bitstuff_codewords();
    add_ecc_codewords();
    add_modewords();
  }

  /*!
   * \brief Function to generate a string that can be used as an image of the barcode
   * \param q_zone Size of additional empty space to be added to the barcode.
   *               e.g. 1 will add a one module thick ring of empty space around the barcode
   * \return A string in svg format of the barcode.
   * \todo support full size code with pattern
   * \todo refactor into smaller functions
   */
  string Aztec::to_svg_string(uint32_t q_zone)
  {

    const int32_t eye_size = 5+ 2*(Number_of_layers>4);
    const uint32_t size    = 2*(2*Number_of_layers + eye_size) + 1;

    uint32_t offset = (size+1)/2;
    const uint32_t grid_offset = offset-1;

    vector<vector<bool>> code_matrix(size, vector(size, false));

    int32_t x = -int32_t(size-1)/2;
    int32_t y = -int32_t(size-1)/2;

    uint32_t changes = 0;

    int8_t xdir =  ((changes+1)%4 -2 +  changes   %2)*( changes   %2);
    int8_t ydir = -( changes   %4 -2 + (changes+1)%2)*((changes+1)%2);

    uint32_t i = 0;

    auto bitstream = get_bits();

    while(abs(x)>eye_size || abs(y)>eye_size)
      {
        code_matrix[grid_offset+x]     [grid_offset+y]      = bitstream[i];
        code_matrix[grid_offset+x+ydir][grid_offset+y-xdir] = bitstream[i+1];

        x = x + xdir;
        y = y + ydir;

        if(x==xdir*int32_t(offset-2*(xdir==1)) && xdir)
          {
            if(xdir==-1)
              {
                y = y+2;
                x = x-xdir;
              }
            else
              x = x+xdir;

            changes = changes + 1;
            xdir =  ((changes+1)%4 -2 +  changes   %2)*( changes   %2);
            ydir = -( changes   %4 -2 + (changes+1)%2)*((changes+1)%2);
          }

        if(y==ydir*int32_t(offset-2) && ydir)
          {
            if(ydir==-1)
              offset = offset -2;

            y = y+ydir;
            changes = changes + 1;
            xdir =  ((changes+1)%4 -2 +  changes   %2)*( changes   %2);
            ydir = -( changes   %4 -2 + (changes+1)%2)*((changes+1)%2);
          }

        i = i+2;
      }

    bits modebits = get_mode_bits();
    for(int32_t i=-eye_size; i<=eye_size; i++)
      {
        for(int32_t j=-eye_size; j<=eye_size; j++)
          {
            auto val = max(abs(i),abs(j));

            if(val==eye_size)
              {
                auto mode_index = (j==-eye_size || i==eye_size) ? i+j+2*eye_size     :
                                                  (j==eye_size) ? 10 -i+j+2*eye_size : 20 -i-j+2*eye_size;

                val = !modebits[mode_index];
              }

            code_matrix[grid_offset+i][grid_offset+j] = (val+1)%2;
          }
      }

    string svg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                 "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n"
                 "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" viewBox=\"0 0 ";

    svg += to_string(size+2*q_zone) + " " + to_string(size+2*q_zone);

    svg += "\" stroke=\"none\">\n"
           "  <rect width=\"100%\" height=\"100%\" fill=\"#FFFFFF\"/>\n"
           "  <path d=\"";


    for(size_t i=0; i<size; i++)
      {
        for(size_t j=0; j<size; j++)
          {
            if(code_matrix[i][j])
              svg += "M" + to_string(i+q_zone) + "," + to_string(j+q_zone) + "h1v1h-1z ";
          }
      }

    svg += "\" fill=\"#000000\"/>\n"
           "</svg>";

    return svg;
  }

  /*!
   * \brief Getter for the codewords converted to a vector of booleans.
   * \return The codewords converted to a vector of booleans.
   */
  bits Aztec::get_bits()
  {
    bits bitstream;

    auto codewords = Code_words;
    codewords.insert(codewords.end(), ECC_words.begin(), ECC_words.end());

    for(size_t i=0; i<codewords.size(); i++)
      {
        auto codeword = codewords[i];
        for(size_t j=1<<(Codeword_size-1); j>0; j>>=1)
            bitstream.push_back(codeword & j);
      }

    return bitstream;
  }

  /*!
   * \brief Getter for the mode message layer converted to a vector of booleans.
   * \return The mode message layer converted to a vector of booleans.
   */
  bits Aztec::get_mode_bits()
  {
    vector<bits> orientationbits = { { 1, 1, 1 },
                                     { 0, 1, 1 },
                                     { 1, 0, 0 },
                                     { 0, 0, 0 } };

    bits all_modebits;

    auto no_modewords = Mode_words.size();
    uint32_t bitindex = 0;

    for(size_t i=0; i<no_modewords; i++)
      {
        auto modeword = Mode_words[i];

        for(size_t j=1<<3; j>0; j>>=1)
          {
            if((bitindex%no_modewords)==0)
              {
                auto orientation = orientationbits[bitindex/no_modewords];
                all_modebits.insert(all_modebits.end(), orientation.begin(), orientation.end());
              }

            all_modebits.push_back(modeword&j);
            bitindex++;
          }
      }

    // first bit to last because orientation mark starts below corner
    all_modebits.push_back(all_modebits[0]);
    all_modebits.erase(all_modebits.begin());

    return all_modebits;
  }

  /*!
   * \brief Function to calculate the required number of layers to encode the data.
   * \return The required number of layers to encode the data.
   */
  uint32_t Aztec::calculate_number_of_layers()
  {
    uint32_t data_size = Data.size()*5;

    /// \todo does not take into account +3 codewords
    uint32_t ecc_size = ceil(data_size*0.23);

    uint32_t total_size_compact = data_size + ecc_size;
    uint32_t total_size_full    = data_size + ecc_size;

    uint32_t width_compact             = ceil(sqrt(total_size_compact + 121)) - 11;
    u_int32_t number_of_layers_compact = width_compact/4  + (width_compact % 4 != 0);

    uint32_t width_full             = ceil(sqrt(total_size_full+196)) - 14;
    u_int32_t number_of_layers_full = width_full/4  + (width_full % 4 != 0);

    if(number_of_layers_compact <= 4)
      Number_of_layers = number_of_layers_compact;
    else
      Number_of_layers = number_of_layers_full;

    return Number_of_layers;
  }

  /*!
   * \brief Function to calculate the number of codewords and codeword size to encode the data.
   * \return The number of codewords and codeword size required to encode the data.
   */
  uint32_t Aztec::calculate_number_of_codewords()
  {
    const uint32_t layers = calculate_number_of_layers();

    if(layers == 0)
      throw runtime_error("data too small to fit into an aztec code");

    if(layers>32)
      throw runtime_error("data too large to fit into an aztec code");

    Codeword_size = (layers>0)  * 6 +
                    (layers>2)  * 2 +
                    (layers>8)  * 2 +
                    (layers>22) * 2;

    uint32_t data_size = Data.size()*5;
    Number_of_codewords = data_size/Codeword_size + (data_size % Codeword_size != 0);

    return Number_of_codewords;
  }

  /*!
   * \brief Coverts the encoded data words to words of the required Codeword_size
   */
  void Aztec::data_to_codewords()
  {
    const uint32_t mask = (1<<Codeword_size) - 1;

    vector<uint32_t> codewords;

    uint8_t filled_bits = 0;
    uint32_t codeword = 0;

    for(size_t i=0; i<Data.size(); i++)
      {
        uint32_t byte = Data[i];

        codeword |= (byte & mask);

        filled_bits += 5;
        if(filled_bits >= Codeword_size)
          {
            filled_bits %= Codeword_size;

            codeword >>= filled_bits;
            codewords.push_back(codeword & mask);

            codeword = byte & ((1<<filled_bits)-1);
          }

        codeword <<= 5;
      }

    if(filled_bits && filled_bits<Codeword_size)
      {
        auto shift = int32_t(Codeword_size)-filled_bits-5;
        if(shift >0)
          codeword <<= shift;
        else
          codeword >>= -shift;

        codeword |= (1 << (Codeword_size-filled_bits)) -1;
        if(codeword == mask || codeword == 0)
            codeword ^= 1;

        codewords.push_back(codeword);
      }

    Code_words = codewords;
    Number_of_codewords = Code_words.size();
  }

  /*!
   * \brief Bitstuffing function to pad the codewords to the required length.
   */
  void Aztec::bitstuff_codewords()
  {
    /// \todo take into account existing padding

    const uint32_t mask = (1<<Codeword_size)-1;

    uint32_t         codeword = 0;
    vector<uint32_t> bitstuffed;
    uint8_t          codebit = 0;

    for(size_t i=0; i<Number_of_codewords; i++)
      {
        uint32_t byte = Code_words[i];

        for(size_t j=1; j < uint32_t(1<<Codeword_size); j<<=1)
          {
            codeword |= (byte&j);

            codebit++;

            if(codebit == Codeword_size)
              {
                bool stuffed = 0;
                if(codeword == mask || codeword == 0)
                  {
                    codeword ^= 1;
                    stuffed = 1;
                  }

                bitstuffed.push_back(codeword);
                codeword = stuffed;
                codebit  = stuffed;
              }
          }
      }

    while(codebit && codebit<Codeword_size)
      {
        codeword |= 1;
        codeword <<= 1;
        codeword |= 1;
        codebit++;
      }

    if(codebit == Codeword_size)
      {
        if(codeword == mask || codeword == 0)
            codeword ^= 1;

        bitstuffed.push_back(codeword);
      }

    Code_words = bitstuffed;
    Number_of_codewords = Code_words.size();
  }

  /*!
   * \brief Calculates the ECC words for the barcode.
   * \todo rename or actually add
   */
  void Aztec::add_ecc_codewords()
  {
    uint32_t eye_size  = Number_of_layers>4 ? 15 : 11;
    uint32_t width     = Number_of_layers*4 + eye_size;
    uint32_t code_bits = width*width - eye_size*eye_size;
    uint32_t ecc_bits  = code_bits - Number_of_codewords*Codeword_size;

    ECC_size = ecc_bits/Codeword_size;

    uint32_t prime = 0;
    switch(Codeword_size)
      {
      case  6: prime =   67; break;
      case  8: prime =  301; break;
      case 10: prime = 1033; break;
      case 12: prime = 4201; break;
      }

    ECC_words = ReedSolomon::get_check_words(Code_words, Codeword_size, ECC_size, prime);
  }

  /*!
   * \brief Adds ECC to the modewords.
   * \todo check naming of this function
   */
  void Aztec::add_modewords()
  {
    const uint32_t prime = 19;
    const uint32_t message_bitsize = 4;

    /// \todo support full size code

    Mode_words.push_back((((Number_of_layers-1)    & 0b000011) << 2) +
                         (((Number_of_codewords-1) & 0b110000) >> 4));
    Mode_words.push_back(  (Number_of_codewords-1) & 0b001111);


    auto ecc_words = ReedSolomon::get_check_words(Mode_words, message_bitsize, 5, prime);

    Mode_words.insert(Mode_words.end(), ecc_words.begin(), ecc_words.end());
  }
}
