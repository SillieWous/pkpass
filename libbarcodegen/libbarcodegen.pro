TEMPLATE = lib
DEFINES += LIBBARCODEGEN_LIBRARY
CONFIG += c++17
CONFIG -= qt

CONFIG(debug, debug|release):DEFINES += DEBUG

VERSION = 0.0.1

TARGET = barcodegen

SOURCES += \
  src/aztec.cpp \
  src/code128.cpp \
  src/ean.cpp \
  src/encoding.cpp \
  src/factory.cpp \
  src/pdf417.cpp \
  src/qrcode.cpp \
  src/qrencoding.cpp \
  src/reedsolomon.cpp \
  src/svg.cpp

HEADERS += \
  include/aztec.h \
  include/code128.h \
  include/ean.h \
  include/encoding.h \
  include/exception.h \
  include/libbarcodegen.h \
  include/pdf417.h \
  include/qrcode.h \
  include/qrencoding.h \
  include/reedsolomon.h \
  include/svg.h

OTHER_FILES += \
  aztec.xml \
  code128.xml \
  manifest-deb.yml \
  qr.xml \
  README.md \
  RELEASE_NOTES.md

INCLUDEPATH += \
  include

LIBS += \
  -lboost_filesystem

ARCH=$$(TARGET_ARCH)
equals(ARCH, "amd64") {
  QMAKE_POST_LINK += cp $$PWD/code128.xml $$PWD/aztec.xml $$PWD/pdf417.xml $$PWD/qr.xml $$OUT_PWD/../test_pkpass && \
}

QMAKE_POST_LINK += \
  cp $$PWD/code128.xml $$PWD/aztec.xml $$PWD/pdf417.xml $$PWD/qr.xml $$OUT_PWD/ && \
  cp $$PWD/code128.xml $$PWD/aztec.xml $$PWD/pdf417.xml $$PWD/qr.xml $$OUT_PWD/../app_pkpass && \
  $$PWD/../package_deb.sh -f $$PWD/manifest-deb.yml -w $$OUT_PWD -a $$(TARGET_ARCH) &&\
  $$PWD/../doc/generate-docs.sh -d $$OUT_PWD -p libbarcodegen -f $$PWD/../doc

encoding.files= \
  code128.xml \
  aztec.xml \
  pdf417.xml \
  qr.xml

unix {
  target.path   = .local/lib
  encoding.path = .local/share/barcodegen
}
!isEmpty(target.path): INSTALLS += target encoding
