QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_test_pkpass.cpp

LIBS += \
  -L$$OUT_PWD/../libbarcodegen/ -lbarcodegen \
  -L$$OUT_PWD/../libpkpass/ -lpkpass \
  -ljsoncpp

INCLUDEPATH += \
  $$PWD/../libbarcodegen/include \
  $$PWD/../libpkpass/include

TEST_RESOURCES = \
  $$PWD/resources/pass.pkpass \
  $$PWD/resources/aztec_valid.svg \
  $$PWD/resources/code128_valid.svg \
  $$PWD/resources/ean13_valid.svg \
  $$PWD/resources/pdf417_valid.svg \
  $$PWD/resources/qr_valid.svg

OTHER_FILES += $$TEST_RESOURCES

QMAKE_POST_LINK += \
  cp $$TEST_RESOURCES $$OUT_PWD/

# Default rules for deployment.
unix {
  target.path = deploy/test/
}
!isEmpty(target.path): INSTALLS += target
