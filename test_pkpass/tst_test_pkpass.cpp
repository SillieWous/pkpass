#include <QtTest>

#include <libpkpass.h>
#include <libbarcodegen.h>

#include <reedsolomon.h>

#include <fstream>

using namespace std;

class test_pkpass : public QObject
{
  Q_OBJECT

public:
  test_pkpass()  {}
  ~test_pkpass() {}

private slots:
  void initTestCase();
  void cleanupTestCase();
  void printPass();
  void createBarcode();

private:
  shared_ptr<pkpass::IPass> Pass;

  string printPassFields(pkpass::IPassFields& passfields);
  void Step(string message, const function<void()>& action);
};

void test_pkpass::initTestCase()
{
}

void test_pkpass::cleanupTestCase()
{

}

void test_pkpass::printPass()
{
  Step("Create pass", [=]()
    {
      int flags = !pkpass::IPass::ECreationFlags::ALLOW_SELF_SIGNED;

      Pass = pkpass::IPass::create("pass.pkpass", flags);
    }
  );

  Step("Print pass", [=]()
    {
      qInfo() << "AppLaunchUrl: "             << Pass->get_app_launch_url().c_str()                 << Qt::endl
            //<< "AssociatedAppIdentifiers: " << Pass->get_associated_app_identifiers()             << Qt::endl
              << "BackgroundColour: "         << Pass->get_background_colour().c_str()              << Qt::endl
            //<< "Barcodes: "                 << Pass->get_barcodes()                               << Qt::endl
            //<< "Beacons: "                  << Pass->get_beacons()                                << Qt::endl
            //<< "BoardingPass: "             << printPassFields(Pass->get_boarding_pass()).c_str() << Qt::endl
            //<< "Coupon: "                   << Pass.->oupon                                       << Qt::endl
              << "Description: "              << Pass->get_description().c_str()                    << Qt::endl
              << "EventTicket: "              << printPassFields(Pass->get_event_ticket()).c_str()  << Qt::endl
              << "ExpirationDate: "           << Pass->get_expiration_date().c_str()                << Qt::endl
              << "ForegroundColour: "         << Pass->get_foreground_colour().c_str()              << Qt::endl
              << "FormatVersion: "            << Pass->get_format_version()                         << Qt::endl
            //<< "Generic: "                  << printPassFields(Pass->get_generic()).c_str()       << Qt::endl
              << "GroupingIdentifier: "       << Pass->get_grouping_identifier().c_str()            << Qt::endl
              << "LabelColour: "              << Pass->get_label_colour().c_str()                   << Qt::endl
            //<< "Locations: "                << Pass->get_locations                                << Qt::endl
              << "MaxDistance: "              << Pass->get_max_distance()                           << Qt::endl
            //<< "Nfc: "                      << Pass->get_nfc()                                    << Qt::endl
              << "OrganisationName: "         << Pass->get_organisation_name().c_str()              << Qt::endl
              << "PassTypeIdentifier: "       << Pass->get_pass_type_identifier().c_str()           << Qt::endl
              << "RelevantDate: "             << Pass->get_relevant_date().c_str()                  << Qt::endl
              << "SerialNumber: "             << Pass->get_serial_number().c_str()                  << Qt::endl
              << "SharingProhibited: "        << Pass->is_sharing_prohibited()                      << Qt::endl
            //<< "StoreCard: "                << printPassFields(Pass->get_store_card()).c_str()    << Qt::endl
              << "SuppressStripShine: "       << Pass->get_suppress_strip_shine()                   << Qt::endl
              << "TeamIdentifier: "           << Pass->get_team_identifier().c_str()                << Qt::endl
              << "UserInfo: "                 << Pass->get_user_info().asString().c_str()           << Qt::endl
              << "Voided: "                   << Pass->is_voided()                                  << Qt::endl
              << "WebServiceUrl: "            << Pass->get_web_service_url().c_str()                << Qt::endl;
    });
}

void test_pkpass::createBarcode()
{
  using namespace barcodegen;

  Step("Create Aztec barcode", []()
    {
      auto barcode = IBarcode::create(EFormat::AZTEC, "This is an example Aztec symbol for Wikipedia.");
      //auto barcode = IBarcode::create(EFormat::AZTEC, "Wikipedia, the free encyclopedia");

      string svg_str = barcode->to_svg_string(5);
      auto stream = std::ofstream("aztec.svg");
      stream << svg_str;
      stream.close();

      auto valid_stream = std::ifstream("aztec_valid.svg");
      string valid_svg_str((std::istreambuf_iterator<char>(valid_stream)),(std::istreambuf_iterator<char>()));
      valid_stream.close();

      Q_ASSERT(svg_str == valid_svg_str);
    });

  Step("Create EAN13 barcode", []()
    {
      auto barcode = IEAN::create(EFormat::EAN13, "262166899099");

      string svg_str = barcode->to_svg_string(5);
      auto stream = std::ofstream("ean13.svg");
      stream << svg_str;
      stream.close();

      auto valid_stream = std::ifstream("ean13_valid.svg");
      string valid_svg_str((std::istreambuf_iterator<char>(valid_stream)),(std::istreambuf_iterator<char>()));
      valid_stream.close();

      Q_ASSERT(svg_str == valid_svg_str);
    });

  Step("Create Code128 barcode", []()
    {
      auto barcode = IBarcode::create(EFormat::CODE128, "RI476394652CH");
      //auto barcode = IBarcode::create(EFormat::CODE128, "Wikipedia");

      string svg_str = barcode->to_svg_string(5);
      auto stream = std::ofstream("code128.svg");
      stream << svg_str;
      stream.close();

      auto valid_stream = std::ifstream("code128_valid.svg");
      string valid_svg_str((std::istreambuf_iterator<char>(valid_stream)),(std::istreambuf_iterator<char>()));
      valid_stream.close();

      Q_ASSERT(svg_str == valid_svg_str);
    });

  Step("Create PDF417 barcode", []()
    {
      //auto barcode = IPdf417::create("1234567890");
      //auto barcode = IPdf417::create("Wikipedia, the free encyclopedia");
      auto barcode = IPdf417::create("Wikipedia");

      string svg_str = barcode->to_svg_string(1, 5);
      auto stream = std::ofstream("pdf417.svg");
      stream << svg_str;
      stream.close();

      auto valid_stream = std::ifstream("pdf417_valid.svg");
      string valid_svg_str((std::istreambuf_iterator<char>(valid_stream)),(std::istreambuf_iterator<char>()));
      valid_stream.close();

      Q_ASSERT(svg_str == valid_svg_str);
    });
  Step("Create QR barcode", []()
    {
      auto barcode = IQr::create("Version 3 QR Code", IQr::EErrorCorrection::HIGH);
      //auto barcode = IQr::create("HELLO WORLD", IQr::EErrorCorrection::QUARTILE);
      //auto barcode = IQr::create("Ver1", IQr::EErrorCorrection::HIGH);

      string svg_str = barcode->to_svg_string(5);
      auto stream = std::ofstream("qr.svg");
      stream << svg_str;
      stream.close();

      auto valid_stream = std::ifstream("qr.svg");
      string valid_svg_str((std::istreambuf_iterator<char>(valid_stream)),(std::istreambuf_iterator<char>()));
      valid_stream.close();

      Q_ASSERT(svg_str == valid_svg_str);
    });
}

string test_pkpass::printPassFields(pkpass::IPassFields& passfields)
{
  string text;
  for(auto const& field : passfields.get_header_fields())
    text += field.second->get_key() + "\n" + field.second->get_label()+ "\n\n";

  for(auto const& field : passfields.get_primary_fields())
    text += field.second->get_key() + "\n" + field.second->get_label() + "\n\n";

  for(auto const& field : passfields.get_secondary_fields())
    text += field.second->get_key() + "\n" + field.second->get_label() + "\n\n";

  for(auto const& field : passfields.get_auxiliary_fields())
    text += field.second->get_key() + "\n" + field.second->get_label() + "\n\n";

  for(auto const& field : passfields.get_back_fields())
    text += field.second->get_key() + "\n" + field.second->get_label() + "\n\n";

  return text;
}

void test_pkpass::Step(string message, const std::function<void()>& action)
{
  qInfo() << message.c_str() << Qt::endl;

  try
  {
    action();
  }
  catch (pkpass::IException& e)
  {
    std::string message =
        "What: " + e.what() + "\n"
        "  Where " + e.where();
    QFAIL(message.c_str());
  }
  catch(std::exception& e)
  {
    std::string message =
        "What: " + string(e.what()) + "\n";
    QFAIL(message.c_str());

  }
}

QTEST_APPLESS_MAIN(test_pkpass)

#include "tst_test_pkpass.moc"
