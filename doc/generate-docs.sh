#! /bin/bash

OPTIND=1
while getopts d:f:hp: OPT; do
  case $OPT in
    d)  OUTDIR=${OPTARG}
        ;;
    f)  DOXYFILE_DIR=${OPTARG}
        ;;
    h)  show_help
        exit 0
        ;;
    p)  PROJECT=${OPTARG}
        ;;
    *)  show_help >&2
        exit 1
        ;;
  esac
done

cd ${DOXYFILE_DIR}

for INTERNAL in "YES" "NO"; do
  export INTERNAL=$INTERNAL
  export PROJECT_NAME=${PROJECT}
  export PROJECT_DIR=../${PROJECT_NAME}

  if [ $INTERNAL == "YES" ]; then
    export OUT_PWD=${OUTDIR}/doc/internal
  else
    export OUT_PWD=${OUTDIR}/doc/external
  fi

  mkdir -p ${OUT_PWD}

  doxygen doxygen.conf
done
