#include "json.h"

namespace Json
{
  /*! \internal
   * \brief Helper function for trying to retrieve a json value from another.
   * \param key Key of the child to be accessed.
   * \param value Parent value which may contain the key.
   * \return Child value, or null value if key is not found.
   */
  Value TryGetValue(string key, Value value)
  {
    try
    {
      return value[key];
    }
    catch (std::exception& e)
    {
      return nullValue;
    }
  }
}
