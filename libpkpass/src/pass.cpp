#include "pass.h"

#include "json.h"
#include "exception.h"

#include <filesystem>
#include <iostream>
#include <fstream>
#include <zip.h>
#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <openssl/pkcs7.h>
#include <openssl/safestack.h>
#include <openssl/x509.h>

using namespace Json;

namespace pkpass
{
  // documented in libpkpass.h
  shared_ptr<IPass> IPass::create(string filename, int flags)
  {
    return make_shared<Pass>(filename, flags);
  }

  /*!
   * \brief Pass::Pass constructor
   * \param filename Filename of the '.pkpass' to be created.
   * \param flags Settings of how the pass should be created.
   */
  Pass::Pass(string filename, int flags)
    : FileName(filename)
    , CreationFlags(flags)
  {
    bool temp    = flags & IPass::ECreationFlags::TEMPORARY_PASS;
    bool fromdir = flags & IPass::ECreationFlags::LOAD_FROM_DIR;

    if(fromdir)
      StorageDirectory = filename;
    else if(temp)
      StorageDirectory = Program_tempdir + Sha1Sum(FileName) + "/";
    else
      StorageDirectory = Program_dir + Sha1Sum(FileName) + "/";

    if(temp && filesystem::exists(StorageDirectory))
      filesystem::remove_all(StorageDirectory);

    if(!fromdir && (temp || !filesystem::exists(StorageDirectory)))
      UnpackPass();

    try
    {
      if(CreationFlags & ECreationFlags::VERIFY_SIGNATURE)
        VerifySignature();
      if(CreationFlags & ECreationFlags::VERIFY_MANIFEST)
        VerifyManifest();

      ParsePass();
    }
    catch (IException&)
    {
      if(!fromdir && (temp || !filesystem::exists(StorageDirectory)))
        filesystem::remove_all(StorageDirectory);
      throw;
    }
  }

  /*!
   * \brief Pass::~Pass Destructor
   */
  Pass::~Pass()
  {
    if(CreationFlags & IPass::ECreationFlags::TEMPORARY_PASS)
      filesystem::remove_all(StorageDirectory);
  }

  /*!
   * \brief Unpacks the pass to the storage directory.
   * \exception IException::Exception_t::NotFound If the specified pass file cannot found.
   * \exception IException::Exception_t::Internal If the pass cannot be unpacked succesfully.
   */
  void Pass::UnpackPass()
  {
    string archiveName = FileName;

    filesystem::create_directories(StorageDirectory);

    struct zip *zipArchive;
    int err;

    if (!(zipArchive = zip_open(archiveName.c_str(), 0, &err)))
      {
        char buf[100];
        zip_error_to_str(buf, sizeof(buf), err, errno);
        throw NotFoundException("can't open zip archive" + archiveName + ": " + buf);
      }

    for (int i = 0; i < zip_get_num_entries(zipArchive, 0); i++)
      {
        struct zip_stat fileStat;

        if (!zip_stat_index(zipArchive, i, 0, &fileStat))
          {
            if (fileStat.name[strlen(fileStat.name) - 1] == '/')
              filesystem::create_directories(StorageDirectory + fileStat.name);
            else
              {
                struct zip_file *zippedFile;
                if (!(zippedFile = zip_fopen_index(zipArchive, i, 0)))
                  throw InternalException("error opening zipped file " + string(fileStat.name));

                auto file = fopen((StorageDirectory + fileStat.name).c_str(), "w");
                if (!file)
                  throw InternalException("error open output stream for file: " + string(fileStat.name));

                uint sum = 0;
                int len = 0;
                do
                  {
                    char buf[256];
                    len = zip_fread(zippedFile, buf, 100);
                    if (len < 0)
                      throw InternalException("error reading zipped file: " + string(fileStat.name));

                    fwrite(buf, len, 1, file);
                    sum += len;
                  }
                while (sum != fileStat.size);

                fclose(file);
                zip_fclose(zippedFile);
              }
          }
      }

    if (zip_close(zipArchive) == -1)
        throw InternalException("can't close zip archive " + archiveName);
  }

  ///
  /*!
   * \brief Function to verify the 'signature' file is a valid signature of 'manifest.json' file,
   *        and the signature chain is trusted.
   * \return Bool indicating if the signature was succesfully verified, taking creation flags into account.
   * \exception IException::Exception_t::NotFound If any of the relevant files cannot be opened.
   * \exception IException::Exception_t::Internal If any of the relevant files cannot be parsed correctly.
   * \exception IException::Exception_t::Unsupported If CA certificated does not support digital file signing as a
   *            purposes.
   *            Occurs if a pass is signed by the Apple CA, for which only a certificate signing cert is available.
   *            Setting the ALLOW_UNSUPPORTED_CERTIFICATE_PURPOSE flag prevents this exception.
   * \exception IException::Exception_t::Argument If the chain of signatures is fully contained in the pass.
   *            Setting the ALLOW_SELF_SIGNED flag prevents this exception.
   * \exception IException::Exception_t::ArgumentException If the signature cannot be verified.
   */
  bool Pass::VerifySignature()
  {
    vector<IException> exceptions;
    string reason;

    /// \todo where is this for MS Windows?
    string caCertFullpath = "/etc/ssl/certs/ca-certificates.crt";
    string signaturePath  = StorageDirectory + "signature";
    string contentPath    = StorageDirectory + "manifest.json";

    BIO *signatureFile    = NULL,
        *caCertFile       = NULL,
        *contentFile      = NULL;
    X509_STORE *certStore = NULL;
    X509 *caCert          = NULL;
    PKCS7 *signature      = NULL;

    STACK_OF(X509) *selfCert = NULL;

    bool verified = false, selfSigned = true, unsupported = false;

    OpenSSL_add_all_algorithms();
    ERR_load_crypto_strings();

    caCertFile    = BIO_new_file(caCertFullpath.c_str(), "r");
    signatureFile = BIO_new_file(signaturePath.c_str(), "r");
    contentFile   = BIO_new_file(contentPath.c_str(), "r");

    if(!caCertFile || !signatureFile || !contentFile)
      {
        if (!caCertFile)
          exceptions.push_back(NotFoundException("error opening ca cert file not opened: " + caCertFullpath));

        if (!signatureFile)
          exceptions.push_back(NotFoundException("error opening signature file: " + signaturePath));

        if (!contentFile)
          exceptions.push_back(NotFoundException("error opening content file: " + contentPath));
      }
    else
      {
        certStore = X509_STORE_new();

        if (!(caCert = PEM_read_bio_X509(caCertFile, NULL, 0, NULL)))
          exceptions.push_back(InternalException("error parsing ca cert file: " + caCertFullpath));

        if (!X509_STORE_add_cert(certStore, caCert))
          exceptions.push_back(InternalException("error adding cert file to cert store: " + caCertFullpath));

        if (!(signature = d2i_PKCS7_bio(signatureFile, NULL)))
          exceptions.push_back(InternalException("error parsing signature file: " + signaturePath));
        else
          selfCert = signature->d.sign->cert;

        if(signature && selfCert && certStore)
          {
            selfSigned = PKCS7_verify(signature, selfCert, NULL, contentFile, NULL, 0);
            verified   = PKCS7_verify(signature, selfCert, certStore, contentFile, NULL, 0);

            if(!verified)
              {
                const char *data;
                ERR_peek_last_error_line_data(NULL, NULL, &data, NULL);

                reason = string(data);
                unsupported = ("Verify error:unsupported certificate purpose" == reason);
              }
          }
      }

    PKCS7_free(signature);
    X509_free(caCert);
    X509_STORE_free(certStore);
    BIO_free(caCertFile);
    BIO_free(contentFile);
    BIO_free(signatureFile);

    if(!exceptions.empty())
      throw exceptions[0];

    if(selfSigned && !(CreationFlags & ECreationFlags::ALLOW_SELF_SIGNED))
      throw ArgumentException("self signed pass");

    if(unsupported && !(CreationFlags & ECreationFlags::ALLOW_UNSUPPORTED_CERTIFICATE_PURPOSE))
      throw UnsupportedException(reason);

    if(!verified && !unsupported)
        throw ArgumentException("pass signature could not be verified: " + reason);

    return verified;
  }

  /*!
   * \brief Function which verifies if the files match the SHA1 hash as listed in 'manifest.json'
   * \return Bool indicating if the manifest was succesfully verified.
   */
  bool Pass::VerifyManifest()
  {
    auto fileStream = ifstream(StorageDirectory + "manifest.json");

    CharReaderBuilder rbuilder;
    rbuilder["collectComments"] = false;

    string errs;
    JsonData jsonData;

    if(!parseFromStream(rbuilder, fileStream, &jsonData, &errs))
      throw InternalException("error while parsing pass. " + errs);

    for(auto it = jsonData.begin(); it != jsonData.end(); it++)
      {
        const auto filename = StorageDirectory + it.name();

        const auto expectedHash = jsonData[it.name()].asString();
        const auto calculatedHash = Sha1Sum(filename);

        if(calculatedHash != expectedHash)
          throw ArgumentException("error hash of file '" + filename + "'=" + calculatedHash + " expected=" + expectedHash);
      }

    return true;
  }

  /*!
   * \brief Process the content of the actual 'pass.json' file.
   *        Fills all relevant members and check if required data is present.
   */
  void Pass::ParsePass()
  {
    auto fileStream = ifstream(StorageDirectory + "pass.json");

    CharReaderBuilder rbuilder;
    rbuilder["collectComments"] = false;

    string errs;
    JsonData jsonData;

    if(!parseFromStream(rbuilder, fileStream, &jsonData, &errs))
      throw InternalException("error while parsing pass. " + errs);

    Description        = jsonData["description"].asString();
    OrganisationName   = jsonData["organizationName"].asString();
    PassTypeIdentifier = jsonData["passTypeIdentifier"].asString();
    SerialNumber       = jsonData["serialNumber"].asString();
    TeamIdentifier     = jsonData["teamIdentifier"].asString();
    if(1 != jsonData["formatVersion"].asInt())
      throw UnsupportedException("error format version must be 1 got " + to_string(jsonData["fromatVersion"].asInt()));

    jsonData.removeMember("formatVersion");

    Coupon       = TryGetValue("coupon", jsonData)       ? new pkpass::Coupon(jsonData["coupon"])            : nullptr;
    EventTicket  = TryGetValue("eventTicket", jsonData)  ? new pkpass::EventTicket(jsonData["eventTicket"])  : nullptr;
    Generic      = TryGetValue("generic", jsonData)      ? new pkpass::Generic(jsonData["generic"])          : nullptr;
    StoreCard    = TryGetValue("storeCard", jsonData)    ? new pkpass::StoreCard(jsonData["storeCard"])      : nullptr;
    BoardingPass = TryGetValue("boardingPass", jsonData) ? new pkpass::BoardingPass(jsonData["boardinPass"]) : nullptr;
    Nfc          = TryGetValue("nfc", jsonData)          ? new pkpass::Nfc(jsonData["nfc"])                  : nullptr;

    if(TryGetValue("barcode", jsonData)) Barcodes.push_back(new Barcode(jsonData["barcode"]));
    if(TryGetValue("barcodes", jsonData))
      for(const auto& barcode : jsonData["barcodes"])
        Barcodes.push_back(new Barcode(barcode));

    if(TryGetValue("beacons", jsonData))
      for(const auto& beacon : jsonData["beacons"])
        Beacons.push_back(new Beacon(beacon));

    if(TryGetValue("locations", jsonData))
      for(const auto& location : jsonData["locations"])
        Locations.push_back(new Location(location));

    MaxDistance         = TryGetValue("maxDistance", jsonData)         ? jsonData["maxDistance"].asDouble()         : -1;
    BackgroundColour    = TryGetValue("backgroundColor", jsonData)     ? jsonData["backgroundColor"].asString()     : BackgroundColour;
    ForegroundColour    = TryGetValue("foregroundColor", jsonData)     ? jsonData["foregroundColor"].asString()     : ForegroundColour;
    LabelColour         = TryGetValue("labelColor", jsonData)          ? jsonData["labelColor"].asString()          : LabelColour;
    ExpirationDate      = TryGetValue("expirationDate", jsonData)      ? jsonData["expirationDate"].asString()      : ExpirationDate;
    RelevantDate        = TryGetValue("relevantDate", jsonData)        ? jsonData["relevantDate"].asString()        : RelevantDate;
    GroupingIdentifier  = TryGetValue("groupingIdentifier", jsonData)  ? jsonData["groupingIdentifier"].asString()  : GroupingIdentifier;
    AppLaunchUrl        = TryGetValue("appLaunchUrl", jsonData)        ? jsonData["appLaunchUrl"].asString()        : AppLaunchUrl;
    AuthenticationToken = TryGetValue("authenticationToken", jsonData) ? jsonData["authenticationToken"].asString() : AuthenticationToken;
    WebServiceUrl       = TryGetValue("webServiceUrl", jsonData)       ? jsonData["webServiceUrl"].asString()       : WebServiceUrl;

    SharingProhibited  = TryGetValue("sharingProhibited", jsonData)  ? jsonData["sharingProhibited"].asBool()  : SharingProhibited;
    SuppressStripShine = TryGetValue("suppressStripShine", jsonData) ? jsonData["suppressStripShine"].asBool() : SuppressStripShine;
    Voided             = TryGetValue("voided", jsonData)             ? jsonData["voided"].asBool()             : Voided;

    if(TryGetValue("associatedStoreIdentifiers", jsonData))
      for(const auto& id : jsonData["associatedStoreIdentifiers"])
        AssociatedAppIdentifiers.push_back(id.asString());

    UserInfo = TryGetValue("userInfo", jsonData);
  }

  /*!
   * \brief Pass::Sha1Sum
   * \param filename Filename of file for which the SHA1 sum needs to be calculated.
   * \return SHA1 sum of the file read as bytes.
   * \exception IException::Exception_t::Internal An error occured computing the SHA1 sum.
   */
  string Pass::Sha1Sum(string filename)
  {
    SHA_CTX shaCtx;

    SHA1_Init(&shaCtx);

    char buf[256];
    std::ifstream file(filename, std::ifstream::binary);
    while (file.good())
      {
        file.read(buf, sizeof(buf));
        if(!SHA1_Update(&shaCtx, buf, file.gcount()))
          throw InternalException("error comupting SHA1 of file: " + filename);
      }

    unsigned char result[SHA_DIGEST_LENGTH];
    if(!SHA1_Final(result, &shaCtx))
      throw InternalException("error comupting SHA1 of file: " + filename);

    std::stringstream shastr;
    shastr << std::hex << std::setfill('0');
    for (const auto &byte: result)
      shastr << std::setw(2) << (int)byte;

    return shastr.str();
  }
}
