#include "passfields.h"
#include "passfieldcontent.h"

namespace pkpass
{
  /*!
   * \brief PassFields::PassFields constructor.
   * \param data json data defining the pass fields.
   */
  PassFields::PassFields(JsonData data)
  {
    auto auxiliary = data["auxiliaryFields"];
    auto back      = data["backFields"];
    auto header    = data["headerFields"];
    auto primary   = data["primaryFields"];
    auto secondary = data["secondaryFields"];

    for(auto it = auxiliary.begin(); it != auxiliary.end(); it++)
      {
        auto value = auxiliary[it.index()];
        auto key   = value["key"].asString();
        auto field = new pkpass::AuxiliaryField(value);
        AuxiliaryFields[key] = field;
      }

    for(auto it = back.begin(); it != back.end(); it++)
      {
        auto value = back[it.index()];
        auto key   = value["key"].asString();
        auto field = new pkpass::BackField(value);
        BackFields[key] = field;
      }

    for(auto it = header.begin(); it != header.end(); it++)
      {
        auto value = header[it.index()];
        auto key   = value["key"].asString();
        auto field = new pkpass::HeaderField(value);
        HeaderFields[key] = field;
      }

    for(auto it = primary.begin(); it != primary.end(); it++)
      {
        auto value = primary[it.index()];
        auto key   = value["key"].asString();
        auto field = new pkpass::PrimaryField(value);
        PrimaryFields[key] = field;
      }

    for(auto it = secondary.begin(); it != secondary.end(); it++)
      {
        auto value = secondary[it.index()];
        auto key   = value["key"].asString();
        auto field = new pkpass::SecondaryField(value);
        SecondaryFields[key] = field;
      }
  }
}
