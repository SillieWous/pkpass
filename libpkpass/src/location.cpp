#include "location.h"

#include "json.h"

namespace pkpass
{
  /*!
   * \brief Location::Location constructor.
   * \param data json data defining the location.
   */
  Location::Location(JsonData data)
  {
    Latitude   = data["latitude"].asDouble();
    Longtitude = data["longtitude"].asDouble();

    Altitude     = Json::TryGetValue("altitude", data)     ? data["altitude"].asDouble()     : 0;
    RelevantText = Json::TryGetValue("relevantText", data) ? data["relevantText"].asString() : RelevantText;
  }
}
