#include "beacon.h"

#include "json.h"

namespace pkpass
{
  /*!
   * \brief Beacon::Beacon constructor.
   * \param data json data defining the beacon.
   */
  Beacon::Beacon(JsonData data)
  {
    ProximityUUID = data["proximityUUID"].asString();

    Major        = Json::TryGetValue("major", data)        ? data["major"].asUInt()          : 0;
    Minor        = Json::TryGetValue("minor", data)        ? data["minor"].asUInt()          : 0;
    RelevantText = Json::TryGetValue("relevantText", data) ? data["relevantText"].asString() : RelevantText;
  }
}
