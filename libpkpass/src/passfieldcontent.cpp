#include "passfieldcontent.h"

#include "json.h"

namespace pkpass
{
  /*!
   * \brief PassFieldContent::PassFieldContent constructor.
   * \param data json data defining the pass field content.
   */
  PassFieldContent::PassFieldContent(JsonData data)
  {
    Key = data["key"].asString();
    Value = data["value"].asString();

    /// \todo use tryget for maps

    AttributedValue   = Json::TryGetValue("attributedValue", data) ? data["attributedValue"].asString() : AttributedValue;
    ChangeMessage     = Json::TryGetValue("changeMessage", data)   ? data["changeMessage"].asString()   : ChangeMessage;
    CurrencyCode      = Json::TryGetValue("currencyCode", data)    ? data["currencyCode"].asString()    : CurrencyCode;
    // DataDetectorTypes;
    if(!data["dateStyle"].empty())
      DateStyle         = EDateStyles.at(data["dateStyle"].asString());

    IgnoresTimeZone   = Json::TryGetValue("ignoresTimeZone", data) ? data["ignoresTimeZone"].asBool() : IgnoresTimeZone;
    IsRelative        = Json::TryGetValue("isRelative", data)      ? data["isRelative"].asBool()      : IsRelative;
    Label             = Json::TryGetValue("label", data)           ? data["label"].asString()         : Label;

    if(!data["numberStyle"].empty())
      NumberStyle       = ENumberStyles  .at(data["numberStyle"].asString());
    if(!data["textAlignment"].empty())
      TextAlignment     = ETextAlignments.at(data["textAlignment"].asString());
    if(!data["timeStyle"].empty())
      TimeStyle         = ETimeStyles    .at(data["timeStyle"].asString());
  }
}
