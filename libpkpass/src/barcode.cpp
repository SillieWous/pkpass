#include "barcode.h"
#include "exception.h"

#include "json.h"

#include <algorithm>

namespace pkpass
{
  /*!
   * \brief Barcode::Barcode constructor.
   * \param data json data defining the barcode.
   */
  Barcode::Barcode(JsonData data)
  {
    auto format = data["format"].asString();
    Format = Formats.at(format);

    Message = data["message"].asString();
    MessageEncoding = data["messageEncoding"].asString();

    AltText = Json::TryGetValue("altText", data) ? data["altTex"].asString() : AltText;
  }
}
