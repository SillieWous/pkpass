#include "nfc.h"

#include "json.h"

namespace pkpass
{
  /*!
   * \brief NFC::NFC constructor.
   * \param data json data defining the nfc tag.
   */
  Nfc::Nfc(JsonData data)
  {
    EncryptionPublicKey = data["encryptionPublicKey"].asString();
    Message = data["message"].asString();

    RequiresAuthentication = Json::TryGetValue("requiresAuthentication", data)
                                ? data["requiresAuthentication"].asBool() : RequiresAuthentication;
  }
}
