# Release notes

### Release 0.1.0
**DATE**\n
*Initial release. Adds basic parsing and verification.*

#### Features
* Unpacking pass to temporary locations
* Signature verification
* Parsing of the pass.json file into structured interfaces.

#### Bug fixes
*Nothing to show here*

#### Deprecated functionality
*Nothing to show here*
