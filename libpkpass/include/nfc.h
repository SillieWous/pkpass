#ifndef NFC_H
#define NFC_H

#include "libpkpass.h"

namespace pkpass
{
  /*! \internal
   * \brief The Nfc class implements logic to interface with INfc's
   */
  class Nfc : virtual public INfc
  {
  public:
    Nfc(JsonData data);

    virtual string get_encryption_public_key()   { return EncryptionPublicKey; }
    virtual string get_message()                 { return Message; }
    virtual bool   get_requires_authentication() { return RequiresAuthentication; }

  protected:
    string EncryptionPublicKey; // required
    string Message;             // required
    bool   RequiresAuthentication = false;

  };
}

#endif // NFC_H
