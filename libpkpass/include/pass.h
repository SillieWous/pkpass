#ifndef PASS_H
#define PASS_H

#include "libpkpass.h"

#include "barcode.h"
#include "beacon.h"
#include "location.h"
#include "nfc.h"
#include "passfieldcontent.h"
#include "passfields.h"

#include <filesystem>

#ifndef DEBUG
#if defined(_MSC_VER) || defined(WIN64) || defined(_WIN64) || defined(__WIN64__) || defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
const std::string Program_dir     = std::string(getenv("APPDATA")) +"/local/pkpass/passes/";
#else
const std::string Program_dir     = std::string(getenv("HOME")) +"/.local/share/pkpass/passes/";
#endif
const std::string Program_tempdir = std::filesystem::temp_directory_path().string()+"/pkpass/passes/";
#else
const std::string Program_dir     = std::string(getenv("HOME")) +"/.local/share/pkpass/passes/";
const std::string Program_tempdir = "tmp/passes/";
#endif

namespace pkpass
{
  /*! \internal
   * \brief The Pass class contains the structure of a pass.
   */
  class Pass : public virtual IPass
  {
  public:
    Pass(string filename, int flags);
    ~Pass();

    virtual string         get_description()          { return Description; }
    virtual string         get_organisation_name()    { return OrganisationName; }
    virtual string         get_pass_type_identifier() { return PassTypeIdentifier; }
    virtual string         get_serial_number()        { return SerialNumber; }
    virtual string         get_team_identifier()      { return TeamIdentifier; }
    virtual int            get_format_version()       { return FormatVersion; }

    virtual IBarcodes      get_barcodes()     { return Barcodes; }
    virtual IBeacons       get_beacons()      { return Beacons; }
    virtual ILocations     get_locations()    { return Locations; }
    virtual double         get_max_distance() { return MaxDistance; }

    virtual IBoardingPass& get_boarding_pass() { return *BoardingPass; }
    virtual ICoupon&       get_coupon()        { return *Coupon; }
    virtual IEventTicket&  get_event_ticket()  { return *EventTicket; }
    virtual IGeneric&      get_generic()       { return *Generic; }
    virtual IStoreCard&    get_store_card()    { return *StoreCard; }
    virtual INfc&          get_nfc()           { return *Nfc; }

    virtual string         get_background_colour() { return BackgroundColour; }
    virtual string         get_foreground_colour() { return ForegroundColour; }
    virtual string         get_label_colour()      { return LabelColour; }

    virtual string         get_expiration_date() { return ExpirationDate; }
    virtual string         get_relevant_date()   { return RelevantDate; }

    virtual strings        get_associated_app_identifiers() { return AssociatedAppIdentifiers; }
    virtual string         get_grouping_identifier()        { return GroupingIdentifier; }

    virtual string         get_app_launch_url()       { return AppLaunchUrl; }
    virtual string         get_authentication_token() { return AuthenticationToken; }
    virtual string         get_web_service_url()      { return WebServiceUrl; }
    virtual JsonData       get_user_info()            { return UserInfo; }
    //SemanticTags  SemanticTags;

    virtual bool           is_sharing_prohibited()    { return SharingProhibited; }
    virtual bool           get_suppress_strip_shine() { return SuppressStripShine; }
    virtual bool           is_voided()                { return Voided; }

    virtual string         get_storage_directory() { return StorageDirectory; }

  protected:
    string         Description;        // required, not localisable
    string         OrganisationName;   // required, not localisable
    string         PassTypeIdentifier; // required
    string         SerialNumber;       // required
    string         TeamIdentifier;     // required
    int            FormatVersion = 1;  // required = 1

    IBarcodes      Barcodes;
    IBeacons       Beacons;
    ILocations     Locations;
    double         MaxDistance;

    pkpass::BoardingPass *BoardingPass;
    pkpass::Coupon       *Coupon;
    pkpass::EventTicket  *EventTicket;
    pkpass::Generic      *Generic;
    pkpass::StoreCard    *StoreCard;
    pkpass::Nfc          *Nfc;

    string         BackgroundColour;
    string         ForegroundColour;
    string         LabelColour;

    string         ExpirationDate;
    string         RelevantDate;

    strings        AssociatedAppIdentifiers;
    string         GroupingIdentifier;

    string         AppLaunchUrl;
    string         AuthenticationToken;
    string         WebServiceUrl;
    JsonData       UserInfo;
    //SemanticTags  SemanticTags;

    bool           SharingProhibited = false;
    bool           SuppressStripShine = false;
    bool           Voided = false;

    string         StorageDirectory;

    string FileName;
    int    CreationFlags;

    void UnpackPass();
    bool VerifySignature();
    bool VerifyManifest();
    void ParsePass();

  private:
    string Sha1Sum(string filename);
  };
}

#endif // PASS_H
