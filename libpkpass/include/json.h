#ifndef JSON_H
#define JSON_H

#include <jsoncpp/json/json.h>

namespace Json
{
  using namespace std;

  Value TryGetValue(string key, Value value);
}

#endif // JSON_H
