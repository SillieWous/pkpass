#ifndef BEACON_H
#define BEACON_H

#include "libpkpass.h"

namespace pkpass
{
  /*! \internal
   * \brief The Beacon class implements logic to interface with IBeacons.
   */
  class Beacon : virtual public IBeacon
  {
  public:
    Beacon(JsonData data);

    virtual uint16_t get_major()          { return Major; }
    virtual uint16_t get_minor()          { return Minor; }
    virtual string   get_proximity_UUID() { return ProximityUUID; }
    virtual string   get_relevant_text()  { return RelevantText; }

  protected:
    string   ProximityUUID; // required
    uint32_t Major;
    uint32_t Minor;
    string   RelevantText;
  };
}

#endif // BEACON_H
