#ifndef LOCATION_H
#define LOCATION_H

#include "libpkpass.h"

namespace pkpass
{
  /*! \internal
   * \brief The Location class implements logic to interface with ILocations.
   */
  class Location : virtual public ILocation
  {
  public:
    Location(JsonData data);

    virtual double get_altitude()      { return Altitude; }
    virtual double get_latitude()      { return Latitude; }
    virtual double get_longtitude()    { return Longtitude; }
    virtual string get_relevant_text() { return RelevantText; }

  protected:
    double Altitude;     // [m]
    double Latitude;     // [m] required
    double Longtitude;   // [m] required
    string RelevantText;
  };
}

#endif // LOCATION_H
