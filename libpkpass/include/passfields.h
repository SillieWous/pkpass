#ifndef PASSFIELDS_H
#define PASSFIELDS_H

#include "libpkpass.h"

namespace pkpass
{
  /*! \internal
   * \brief The PassFields class implements logic to interface with IPassFields passes
   */
  class PassFields : virtual public IPassFields
  {
  public:
    PassFields(JsonData data);

    virtual IAuxiliaryFields get_auxiliary_fields() { return AuxiliaryFields; }
    virtual IBackFields      get_back_fields()      { return BackFields; }
    virtual IHeaderFields    get_header_fields()    { return HeaderFields; }
    virtual IPrimaryFields   get_primary_fields()   { return PrimaryFields; }
    virtual ISecondaryFields get_secondary_fields() { return SecondaryFields; }

  protected:
    IAuxiliaryFields AuxiliaryFields;
    IBackFields      BackFields;
    IHeaderFields    HeaderFields;
    IPrimaryFields   PrimaryFields;
    ISecondaryFields SecondaryFields;
  };

  /*! \internal
   * \brief The BoardingPass class implements logic to interface with IBoardingPasses
   */
  class BoardingPass : public PassFields, virtual public IBoardingPass
  {
  public:
    BoardingPass(JsonData data) : PassFields(data) {};

    ETransitType get_transit_type() { return TransitType; }

  protected:
    ETransitType TransitType;
  };

  /*! \internal
   * \brief The Coupon class implements logic to interface with ICoupon passes.
   */
  class Coupon : public PassFields, virtual public ICoupon
  {
  public:
    Coupon(JsonData data) : PassFields(data) {};
  };

  /*! \internal
   * \brief The EventTicket class implements logic to interface with IEventTickets
   */
  class EventTicket : public PassFields, virtual public IEventTicket
  {
  public:
    EventTicket(JsonData data) : PassFields(data) {};
  };

  /*! \internal
   * \brief The Generic class implements logic to interface with IGeneric passes.
   */
  class Generic : public PassFields, virtual public IGeneric
  {
  public:
    Generic(JsonData data) : PassFields(data) {};
  };

  /*! \internal
   * \brief The StoreCard class implements logic to interface with IStorCards
   */
  class StoreCard : public PassFields, virtual public IStoreCard
  {
  public:
    StoreCard(JsonData data) : PassFields(data) {};
  };
}

#endif // PASSFIELDS_H
