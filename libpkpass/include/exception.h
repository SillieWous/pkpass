#ifndef EXCEPTION_H
#define EXCEPTION_H

#define ArgumentException(what)       pkpass::IException(what, __FILE__, __FUNCTION__, __LINE__, pkpass::IException::Exception_t::Argument)
#define InternalException(what)       pkpass::IException(what, __FILE__, __FUNCTION__, __LINE__, pkpass::IException::Exception_t::Internal)
#define UnsupportedException(what)    pkpass::IException(what, __FILE__, __FUNCTION__, __LINE__, pkpass::IException::Exception_t::Unsupported)
#define TimeoutException(what)        pkpass::IException(what, __FILE__, __FUNCTION__, __LINE__, pkpass::IException::Exception_t::Timeout)
#define NotImplementedException(what) pkpass::IException(what, __FILE__, __FUNCTION__, __LINE__, pkpass::IException::Exception_t::NotImplemented)
#define NotFoundException(what)       pkpass::IException(what, __FILE__, __FUNCTION__, __LINE__, pkpass::IException::Exception_t::NotFound)

#endif // EXCEPTION_H
