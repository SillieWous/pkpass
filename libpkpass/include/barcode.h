#ifndef BARCODE_H
#define BARCODE_H

#include "libpkpass.h"

namespace pkpass
{
  /*! \internal
   * \brief The Barcode class implements logic to interface with IBarcodes.
   */
  class Barcode : virtual public IBarcode
  {
  public:
    Barcode(JsonData data);

    virtual string  get_alt_text()         { return AltText; }
    virtual EFormat get_format()           { return Format; }
    virtual string  get_message()          { return Message; }
    virtual string  get_message_encoding() { return MessageEncoding; }

  protected:
    EFormat Format;
    string  Message;
    string  MessageEncoding;
    string  AltText;

  private:
    const map<string, EFormat> Formats = { { "PKBarcodeFormatAztec",   EFormat::AZTEC   },
                                           { "PKBarcodeFormatCode128", EFormat::CODE128 },
                                           { "PKBarcodeFormatPDF417",  EFormat::PDF417  },
                                           { "PKBarcodeFormatQR",      EFormat::QR      } };
  };
}

#endif // BARCODE_H
