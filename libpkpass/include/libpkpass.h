#ifndef LIBPKPASS_H
#define LIBPKPASS_H

#if defined(_MSC_VER) || defined(WIN64) || defined(_WIN64) || defined(__WIN64__) || defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
#  define Q_DECL_EXPORT __declspec(dllexport)
#  define Q_DECL_IMPORT __declspec(dllimport)
#else
#  define Q_DECL_EXPORT     __attribute__((visibility("default")))
#  define Q_DECL_IMPORT     __attribute__((visibility("default")))
#endif

#if defined(LIBPKPASS_LIBRARY)
#  define LIBPKPASS_EXPORT Q_DECL_EXPORT
#else
#  define LIBPKPASS_EXPORT Q_DECL_IMPORT
#endif

#include <string>
#include <vector>
#include <map>
#include <stdint.h>

#include <jsoncpp/json/json.h>

/// \brief Main namespace
namespace pkpass
{
  using namespace std;

  typedef vector<string> strings;
  typedef Json::Value JsonData;

  /*!
   * \brief Interface for interacting with barcodes.
   */
  class LIBPKPASS_EXPORT IBarcode
  {
  public:
    /*!
     * \brief Format specifier for barcode types.
     */
    enum class EFormat
    {
      QR,     ///< QR barcode
      PDF417, ///< PDF417 barcode
      AZTEC,  ///< Aztec barcode
      CODE128 ///< Code128 barcode
    };

    /*!
     * \brief get_alt_text
     * \return Alternative text.
     */
    virtual string  get_alt_text() = 0;

    /*!
     * \brief get_format
     * \return Barcode format.
     */
    virtual EFormat get_format() = 0; // required

    /*!
     * \brief get_message
     * \return Message encoded by the barcode.
     */
    virtual string  get_message() = 0; // required

    /*!
     * \brief get_message_encoding
     * \return Text encoding of the message.
     */
    virtual string  get_message_encoding() = 0; // required

  };
  /*!
   * \brief A vector of barcodes.
   */
  typedef vector<IBarcode*> IBarcodes;

  /*!
   * \brief Interface for interacting with beacons.
   */
  class LIBPKPASS_EXPORT IBeacon
  {
  public:
    /*!
     * \brief get_major
     * \return The major identifier of a BLE location beacon.
     */
    virtual uint16_t get_major() = 0;

    /*!
     * \brief get_minor
     * \return The minor identifier of a BLE location beacon.
     */
    virtual uint16_t get_minor() = 0;

    /*!
     * \brief get_proximity_UUID
     * \return The unique identifier of a BLE location beacon.
     */
    virtual string   get_proximity_UUID() = 0;

    /*!
     * \brief get_relevant_text
     * \return Text to display in the presence of the beacon.
     */
    virtual string   get_relevant_text() = 0;
  };
  /*!
   * \brief Vector of beacons.
   */
  typedef vector<IBeacon*> IBeacons;

  /*!
   * \brief Interface for interacting with pass field content.
   */
  class LIBPKPASS_EXPORT IPassFieldContent
  {
  public:
    /*!
     * \brief Special types of data that can be used of a field.
     */
    enum class EDataDetectorType
    {
      PHONE_NUMBER,  ///< Detect phone numbers
      LINK,          ///< Detect URLs
      ADDRESS,       ///< Detect adresses
      CALENDAR_EVENT ///< Detect calender events
    };
    /*!
     * \brief Vector of dectector types.
     */
    typedef vector<EDataDetectorType> EDataDetectorTypes;

    /*!
     * \brief Styles to display dates in of a field.
     */
    enum class EDateStyle
    {
      NONE,   ///< No specific style
      SHORT,  ///< Short format style
      MEDIUM, ///< Medium format style
      LONG,   ///< Long format style
      FULL    ///< Full format style
    };

    /*!
     * \brief Styles to display numbers in of a field.
     */
    enum class ENumberStyle
    {
      DECIMAL,    ///< Format number as a decimal.
      PERCENT,    ///< Format number as a percentage.
      SCIENTIFIC, ///< Format number using scientific notation.
      SPELL_OUT   ///< Spell out the number.
    };

    /*!
     * \brief Possible text alignments of a field.
     */
    enum class ETextAlignment
    {
      LEFT,   ///< Left text alignment
      CENTER, ///< Center text alignment
      RIGHT,  ///< Right text alignment
      NATURAL ///< Natural text alignment
    };

    /*!
     * \brief Styles to display time in of a field.
     */
    enum class ETimeStyle
    {
      NONE,   ///< No specific style
      SHORT,  ///< Short format style
      MEDIUM, ///< Medium format style
      LONG,   ///< Long format style
      FULL    ///< Full format style
    };

    /*!
     * \brief get_attributed_value
     * \return Value of the field including HTML markup for links.
     */
    virtual string             get_attributed_value() = 0;

    /*!
     * \brief get_change_message
     * \return Text to display in an alert upon an update of the pass.
     */
    virtual string             get_change_message() = 0;

    /*!
     * \brief get_currency_code
     * \return The ISO 4217 currency code to use for the value of the field.
     */
    virtual string             get_currency_code() = 0;

    /*!
     * \brief get_data_detector_types
     * \return Types to detect in the value.
     *         Only relevant for fields on the back of a pass.
     */
    virtual EDataDetectorTypes get_data_detector_types() = 0;

    /*!
     * \brief get_date_style
     * \return The date style of the field.
     */
    virtual EDateStyle         get_date_style() = 0;

    /*!
     * \brief ignores_time_zone
     * \return Indicator if the current time zone is to be ignored.
     *         FALSE (default): use current time zone.
     *         TRUE: use the time as specified in the value of the field.
     */
    virtual bool               ignores_time_zone() = 0;

    /*!
     * \brief is_relative
     * \return Indicator if the date value is relative or not.
     *         FALSE (default): interpret as absolute date.
     *         TRUE: interpret as relatvie date.
     */
    virtual bool               is_relative() = 0;

    /*!
     * \brief get_key
     * \return The unique key which can be used to identify the field.
     */
    virtual string             get_key() = 0;

    /*!
     * \brief get_label
     * \return The text for a field label.
     */
    virtual string             get_label() = 0;                // not localisable

    /*!
     * \brief get_number_style
     * \return The style to display numbers of the field in.
     */
    virtual ENumberStyle       get_number_style() = 0;

    /*!
     * \brief get_text_alignment
     * \return The text aligment of the field.
     */
    virtual ETextAlignment     get_text_alignment() = 0;

    /*!
     * \brief get_time_style
     * \return The style to display time of the field in.
     */
    virtual ETimeStyle         get_time_style() = 0;

    /*!
     * \brief get_value
     * \return The value of the field.
     */
    virtual string             get_value() = 0;
  };

  /*!
   * \brief Interface for an auxiliary field of a pass.
   */
  class LIBPKPASS_EXPORT IAuxiliaryField : virtual public IPassFieldContent
  {
  public:
    /*!
     * \brief get_row
     * \return Row of the auxiliary field.
     *         Either 0 or 1
     */
    virtual int get_row() = 0;
  };

  /*!
   * \brief Interface for a field on the back of a pass.
   */
  class IBackField      : virtual public IPassFieldContent {};
  /*!
   * \brief Interface for a header field of a pass.
   */
  class IHeaderField    : virtual public IPassFieldContent {};
  /*!
   * \brief Interface for a primary field of a pass.
   */
  class IPrimaryField   : virtual public IPassFieldContent {};
  /*!
   * \brief Interface for a secondary field of a pass.
   */
  class ISecondaryField : virtual public IPassFieldContent {};

  /*!
   * \brief Map containing fields of a pass.
   */
  typedef map<string, IPassFieldContent*> IPassFieldContents;
  typedef map<string, IAuxiliaryField*>   IAuxiliaryFields;
  typedef IPassFieldContents              IBackFields;
  typedef IPassFieldContents              IHeaderFields;
  typedef IPassFieldContents              IPrimaryFields;
  typedef IPassFieldContents              ISecondaryFields;

  /*!
   * \brief Interface for interacting with fields of a pass.
   */
  class LIBPKPASS_EXPORT IPassFields
  {
  public:
    /*!
     * \brief get_auxiliary_fields
     * \return The auxiliary fields of a pass.
     */
    virtual IAuxiliaryFields get_auxiliary_fields() = 0;

    /*!
     * \brief get_back_fields
     * \return The fields on the back a pass.
     */
    virtual IBackFields      get_back_fields() = 0;

    /*!
     * \brief get_header_fields
     * \return The header fields of a pass.
     */
    virtual IHeaderFields    get_header_fields() = 0;

    /// <returns>The primary fields of a pass.</returns>
    /*!
     * \brief get_primary_fields
     * \return
     */
    virtual IPrimaryFields   get_primary_fields() = 0;

    /*!
     * \brief get_secondary_fields
     * \return The secondary fields of a pass.
     */
    virtual ISecondaryFields get_secondary_fields() = 0;
  };

  /*!
   * \brief Interface for interacting with a boarding pass.
   */
  class LIBPKPASS_EXPORT IBoardingPass : virtual public IPassFields
  {
  public:
    /*!
     * \brief Specifier for the type of the transit.
     */
    enum class ETransitType
    {
      AIR,     ///< Airplane ticket
      BOAT,    ///< Boat ticket
      BUS,     ///< Bus ticket
      GENERIC, ///< Other type of boarding pass
      TRAIN    ///< Train ticket
    };

    /*!
     * \brief get_transit_type
     * \return The transit type of the boarding pass.
     */
    virtual ETransitType get_transit_type() = 0;
  };
  /*!
   * \brief Interface for interacting with a coupon pass.
   */
  class ICoupon      : virtual public IPassFields {};
  /*!
   * \brief Interface for interacting with an event pass.
   */
  class IEventTicket : virtual public IPassFields {};
  /*!
   * \brief Interface for interacting with a generic pass.
   */
  class IGeneric     : virtual public IPassFields {};
  /*!
   * \brief Interface for interacting with a store card pass.
   */
  class IStoreCard   : virtual public IPassFields {};

  /*!
   * \brief Interface for interacting with pass locations.
   */
  class LIBPKPASS_EXPORT ILocation
  {
  public:
    /*!
     * \brief get_altitude
     * \return The altitude of the location. [m]
     */
    virtual double get_altitude() = 0;

    /*!
     * \brief get_latitude
     * \return The latitude of the location. [m]
     */
    virtual double get_latitude() = 0;

    /// <returns></returns>
    /*!
     * \brief get_longtitude
     * \return The longtitude of the location. [m]
     */
    virtual double get_longtitude() = 0;

    /*!
     * \brief get_relevant_text
     * \return
     */
    virtual string get_relevant_text() = 0;
  };
  /*!
   * \brief A vector of locations.
   */
  typedef vector<ILocation*> ILocations;

  /*!
   * \brief Interface for interacting with NFC tags of the pass.
   */
  class LIBPKPASS_EXPORT INfc
  {
  public:
    /*!
     * \brief get_encryption_public_key
     * \return The public encryption key the Value Added Services protocol uses.
     */
    virtual string get_encryption_public_key() = 0;

    /*!
     * \brief get_message
     * \return The message to transmit.
     */
    virtual string get_message() = 0;

    /*!
     * \brief get_requires_authentication
     * \return Indicator if the pass requires the user to authenticate.
     *         FALSE (default): no authentication required.
     *         TRUE: authentication is required for each use of the pass.
     */
    virtual bool   get_requires_authentication() = 0;
  };

  /*!
   * \brief Interface for interacting with a pass or
   *        create one from a .pkpass file.
   */
  class LIBPKPASS_EXPORT IPass
  {
  public:
    /*!
     * \brief Indicator flags used to indicate how a pass should be created.
     */
    // Not enum class to allow ORing the flags
    enum ECreationFlags
    {
      VERIFY_SIGNATURE                      = 1 << 0, ///< 2^0 Verify the signature in the pass.
      VERIFY_MANIFEST                       = 1 << 1, ///< 2^1 Verify the manifest in the pass.
      ALLOW_UNSUPPORTED_CERTIFICATE_PURPOSE = 1 << 2, /*!< 2^2 Allow certificates that are not intended for digital
                                                       * signatures. For example the Apple root CA used to sign passes
                                                       * only allows certificate signing purposes.
                                                       */
      ALLOW_SELF_SIGNED                     = 1 << 3, /*!< 2^3 Allow signatures of which the chain is fully contianed
                                                       * in the pass.
                                                       */
      TEMPORARY_PASS                        = 1 << 4, ///< 2^4 Save the pass in a temporary location.
      LOAD_FROM_DIR                         = 1 << 5, ///< 2^5 Load pass from a given directory.
    };

    /*!
     * \brief Create
     * \param pkpassFileName Filename of the pass.
     * \param flags Creation flags.
    *               Default: VERIFY_SIGNATURE, VERIFY_MANIFEST and TEMPORARY_PASS are set.
     * \return
     */
    static shared_ptr<IPass> create(string pkpassFileName, int flags =
        (ECreationFlags::VERIFY_SIGNATURE | ECreationFlags::VERIFY_MANIFEST | ECreationFlags::TEMPORARY_PASS));

    /*!
     * \brief get_description
     * \return A short description of the pass.
     */
    virtual string         get_description() = 0;          // not localisable

    /*!
     * \brief get_organisation_name
     * \return The name of the orinisation.
     */
    virtual string         get_organisation_name() = 0;    // not localisable

    /*!
     * \brief get_pass_type_identifier
     * \return The pass type identifier that is used to sign the pass with.
     */
    virtual string         get_pass_type_identifier() = 0;

    /*!
     * \brief get_serial_number
     * \return An alphanumeric serial number.
     */
    virtual string         get_serial_number() = 0;

    /*!
     * \brief get_team_identifier
     * \return The Team ID for the Apple Developer Program account that registered the pass type identifier.
     */
    virtual string         get_team_identifier() = 0;      // required

    /*!
     * \brief get_format_version
     * \return The pass format version. Must be equal to 1.
     */
    virtual int            get_format_version() = 0;


    /*!
     * \brief get_barcodes
     * \return The barcodes contained in the pass.
     */
    virtual IBarcodes      get_barcodes() = 0;

    /*!
     * \brief get_beacons
     * \return The beacons contained in the pass.
     */
    virtual IBeacons       get_beacons() = 0;

    /*!
     * \brief get_locations
     * \return The locations contained in the pass.
     */
    virtual ILocations     get_locations() = 0;

    /*!
     * \brief get_max_distance
     * \return The maximum distance to any of the locations where the pass is considered to be relevant.
     */
    virtual double         get_max_distance() = 0;


    /*!
     * \brief get_boarding_pass
     * \return The boardingpass information contained in the pass.
     */
    virtual IBoardingPass& get_boarding_pass() = 0;

    /*!
     * \brief get_coupon
     * \return The coupon information contained in the pass.
     */
    virtual ICoupon&       get_coupon() = 0;

    /*!
     * \brief get_event_ticket
     * \return The event ticket information contained in the pass.
     */
    virtual IEventTicket&  get_event_ticket() = 0;

    /*!
     * \brief get_generic
     * \return The generic pass information contained in the pass.
     */
    virtual IGeneric&      get_generic() = 0;

    /*!
     * \brief get_store_card
     * \return The store card information contained in the pass.
     */
    virtual IStoreCard&    get_store_card() = 0;

    /*!
     * \brief get_nfc
     * \return The NFC information contained in the pass.
     */
    virtual INfc&          get_nfc() = 0;


    /*!
     * \brief get_background_colour
     * \return CSS rgb background colour of the pass.
     */
    virtual string         get_background_colour() = 0;

    /*!
     * \brief get_foreground_colour
     * \return CSS rgb foreground colour of the pass.
     */
    virtual string         get_foreground_colour() = 0;

    /*!
     * \brief get_label_colour
     * \return CSS rgb label colour of the pass.
     */
    virtual string         get_label_colour() = 0;


    /*!
     * \brief get_expiration_date
     * \return The expiration date of the pass.
     */
    virtual string         get_expiration_date() = 0;

    /*!
     * \brief get_relevant_date
     * \return The relevant date of the pass.
     */
    virtual string         get_relevant_date() = 0;


    /*!
     * \brief get_associated_app_identifiers
     * \return IDs of apps associated with the pass.
     */
    virtual strings        get_associated_app_identifiers() = 0;

    /*!
     * \brief get_grouping_identifier
     * \return Identifiers to use for grouping passes together when displayed.
     */
    virtual string         get_grouping_identifier() = 0;


    /*!
     * \brief get_app_launch_url
     * \return A URL to pass to associated apps.
     */
    virtual string         get_app_launch_url() = 0;

    /*!
     * \brief get_authentication_token
     * \return Authentication token to pass to the associated web service.
     */
    virtual string         get_authentication_token() = 0;

    /*!
     * \brief get_web_service_url
     * \return A URL for a web service that is used to update or personalise the pass.
     */
    virtual string         get_web_service_url() = 0;

    /*!
     * \brief get_user_info
     * \return JSON data that contains information for companion apps.
     */
    virtual JsonData       get_user_info() = 0;

    // <returns></returns>
    //SemanticTags  SemanticTags;


    /*!
     * \brief is_sharing_prohibited
     * \return  Indicator to show if sharing the pass is allowed.
     *          It does not actually prevent sharing.
     *          FALSE (default): sharing of the pass is permitted.
     *          TRUE: sharing of the pass is prohibited.
     */
    virtual bool           is_sharing_prohibited() = 0;    // default false

    /*!
     * \brief get_suppress_strip_shine
     * \return Inticator to control if the strip image is to be displayed without a shine effect.
     *         TRUE (default): suppress displaying of the shine effect
     *         FALSE: display the shine effect
     */
    virtual bool           get_suppress_strip_shine() = 0; // default false

    /*!
     * \brief is_voided
     * \return Indicator if the pass is voided.
     *         FALSE (default): the pass is not voided.
     *         TRUE: the pass is voided.
     */
    virtual bool           is_voided() = 0;                // default false;


    /*!
     * \brief get_storage_directory
     * \return The location the pass file is stored.
     */
    virtual string         get_storage_directory() = 0;
  };

  /*!
   * \brief Exception interface.
   */
  class LIBPKPASS_EXPORT IException
  {
  public:
    /*!
     * \brief Possible types of IExceptions.
     */
    enum class Exception_t
    {
      Argument,       ///< Invalid argument received.
      Internal,       ///< Something went wrong internally.
      Unsupported,    ///< Functionality is not supported.
      Timeout,        ///< Execution took too long.
      NotImplemented, ///< Functionality is not implemented.
      NotFound        ///< Something that is needed is not found, e.g. a file.
    };

    /*!
     * \brief Constructor for IException class.
     * \param what Description of what the exception is about.
     * \param file File that caused the exception.
     * \param function Function that caused the exception.
     * \param line Line that caused the exception.
     * \param type Type of exception being thrown.
     */
    IException(string what, string file, string function, int line, Exception_t type)
      : What(what),
        Type(type)
    {
        Where = "File:" + file + " Function:" + function + " Line:" + to_string(line);
    }

    /*!
     * \brief what
     * \return Description of what the exception is about.
     */
    string      what() { return What; }

    /*!
     * \brief where
     * \return Description of where the exception occured.
     */
    string      where(){ return Where; }

    /*!
     * \brief type
     * \return Type of the exception.
     */
    Exception_t type() { return Type; }

  protected:
    /*!
     * \brief What
     */
    string      What;
    /*!
     * \brief Where
     */
    string      Where;
    /*!
     * \brief Type
     */
    Exception_t Type;
  };
}

#endif // LIBPKPASS_H
