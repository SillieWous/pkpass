#ifndef PASSFIELDCONTENT_H
#define PASSFIELDCONTENT_H

#include "libpkpass.h"

#include <boost/assign/list_of.hpp>

using namespace boost::assign;

namespace pkpass
{
  /*! \internal
   * \brief The PassFieldContent class implements logic to interface with IPassFieldContents
   */
  class PassFieldContent : virtual public IPassFieldContent
  {
  public:
    PassFieldContent(JsonData data);

    virtual string             get_attributed_value()    { return AttributedValue; }
    virtual string             get_change_message()      { return ChangeMessage; }
    virtual string             get_currency_code()       { return CurrencyCode; }
    virtual EDataDetectorTypes get_data_detector_types() { return DataDetectorTypes; }
    virtual EDateStyle         get_date_style()          { return DateStyle; }
    virtual bool               ignores_time_zone()       { return IgnoresTimeZone; }
    virtual bool               is_relative()             { return IsRelative; }
    virtual string             get_key()                 { return Key; }
    virtual string             get_label()               { return Label; }
    virtual ENumberStyle       get_number_style()        { return NumberStyle; }
    virtual ETextAlignment     get_text_alignment()      { return TextAlignment; }
    virtual ETimeStyle         get_time_style()          { return TimeStyle; }
    virtual string             get_value()               { return Value; }

  protected:
    string             Key;   // required
    string             Value; // required
    string             Label;

    string             AttributedValue;
    string             ChangeMessage;
    string             CurrencyCode;
    EDataDetectorTypes DataDetectorTypes;
    EDateStyle         DateStyle;
    bool               IgnoresTimeZone = false;
    bool               IsRelative = false;
    ENumberStyle       NumberStyle;
    ETextAlignment     TextAlignment;
    ETimeStyle         TimeStyle;

  private:
    const map<string, EDataDetectorType> EDataDetectorTypes = { { "PKDataDetectorTypePhoneNumber",   EDataDetectorType::PHONE_NUMBER   },
                                                                { "PKDataDetectorTypeLink",          EDataDetectorType::LINK           },
                                                                { "PKDataDetectorTypeAddress",       EDataDetectorType::ADDRESS        },
                                                                { "PKDataDetectorTypeCalendarEvent", EDataDetectorType::CALENDAR_EVENT } };
    const map<string, EDateStyle>        EDateStyles        = { { "PKDateStyleFull",                 EDateStyle::FULL   },
                                                                { "PKDateStyleLong",                 EDateStyle::LONG   },
                                                                { "PKDateStyleMedium",               EDateStyle::MEDIUM },
                                                                { "PKDateStyleNone",                 EDateStyle::NONE   },
                                                                { "PKDateStyleShort",                EDateStyle::SHORT  } };
    const map<string, ENumberStyle>      ENumberStyles      = { { "PKNumberStyleDecimal",            ENumberStyle::DECIMAL    },
                                                                { "PKNumberStylePercent",            ENumberStyle::PERCENT    },
                                                                { "PKNumberStyleScientific",         ENumberStyle::SCIENTIFIC },
                                                                { "PKNumberStyleSpellOut",           ENumberStyle::SPELL_OUT  } };
    const map<string, ETextAlignment>    ETextAlignments    = { { "PKTextAlignmentCenter",           ETextAlignment::CENTER  },
                                                                { "PKTextAlignmentLeft",             ETextAlignment::LEFT    },
                                                                { "PKTextAlignmentNatural",          ETextAlignment::NATURAL },
                                                                { "PKTextAlignmentRight",            ETextAlignment::RIGHT   } };
    const map<string, ETimeStyle>        ETimeStyles        = { { "PKDateStyleFull",                 ETimeStyle::FULL   },
                                                                { "PKDateStyleLong",                 ETimeStyle::LONG   },
                                                                { "PKDateStyleMedium",               ETimeStyle::MEDIUM },
                                                                { "PKDateStyleNone",                 ETimeStyle::NONE   },
                                                                { "PKDateStyleShort",                ETimeStyle::SHORT  } };
  };

  /*! \internal
   * \brief The AuxiliaryField class
   */
  class AuxiliaryField : public PassFieldContent, virtual public IAuxiliaryField
  {
  public:
    AuxiliaryField (JsonData data) : PassFieldContent (data) {};

    int get_row() { return Row; }

  protected:
    int Row;
  };

  /*! \internal
   * \brief The BackField class
   */
  class BackField : public PassFieldContent, virtual public IBackField
  {
  public:
    BackField(JsonData data) : PassFieldContent (data) {};
  };

  /*! \internal
   * \brief The HeaderField class
   */
  class HeaderField : public PassFieldContent, virtual public IHeaderField
  {
  public:
    HeaderField(JsonData data) : PassFieldContent (data) {};
  };

  /*! \internal
   * \brief The PrimaryField class
   */
  class PrimaryField : public PassFieldContent, virtual public IPrimaryField
  {
  public:
    PrimaryField(JsonData data) : PassFieldContent (data) {};
  };

  /*! \internal
   * \brief The SecondaryField class
   */
  class SecondaryField : public PassFieldContent, virtual public ISecondaryField
  {
  public:
    SecondaryField(JsonData data) : PassFieldContent (data) {};
  };
}

#endif // PASSFIELDCONTENT_H
