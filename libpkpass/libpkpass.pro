TEMPLATE = lib
DEFINES += LIBPKPASS_LIBRARY
CONFIG += c++17
CONFIG -= qt

CONFIG(debug, debug|release):DEFINES += DEBUG

VERSION = 0.0.1

TARGET = pkpass

SOURCES += \
  src/barcode.cpp \
  src/beacon.cpp \
  src/json.cpp \
  src/location.cpp \
  src/nfc.cpp \
  src/pass.cpp \
  src/passfieldcontent.cpp \
  src/passfields.cpp

HEADERS += \
  include/barcode.h \
  include/beacon.h \
  include/exception.h \
  include/json.h \
  include/libpkpass.h \
  include/location.h \
  include/nfc.h \
  include/pass.h \
  include/passfieldcontent.h \
  include/passfields.h

OTHER_FILES += \
  manifest-deb.yml \
  README.md \
  RELEASE_NOTES.md

INCLUDEPATH += \
  include

LIBS += \
  -ljsoncpp \
  -lzip

QMAKE_POST_LINK = \
  $$PWD/../package_deb.sh -f $$PWD/manifest-deb.yml -w $$OUT_PWD -a $$(TARGET_ARCH) &&\
  $$PWD/../doc/generate-docs.sh -d $$OUT_PWD -p libpkpass -f $$PWD/../doc

unix {
  target.path = .local/lib
}
!isEmpty(target.path): INSTALLS += target
