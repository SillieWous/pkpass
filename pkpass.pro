TEMPLATE=subdirs

ARCH=$$(TARGET_ARCH)
equals(ARCH, "amd64") {
  SUBDIRS = \
    app_pkpass \
    libbarcodegen \
    libpkpass \
    test_pkpass

  test_pkpass.depends = libpkpass libbarcodegen
}

equals(ARCH, "arm64") {
  SUBDIRS = \
    app_pkpass \
    libbarcodegen \
    libpkpass
}

app_pkpass.depends  = libpkpass libbarcodegen

OTHER_FILES = \
  package_deb.sh \
  doc/generate-docs.sh \
  doc/doxygen.conf \
  doc/custom.css
